<?php 
require_once '../modelos/PlanchetaCatastral.php';
$p=new PlanchetaCatastral();

$idPlancheta=isset($_POST["idPlancheta"])? limpiarCadena($_POST["idPlancheta"]):""; //si existe un envio por metodo post llamo a limpiar cadena

$referencia=isset($_POST["referencia"])? limpiarCadena($_POST["referencia"]):"";
$referenciaOriginal=isset($_POST["referenciaOriginal"])? limpiarCadena($_POST["referenciaOriginal"]):"";
$planchetaActual=isset($_POST["planchetaActual"])? limpiarCadena($_POST["planchetaActual"]):"";



switch ($_GET["op"]) {
	case 'guardarOeditar':
			session_start();
			$ultimaModificacion=$_SESSION['idUsuarioDefPos'];

			$plancheta=$planchetaActual;
			

			$resultadoRef=null;
			$referenciaOriginal="";
			//traemos la referencia si existe
			if(!empty($idPlancheta)){
				$resultado=$p->buscarId($idPlancheta);
				while($reg=$resultado->fetch_object()){
					$referenciaOriginal=$reg->referencia;
				}   
			}
			if($referencia!=$referenciaOriginal){
				$resultadoRef=$p->comprobarExistenciaRef($referencia);
			}
				
			if(!empty($resultadoRef)){
				echo "¡Referencia ya Existe!";
			}else{
				if(is_uploaded_file($_FILES['plancheta']['tmp_name'])){

					$plancheta=$referencia.'.pdf';
					move_uploaded_file($_FILES['plancheta']['tmp_name'], "../files/planchetas/".$plancheta);
					
				}


				//nuevo
				if(empty($idPlancheta)){
					$resultado=$p->insertar($referencia,$plancheta,$ultimaModificacion);
					echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
				}else{
					//editar
					$resultado=$p->editar($idPlancheta,$referencia,$plancheta,$ultimaModificacion);
					echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
				}
			}		
	
	break;
	
	case 'mostrar':
		$resultado=$p->buscar($idPlancheta);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;



	case 'listar':
		session_start();
		$varAlteracionPlanchetas=$_SESSION['alt_planchetas'];
		$resultado=$p->listar();
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			
			if($varAlteracionPlanchetas==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_plancheta.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$reg->id_plancheta,
				"1"=>$opciones,
				"2"=>$reg->referencia,
				"3"=>'<a href="../files/planchetas/'.$reg->plancheta."?".rand().'" target="_blank">'.$reg->plancheta,
				"4"=>$reg->ultimaModificacion,

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'borrarArchivo':
		session_start();
		$idPlan=$_REQUEST['idPlan'];
		$idUltimaModificacion=$_SESSION['idUsuarioDefPos'];

		//buscamos el expediente
		$resultado=$p->buscarId($idPlan);
		while($reg=$resultado->fetch_object()){
			$plancheta=$reg->plancheta;		
		}   


		$ubicacion='..\files\planchetas\\'.$plancheta; //borra el archivo en la carpeta
		unlink($ubicacion);
		$resultado=$p->borrarPlancheta($idPlan,$idUltimaModificacion);
		echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
		
	

	break;


	case 'traerAlteracion':
		session_start();
		$respuesta=$_SESSION['alt_planchetas'];
		echo $respuesta;
	break;


	
	
}
?>