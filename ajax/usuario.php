<?php 

require_once '../modelos/Usuario.php';
$u=new Usuario();

$idUsuario=isset($_POST["idUsuario"])? limpiarCadena($_POST["idUsuario"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$idTipoUsuario=isset($_POST["idTipoUsuario"])? limpiarCadena($_POST["idTipoUsuario"]):"";
$usuario=isset($_POST["usuario"])? limpiarCadena($_POST["usuario"]):"";
$clave=isset($_POST["clave"])? limpiarCadena($_POST["clave"]):"";
$estado=isset($_POST["estado"])? limpiarCadena($_POST["estado"]):"";

switch ($_GET["op"]) {
	case 'guardarOeditar':
		
			$resultadoUsuario=null;
			$usuarioOriginal="";
			//traemos el usuario si existe
			if(!empty($idUsuario)){
				$resultado=$u->buscarId($idUsuario);
				while($reg=$resultado->fetch_object()){
					$usuarioOriginal=$reg->usuario;
				}   
			}
			if($usuario!=$usuarioOriginal){
				$resultadoUsuario=$u->comprobarExistenciaUsuario($usuario);
			}

			if(!empty($resultadoUsuario)){
				echo "¡Usuario Ya Existe!";
			}else{
				//CLAVE HASH SHA256
				$claveHash=hash("SHA256",$clave);
				if(empty($idUsuario)){
					$resultado=$u->insertar($nombre,$idTipoUsuario,$usuario,$claveHash,$estado);
					echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
				}else{
					$resultado=$u->editar($idUsuario,$nombre,$idTipoUsuario,$usuario,$estado);
					echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
				}
			}
		
		


		
	break;


	case 'modificarClave':
		$nuevaClave=isset($_POST["nuevaClave"])? limpiarCadena($_POST["nuevaClave"]):"";
		$idModificarClave=isset($_POST["idModificarClave"])? limpiarCadena($_POST["idModificarClave"]):"";
		//CLAVE HASH SHA256
		$claveHash=hash("SHA256",$nuevaClave);

		$resultado=$u->modificarClave($idModificarClave,$claveHash);
		echo $resultado ? "¡Clave modificada con exito!" : "¡Clave no se pudo modificar!";

	break;

	case 'modificarClaveMiCuenta':
		session_start();
		$nuevaClave=isset($_POST["nuevaClave"])? limpiarCadena($_POST["nuevaClave"]):"";
		$idModificarClave=$_SESSION['idUsuarioDefPos'];
		//CLAVE HASH SHA256
		$claveHash=hash("SHA256",$nuevaClave);

		$resultado=$u->modificarClave($idModificarClave,$claveHash);
		echo $resultado ? "¡Clave modificada con exito!" : "¡Clave no se pudo modificar!";

	break;


	case 'editarMiCuenta':
		$idUsuario=$_SESSION['idUsuarioDefPos'];
		
		$resultadoUsuario=null;
		$resultado=$u->buscarId($idUsuario);
		while($reg=$resultado->fetch_object()){
			$usuarioOriginal=$reg->usuario;
		}   
		if($usuario!=$usuarioOriginal){
			$resultadoUsuario=$u->comprobarExistenciaUsuario($usuario);
		}

		if(!empty($resultadoUsuario)){
			echo "¡Usuario Ya Existe!";
		}else{
			
			$resultado=$u->editarMiCuenta($idUsuario,$nombre,$usuario);
			echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
			
		}
		

	break;

	
	case 'mostrar':
		$resultado=$u->buscar($idUsuario);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	case 'desactivar':
		$resultado=$u->desactivar($idUsuario);
		echo $resultado ? "¡Registro se cambio a estado 'INACTIVO'!" : "¡No se pudo cambiar el estado!";
	break;

	case 'activar':
		$resultado=$u->activar($idUsuario);
		echo $resultado ? "¡Registro se cambio a estado 'ACTIVO'!" : "¡No se pudo cambiar el estado!";
	break;

	case 'listar':
		session_start();
		$varAlteracionUsuarios=$_SESSION['alt_configuracion'];
		$resultado=$u->listar();
			
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionUsuarios==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_usuario.')"><i class="fas fa-pencil-alt"></i></button>';
			}else{
				$opciones='';
			}


			if($reg->estado=="ACTIVO"){
				$status='<span class="label bg-green">'.$reg->estado.'</span>';
			}else{
				$status='<span class="label bg-red">'.$reg->estado.'</span>';
			}
			
			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nombre,
				"2"=>$reg->tipo_usuario,
				"3"=>$reg->usuario,
				"4"=>$status,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'selectTipoUsuario':
		require_once '../modelos/TipoUsuario.php';
		$t=new TipoUsuario();
		$resultado=$t->select();
		$opcion="[SELECCIONAR]";
		echo '<option selected="true" disabled="disabled">'.$opcion.'</option>';
		while($reg=$resultado->fetch_object()){

			echo '<option value='.$reg->id_tipo_usuario.'>'.$reg->nombre.'</option>';

		}


	break;

	

	


	
	
}
?>