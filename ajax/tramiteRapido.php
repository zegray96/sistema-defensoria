<?php 

require_once '../modelos/TramiteRapido.php';
$t=new TramiteRapido();

$idTramite=isset($_POST["idTramite"])? limpiarCadena($_POST["idTramite"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$fechaInicio=isset($_POST["fechaInicio"])? limpiarCadena($_POST["fechaInicio"]):"";
$fechaFinalizacion=isset($_POST["fechaFinalizacion"])? limpiarCadena($_POST["fechaFinalizacion"]):"";
$nroTramite=isset($_POST["nroTramite"])? limpiarCadena($_POST["nroTramite"]):"";
$anio=isset($_POST["anio"])? limpiarCadena($_POST["anio"]):"";
$requirente=isset($_POST["requirente"])? limpiarCadena($_POST["requirente"]):"";
$dni=isset($_POST["dni"])? limpiarCadena($_POST["dni"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$domicilio=isset($_POST["domicilio"])? limpiarCadena($_POST["domicilio"]):"";
$reclamo=isset($_POST["reclamo"])? limpiarCadena($_POST["reclamo"]):"";
$idAgente=isset($_POST["idAgente"])? limpiarCadena($_POST["idAgente"]):"";
$estado=isset($_POST["estado"])? limpiarCadena($_POST["estado"]):"";
$informeActual=isset($_POST["informeActual"])? limpiarCadena($_POST["informeActual"]):"";



switch ($_GET["op"]) {
	case 'guardarOeditar':

			session_start();
			$ultimaModificacion=$_SESSION['idUsuarioDefPos'];

			$informe=$informeActual;	

			$resultadoNro=null;
			$nroYanio=$nroTramite.$anio;
			$nroYanioOriginal="";
			//traemos el tramite si existe
			if(!empty($idTramite)){
				$resultado=$t->buscarId($idTramite);
				while($reg=$resultado->fetch_object()){
					$nroYanioOriginal=$reg->nro_tramite.$reg->anio;
				}   
			}
			if($nroYanio!=$nroYanioOriginal){
				$resultadoNro=$t->comprobarExistenciaNro($nroTramite,$anio);
			}
				
			if(!empty($resultadoNro)){
				echo "¡N° de Tramite ya Existe!";
			}else{

				if(is_uploaded_file($_FILES['informe']['tmp_name'])){

					$informe=$nroTramite.'-'.$anio.'_informe.pdf';
					move_uploaded_file($_FILES['informe']['tmp_name'], "../files/informesTramitesRapidos/".$informe);
					
				}

				
				if(empty($idTramite)){
					$resultado=$t->insertar($nroTramite,$anio,$fechaInicio,$fechaFinalizacion,$estado,$requirente,$dni,$telefono,$domicilio,$reclamo,$informe,$idAgente,$ultimaModificacion);
					echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
				}else{
					//editar
					$resultado=$t->editar($idTramite,$nroTramite,$anio,$fechaInicio,$fechaFinalizacion,$estado,$requirente,$dni,$telefono,$domicilio,$reclamo,$informe,$idAgente,$ultimaModificacion);
					echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
				}
			}	
	
	break;
	
	case 'mostrar':
		$resultado=$t->buscar($idTramite);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;


	case 'buscarPorNro':
		session_start();
		$varAlteracionTramitesRapidos=$_SESSION['alt_tramites_rapidos'];
		$varNroTramite=$_REQUEST['varNroTramite'];
		$resultado=$t->buscarPorNro($varNroTramite);
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			$fechaIniFormateada = date("Y-m-d", strtotime($reg->fecha_inicio));
			if($reg->fecha_finalizacion=="0000-00-00"){
				$fechaFinFormateada="";
			}else{
				$fechaFinFormateada = date("Y-m-d", strtotime($reg->fecha_finalizacion));
			}
			

			if($reg->estado=="EN TRAMITE"){
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="CERRADO-NOTIFICADO"){
					$estado='<span class="label bg-green">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="CERRADO-SIN NOTIFICAR"){
						$estado='<span class="label bg-orange">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="PASO A EXPEDIENTE"){
							$estado='<span class="label bg-blue">'.$reg->estado.'</span>';
						}
					}
				}
			}

			if($varAlteracionTramitesRapidos==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_tramite_rapido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$reg->id_tramite_rapido,
				"1"=>$opciones,
				"2"=>$fechaIniFormateada,
				"3"=>$fechaFinFormateada,
				"4"=>$estado,
				"5"=>$reg->nro_tramite,
				"6"=>$reg->anio,
				"7"=>$reg->requirente,
				"8"=>$reg->dni,
				"9"=>$reg->telefono,
				"10"=>$reg->domicilio,
				"11"=>$reg->reclamo,
				"12"=>$reg->agente,
				"13"=>'<a href="../files/informesTramitesRapidos/'.$reg->informe.'?'.rand().'" target="_blank">'.$reg->informe,
				"14"=>$reg->ultimaModificacion,
				

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'buscarPorFecha':
		session_start();
		$varAlteracionTramitesRapidos=$_SESSION['alt_tramites_rapidos'];
		$varFechaIniBuscar=$_REQUEST['varFechaIniBuscar'];
		$varFechaFinBuscar=$_REQUEST['varFechaFinBuscar'];

		$resultado=$t->buscarEntreFechas($varFechaIniBuscar,$varFechaFinBuscar);
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			$fechaIniFormateada = date("Y-m-d", strtotime($reg->fecha_inicio));
			if($reg->fecha_finalizacion=="0000-00-00"){
				$fechaFinFormateada="";
			}else{
				$fechaFinFormateada = date("Y-m-d", strtotime($reg->fecha_finalizacion));
			}
			

			if($reg->estado=="EN TRAMITE"){
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="CERRADO-NOTIFICADO"){
					$estado='<span class="label bg-green">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="CERRADO-SIN NOTIFICAR"){
						$estado='<span class="label bg-orange">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="PASO A EXPEDIENTE"){
							$estado='<span class="label bg-blue">'.$reg->estado.'</span>';
						}
					}
				}
			}

			if($varAlteracionTramitesRapidos==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_tramite_rapido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$reg->id_tramite_rapido,
				"1"=>$opciones,
				"2"=>$fechaIniFormateada,
				"3"=>$fechaFinFormateada,
				"4"=>$estado,
				"5"=>$reg->nro_tramite,
				"6"=>$reg->anio,
				"7"=>$reg->requirente,
				"8"=>$reg->dni,
				"9"=>$reg->telefono,
				"10"=>$reg->domicilio,
				"11"=>$reg->reclamo,
				"12"=>$reg->agente,
				"13"=>'<a href="../files/informesTramitesRapidos/'.$reg->informe.'?'.rand().'" target="_blank">'.$reg->informe,
				"14"=>$reg->ultimaModificacion,
				

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listarPorAnio':
		session_start();
		$varAlteracionTramitesRapidos=$_SESSION['alt_tramites_rapidos'];
		$varAnio=$_REQUEST['varAnio'];
		$resultado=$t->listarPorAnio($varAnio);
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			$fechaIniFormateada = date("Y-m-d", strtotime($reg->fecha_inicio));
			if($reg->fecha_finalizacion=="0000-00-00"){
				$fechaFinFormateada="";
			}else{
				$fechaFinFormateada = date("Y-m-d", strtotime($reg->fecha_finalizacion));
			}
			

			if($reg->estado=="EN TRAMITE"){
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="CERRADO-NOTIFICADO"){
					$estado='<span class="label bg-green">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="CERRADO-SIN NOTIFICAR"){
						$estado='<span class="label bg-orange">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="PASO A EXPEDIENTE"){
							$estado='<span class="label bg-blue">'.$reg->estado.'</span>';
						}
					}
				}
			}

			if($varAlteracionTramitesRapidos==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_tramite_rapido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$reg->id_tramite_rapido,
				"1"=>$opciones,
				"2"=>$fechaIniFormateada,
				"3"=>$fechaFinFormateada,
				"4"=>$estado,
				"5"=>$reg->nro_tramite,
				"6"=>$reg->anio,
				"7"=>$reg->requirente,
				"8"=>$reg->dni,
				"9"=>$reg->telefono,
				"10"=>$reg->domicilio,
				"11"=>$reg->reclamo,
				"12"=>$reg->agente,
				"13"=>'<a href="../files/informesTramitesRapidos/'.$reg->informe.'?'.rand().'" target="_blank">'.$reg->informe,
				"14"=>$reg->ultimaModificacion,
				

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'selectAgente':
		require_once '../modelos/Agente.php';
		$a=new Agente();
		$resultado=$a->select();
		$opcion="[SELECCIONAR]";
		echo '<option selected="true" disabled="disabled">'.$opcion.'</option>';
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->id_agente.'>'.$reg->nombre.'</option>';
		}
	break;

	case 'ultimoNro':
		$anio=$_REQUEST['anio'];
		$resultado=$t->ultimoNro($anio);
		echo json_encode($resultado);
	break;


	case 'fecha':
		date_default_timezone_set('America/Argentina/Buenos_Aires'); //importante aclarar la ubicacion, sino toma otra fecha
		echo date('Y-m-d');
	break;

	

	case 'borrarArchivo':
		session_start();
		$idTra=$_REQUEST['idTra'];
		$valor=$_REQUEST['valor'];
		$idUltimaModificacion=$_SESSION['idUsuarioDefPos'];

		//buscamos el expediente
		$resultado=$t->buscarIdParaBorrar($idTra);
		while($reg=$resultado->fetch_object()){
			$informe=$reg->informe;
		}   
		         
		//de acuerdo al valor que le pasemos, eliminamos el archivo
		if($valor=='informe'){
			$ubicacion='..\files\informesTramitesRapidos\\'.$informe; //borra el archivo en la carpeta
			unlink($ubicacion);
			$resultado=$t->borrarInforme($idTra,$idUltimaModificacion);
			echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
		}
	

	break;

	case 'traerAlteracion':
		session_start();
		$respuesta=$_SESSION['alt_tramites_rapidos'];
		echo $respuesta;
	break;


	
	
}

?>