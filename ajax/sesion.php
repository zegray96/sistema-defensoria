<?php
session_start();
require_once '../modelos/Usuario.php';
$u=new Usuario();

switch ($_GET["op"]) {
	case 'verificar':
			$usuarioAcceso=isset($_POST["usuarioAcceso"])? limpiarCadena($_POST["usuarioAcceso"]):"";
			$claveAcceso=isset($_POST["claveAcceso"])? limpiarCadena($_POST["claveAcceso"]):"";
			//CLAVE HASH SHA256
			$claveHash=hash("SHA256",$claveAcceso);
			$resultado=$u->verificar($usuarioAcceso,$claveHash);
			$reg=$resultado->fetch_object();
			if(isset($reg)){
				//variables de sesion
				$_SESSION['idUsuarioDefPos']=$reg->id_usuario;
				$_SESSION['nombre']=$reg->nombre;
				$_SESSION['usuario']=$reg->usuario;
				$_SESSION['clave']=$reg->clave;
				$_SESSION['estado']=$reg->estado;
				$_SESSION['id_tipo_usuario']=$reg->id_tipo_usuario;
				$_SESSION['tipo_usuario']=$reg->tipo_usuario;
				$_SESSION['v_configuracion']=$reg->v_configuracion;
				$_SESSION['v_recursos_humanos']=$reg->v_recursos_humanos;
				$_SESSION['v_expedientes']=$reg->v_expedientes;
				$_SESSION['v_tramites_rapidos']=$reg->v_tramites_rapidos;
				$_SESSION['v_planchetas']=$reg->v_planchetas;
				$_SESSION['v_reclamos']=$reg->v_reclamos;
				$_SESSION['v_ref_pre_org']=$reg->v_ref_pre_org;
				$_SESSION['v_administracion']=$reg->v_administracion;
				$_SESSION['v_turnos']=$reg->v_turnos;
				$_SESSION['documentos_modelo']=$reg->documentos_modelo;
				$_SESSION['alt_configuracion']=$reg->alt_configuracion;
				$_SESSION['alt_recursos_humanos']=$reg->alt_recursos_humanos;
				$_SESSION['alt_expedientes']=$reg->alt_expedientes;
				$_SESSION['alt_tramites_rapidos']=$reg->alt_tramites_rapidos;
				$_SESSION['alt_planchetas']=$reg->alt_planchetas;
				$_SESSION['alt_reclamos']=$reg->alt_reclamos;
				$_SESSION['alt_ref_pre_org']=$reg->alt_ref_pre_org;
				$_SESSION['alt_administracion']=$reg->alt_administracion;
				$_SESSION['alt_turnos']=$reg->alt_turnos;
				$_SESSION['new_configuracion']=$reg->new_configuracion;
				$_SESSION['new_recursos_humanos']=$reg->new_recursos_humanos;
				$_SESSION['new_expedientes']=$reg->new_expedientes;
				$_SESSION['new_tramites_rapidos']=$reg->new_tramites_rapidos;
				$_SESSION['new_planchetas']=$reg->new_planchetas;
				$_SESSION['new_reclamos']=$reg->new_reclamos;
				$_SESSION['new_ref_pre_org']=$reg->new_ref_pre_org;
				$_SESSION['new_administracion']=$reg->new_administracion;
				$_SESSION['new_turnos']=$reg->new_turnos;
			}
			echo json_encode($reg);
		break;

		case 'salir':
		
			//limpiamos las variables de sesion
			session_unset();

			//destruimos la sesion
			session_destroy();

			
		break;
}
?>