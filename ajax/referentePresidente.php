<?php 

require_once '../modelos/ReferentePresidente.php';
$r=new ReferentePresidente();

$idRefPre=isset($_POST["idRefPre"])? limpiarCadena($_POST["idRefPre"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$apellidoNombre=isset($_POST["apellidoNombre"])? limpiarCadena($_POST["apellidoNombre"]):"";
$cargo=isset($_POST["cargo"])? limpiarCadena($_POST["cargo"]):"";
$barrio=isset($_POST["barrio"])? limpiarCadena($_POST["barrio"]):"";
$dni=isset($_POST["dni"])? limpiarCadena($_POST["dni"]):"";
$domicilio=isset($_POST["domicilio"])? limpiarCadena($_POST["domicilio"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";

switch ($_GET["op"]) {
	case 'guardarOeditar':
		
			

	
			if(empty($idRefPre)){
				$resultado=$r->insertar($apellidoNombre,$cargo,$barrio,$dni,$domicilio,$telefono,$email);
				echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
			}else{
				$resultado=$r->editar($idRefPre,$apellidoNombre,$cargo,$barrio,$dni,$domicilio,$telefono,$email);
				echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
			}
			
		
		


		
	break;
	
	case 'mostrar':
		$resultado=$r->buscar($idRefPre);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	
	case 'listar':
		session_start();
		$varAlteracionRefPreOrg=$_SESSION['alt_ref_pre_org'];
		$resultado=$r->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionRefPreOrg==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_ref_pre.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-danger" onclick="eliminar('.$reg->id_ref_pre.')"><i class="fas fa-trash-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->cargo=="REFERENTE"){
				$cargo='<span style="color:#0e900e; font-weight:bold">REFERENTE</span>';
			}else{
				if($reg->cargo=="PRESIDENTE DE BARRIO"){
					$cargo='<span style="color:#00458e; font-weight:bold">PRESIDENTE DE BARRIO</span>';
				}else{
					if($reg->cargo=="PRESIDENTE PRO TIERRA"){
						$cargo='<span style="color:#ce7a0e; font-weight:bold">PRESIDENTE PRO TIERRA</span>';
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$cargo,
				"2"=>$reg->barrio,
				"3"=>$reg->apellido_nombre,
				"4"=>$reg->dni,
				"5"=>$reg->domicilio,
				"6"=>$reg->telefono,
				"7"=>$reg->email,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'eliminar':
		
		
		$resultado=$r->eliminar($idRefPre);
		echo $resultado ? "¡Registro eliminado con exito!" : "¡Registro no se pudo eliminar!";
		
	break;

	
	

	


	
	
}
?>