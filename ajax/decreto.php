<?php 

require_once '../modelos/Decreto.php';
$d=new Decreto();

$idDecreto=isset($_POST["idDecreto"])? limpiarCadena($_POST["idDecreto"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$nroDecreto=isset($_POST["nroDecreto"])? limpiarCadena($_POST["nroDecreto"]):"";
$anio=isset($_POST["anio"])? limpiarCadena($_POST["anio"]):"";
$fecha=isset($_POST["fecha"])? limpiarCadena($_POST["fecha"]):"";
$detalle=isset($_POST["detalle"])? limpiarCadena($_POST["detalle"]):"";
$estado=isset($_POST["estado"])? limpiarCadena($_POST["estado"]):"";
$area=isset($_POST["area"])? limpiarCadena($_POST["area"]):"";
$idAgente=isset($_POST["idAgente"])? limpiarCadena($_POST["idAgente"]):"";

switch ($_GET["op"]) {
	case 'guardarOeditar':
		session_start();
		$ultimaModificacion=$_SESSION['idUsuarioDefPos'];



		$resultadoNro=null;
		$nroYanio=$nroDecreto.$anio;
		$nroYanioOriginal="";

		if(!empty($idDecreto)){
			$resultado=$d->buscarId($idDecreto);
			while($reg=$resultado->fetch_object()){
				$nroYanioOriginal=$reg->nro_decreto.$reg->anio;
			}   
		}
		if($nroYanio!=$nroYanioOriginal){
			$resultadoNro=$d->comprobarExistenciaNro($nroDecreto,$anio);
		}

		if(!empty($resultadoNro)){
			echo "¡N° de decreto ya existe!";
		}else{

			if(empty($idDecreto)){
				$resultado=$d->insertar($nroDecreto,$anio,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion);
				echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
			}else{
				$resultado=$d->editar($idDecreto,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion);
				echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
			}
		}		
			
		
	break;
	
	case 'mostrar':
		$resultado=$d->buscar($idDecreto);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	
	case 'listar':
		session_start();
		$varAlteracionAdministracion=$_SESSION['alt_administracion'];
		$resultado=$d->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionAdministracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_decreto.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="CONCLUIDO"){
				$estado='<span class="label bg-green">'.$reg->estado.'</span>';
			}else{
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}


			if($reg->area=="ADMINISTRACION"){
				$area='<span style="color:#ce7a0e; font-weight:bold">ADMINISTRACION</span>';
			}else{	
				if ($reg->area=="JURIDICO") {
					$area='<span style="color:#00458e; font-weight:bold">JURIDICO</span>';
				}else{
					if ($reg->area=="RECURSOS HUMANOS") {
						$area='<span style="color:#A00303; font-weight:bold">RECURSOS HUMANOS</span>';
					}
				}
				
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nro_decreto,
				"2"=>$reg->anio,
				"3"=>$reg->fecha,
				"4"=>$estado,
				"5"=>$area,
				"6"=>$reg->detalle,
				"7"=>$reg->agente,
				"8"=>$reg->ultimaModificacion,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listarPorAnio':
		session_start();
		$varAlteracionAdministracion=$_SESSION['alt_administracion'];
		$varAnio=$_REQUEST['varAnio'];
		$resultado=$d->listarPorAnio($varAnio);
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionAdministracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_decreto.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="CONCLUIDO"){
				$estado='<span class="label bg-green">'.$reg->estado.'</span>';
			}else{
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}


			if($reg->area=="ADMINISTRACION"){
				$area='<span style="color:#ce7a0e; font-weight:bold">ADMINISTRACION</span>';
			}else{	
				if ($reg->area=="JURIDICO") {
					$area='<span style="color:#00458e; font-weight:bold">JURIDICO</span>';
				}else{
					if ($reg->area=="RECURSOS HUMANOS") {
						$area='<span style="color:#A00303; font-weight:bold">RECURSOS HUMANOS</span>';
					}
				}
				
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nro_decreto,
				"2"=>$reg->anio,
				"3"=>$reg->fecha,
				"4"=>$estado,
				"5"=>$area,
				"6"=>$reg->detalle,
				"7"=>$reg->agente,
				"8"=>$reg->ultimaModificacion,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'ultimoNro':
		$anio=$_REQUEST['anio'];
		$resultado=$d->ultimoNro($anio);
		echo json_encode($resultado);
	break;

	case 'buscarPorNro':
		session_start();
		$varAlteracionAdministracion=$_SESSION['alt_administracion'];
		$varNroDecreto=$_REQUEST['varNroDecreto'];
		$resultado=$d->buscarPorNro($varNroDecreto);
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionAdministracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_decreto.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="CONCLUIDO"){
				$estado='<span class="label bg-green">'.$reg->estado.'</span>';
			}else{
				$estado='<span class="label bg-red">'.$reg->estado.'</span>';
			}


			if($reg->area=="ADMINISTRACION"){
				$area='<span style="color:#ce7a0e; font-weight:bold">ADMINISTRACION</span>';
			}else{	
				if ($reg->area=="JURIDICO") {
					$area='<span style="color:#00458e; font-weight:bold">JURIDICO</span>';
				}else{
					if ($reg->area=="RECURSOS HUMANOS") {
						$area='<span style="color:#A00303; font-weight:bold">RECURSOS HUMANOS</span>';
					}
				}
				
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nro_decreto,
				"2"=>$reg->fecha,
				"3"=>$estado,
				"4"=>$area,
				"5"=>$reg->detalle,
				"6"=>$reg->agente,
				"7"=>$reg->ultimaModificacion,
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'selectAgente':
		require_once '../modelos/Agente.php';
		$a=new Agente();
		$resultado=$a->select();
		$opcion="[SELECCIONAR]";
		echo '<option selected="true" disabled="disabled">'.$opcion.'</option>';
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->id_agente.'>'.$reg->nombre.'</option>';
		}
	break;


	

	


	
	
}
?>