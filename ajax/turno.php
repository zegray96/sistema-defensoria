<?php 

require_once '../modelos/Turno.php';
$t=new Turno();

$idTurno=isset($_POST["idTurno"])? limpiarCadena($_POST["idTurno"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$apellidoNombre=isset($_POST["apellidoNombre"])? limpiarCadena($_POST["apellidoNombre"]):"";
$dni=isset($_POST["dni"])? limpiarCadena($_POST["dni"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";
$motivo=isset($_POST["motivo"])? limpiarCadena($_POST["motivo"]):"";
$fecha=isset($_POST["fecha"])? limpiarCadena($_POST["fecha"]):"";
$hora=isset($_POST["hora"])? limpiarCadena($_POST["hora"]):"";

switch ($_GET["op"]) {

	// TURNOS INTERNO
	case 'listarCalendario':
		$resultado=$t->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			$data[]=array(
				"id_turno"=>$reg->id_turno,
				"title"=>$reg->title,
				"apellido_nombre"=>$reg->apellido_nombre,
				"dni"=>$reg->dni,
				"telefono"=>$reg->telefono,
				"email"=>$reg->email,
				"motivo"=>$reg->motivo,
				"start"=>$reg->start,
				
			);

		}

		
		echo json_encode($data);
	break;


	case 'listarHoy':
		session_start();
		date_default_timezone_set('America/Argentina/Buenos_Aires'); //importante aclarar la ubicacion, sino toma otra fecha
		$varAlteracionTurnos=$_SESSION['alt_turnos'];
		$fechaActual = date('Y-m-d');
		$resultado=$t->listarPorFecha($fechaActual);
		
			

		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionTurnos==1){
				$opciones='<button class="btn btn-danger" onclick="eliminarDesdeTabla('.$reg->id_turno.')"><i class="fas fa-trash-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$fechaHora = explode(" ", $reg->start);
			$hora = substr($fechaHora[1], 0, 5);
			$data[]=array(
				"0"=>$opciones,
				"1"=>$fechaHora[0],
				"2"=>'<span class="label bg-blue" style="font-size:15px;">'.$hora.'</span>',
				"3"=>$reg->apellido_nombre,
				"4"=>$reg->dni,
				"5"=>$reg->telefono,
				"6"=>$reg->email,
				"7"=>$reg->motivo,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;
	

	case 'listarTabla':
		session_start();
		$varAlteracionTurnos=$_SESSION['alt_turnos'];
		$resultado=$t->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionTurnos==1){
				$opciones='<button class="btn btn-danger" onclick="eliminarDesdeTabla('.$reg->id_turno.')"><i class="fas fa-trash-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$fechaHora = explode(" ", $reg->start);
			$hora = substr($fechaHora[1], 0, 5);
			$data[]=array(
				"0"=>$opciones,
				"1"=>$fechaHora[0],
				"2"=>'<span class="label bg-blue" style="font-size:15px;">'.$hora.'</span>',
				"3"=>$reg->apellido_nombre,
				"4"=>$reg->dni,
				"5"=>$reg->telefono,
				"6"=>$reg->email,
				"7"=>$reg->motivo,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'guardarOeditar':
				
				// COMPROBAMOS DIPONIBILIDAD NUEVAMENTE LA HORA
				$disponible='¡Horario ya fue reservado! Elija otro..';
				
				$fechaHora=$fecha.' '.$hora;
				$resultado=$t->radioHora($fechaHora);
				if(empty($resultado)){
					$disponible='si';
				}			


				// SI ESTA DIPONIBLE GUARDAMOS
				if($disponible=="si"){
					
					// SI ESTA TODO OK GUARDAMOS	
					if(empty($idTurno)){
						$resultado=$t->insertar($apellidoNombre,$dni,$telefono,$email,$motivo,$fecha,$hora);
						echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
					}else{
						$resultado=$t->editar($idTurno,$apellidoNombre,$dni,$telefono,$email,$motivo,$fecha,$hora);
						echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
					}

				}else{
					echo $disponible;
				}
			

	break;


	case 'eliminar':
		$idTurno=$_REQUEST['idTurno'];
		$resultado=$t->eliminar($idTurno);
		echo $resultado ? "¡Registro eliminado con exito!" : "¡Registro no se pudo eliminar!";
			

	break;

	case 'eliminarDesdeTabla':
		$resultado=$t->eliminar($idTurno);
		echo $resultado ? "¡Registro eliminado con exito!" : "¡Registro no se pudo eliminar!";
	break;

	case 'radioHora':
		
		$fecha=$_REQUEST['fecha'];
		$disponible=false;
		for ($i = 9; $i <= 11; $i++) {
			$numeroConCeros = str_pad($i, 2, "0", STR_PAD_LEFT);
			$hora=$numeroConCeros.":00";
			$fechaHora=$fecha.' '.$hora;
			$resultado=$t->radioHora($fechaHora);
			if(empty($resultado)){
				echo '<label class="radio-inline"><input type="radio" name="hora" value='.$hora.' required>'.$hora.'</label>';
				$disponible=true;
			}

			
		}

		if(!$disponible){
			echo '<span>No hay horarios disponibles</span>';
		}
		
	break;


	

	case 'radioHoraEditar':
		$fechaOriginal=$_REQUEST['fechaOriginal'];
		$horaOriginal=$_REQUEST['horaOriginal'];
		$fecha=$_REQUEST['fecha'];
		$disponible=false;

		
		
			for ($i = 9; $i <= 11; $i++) {
				$numeroConCeros = str_pad($i, 2, "0", STR_PAD_LEFT);
				$hora=$numeroConCeros.":00";
				$fechaHora=$fecha.' '.$hora;
				$resultado=$t->radioHora($fechaHora);
				if(empty($resultado)){
					echo '<label class="radio-inline"><input type="radio" name="hora" value='.$hora.' required>'.$hora.'</label>';
					$disponible=true;
				}


			}
			if($fecha==$fechaOriginal){
				echo '<label class="radio-inline"><input checked type="radio" name="hora" value='.$horaOriginal.' required>'.$horaOriginal.'</label>';
			}else{
				if(!$disponible){
					echo '<span>No hay horarios disponibles</span>';
				}
			}
			
		
		
		
	break;


	case 'radioHoraEvento':
		
		$fecha=$_REQUEST['fecha'];
		for ($i = 9; $i <= 11; $i++) {
			$numeroConCeros = str_pad($i, 2, "0", STR_PAD_LEFT);
			$hora=$numeroConCeros.":00";
			$fechaHora=$fecha.' '.$hora;
			$resultado=$t->radioHora($fechaHora);
			if(empty($resultado)){
				echo '<label class="radio-inline"><input type="radio" name="hora" value='.$hora.' required>'.$hora.'</label>';
			}

			
		}
		
	break;

	case 'traerAlteracion':
		session_start();
		$respuesta=$_SESSION['alt_turnos'];
		echo $respuesta;
	break;

	case 'traerNuevo':
		session_start();
		$respuesta=$_SESSION['new_turnos'];
		echo $respuesta;
	break;


	// TURNOS EXTERNO
	case 'botonesHorario':
		
		$fecha=$_REQUEST['fecha'];
		for ($i = 9; $i <= 11; $i++) {
			$numeroConCeros = str_pad($i, 2, "0", STR_PAD_LEFT);
			$hora=$numeroConCeros.":00";
			$fechaHora=$fecha.' '.$hora;
			$resultado=$t->radioHora($fechaHora);
			if(empty($resultado)){
				echo '<input style="margin-right:10px;" type="button" id="h'.$numeroConCeros.'" class="btn btn-default" onclick="colorearHora('.$numeroConCeros.')" value='.$hora.'>';
			}

			
		}
		
	break;

	case 'guardar':
			$anio = substr($fecha, 6, 4); 
			$mes = substr($fecha, 3, 2);
			$dia = substr($fecha, 0, 2);  
			$fechaEntera = $anio.'-'.$mes.'-'.$dia;

			// COMPROBAMOS DIPONIBILIDAD NUEVAMENTE LA HORA
			$disponible='¡Horario ya fue reservado! Elija otro..';
			
			$fechaHora=$fechaEntera.' '.$hora;
			$resultado=$t->radioHora($fechaHora);
			if(empty($resultado)){
				$disponible='si';
			}			
			

			// SI ESTA DIPONIBLE GUARDAMOS
			if($disponible=="si"){
				// COMPROBAMOS CAMPOS OBLIGATORIOS DE NUEVO
				if($apellidoNombre=="" || $dni=="" || $telefono=="" || $fechaEntera=="" || $hora=="" || $motivo==""){
					echo '¡Complete campos obligatorios!';
				}else{
					// SI ESTA TODO OK GUARDAMOS
					$resultado=$t->insertar($apellidoNombre,$dni,$telefono,$email,$motivo,$fechaEntera,$hora);
					echo $resultado ? "¡Turno agendado con exito!" : "¡Turno no se pudo agendar!";
				}
			}else{
				echo $disponible;
			}
			
			
			
			
	break;
	

	// PARA INTERNO Y EXTERNO
	case 'disponibilidad':
		
		$fecha=$_REQUEST['fecha'];
		$disponible='¡No existen horarios disponibles en la fecha seleccionada!';
		for ($i = 9; $i <= 11; $i++) {
			$numeroConCeros = str_pad($i, 2, "0", STR_PAD_LEFT);
			$hora=$numeroConCeros.":00";
			$fechaHora=$fecha.' '.$hora;
			$resultado=$t->radioHora($fechaHora);
			if(empty($resultado)){
				$disponible='si';
			}			
		}
		echo $disponible;
		
	break;
}

	

?>