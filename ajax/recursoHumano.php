<?php 
require_once '../modelos/RecursoHumano.php';
$r=new RecursoHumano();

$idPersonal=isset($_POST["idPersonal"])? limpiarCadena($_POST["idPersonal"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$apellidoNombre=isset($_POST["apellidoNombre"])? limpiarCadena($_POST["apellidoNombre"]):"";
$dni=isset($_POST["dni"])? limpiarCadena($_POST["dni"]):"";
$cuil=isset($_POST["cuil"])? limpiarCadena($_POST["cuil"]):"";
$situacionRevista=isset($_POST["situacionRevista"])? limpiarCadena($_POST["situacionRevista"]):"";
$categoria=isset($_POST["categoria"])? limpiarCadena($_POST["categoria"]):"";
$nroLegajo=isset($_POST["nroLegajo"])? limpiarCadena($_POST["nroLegajo"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$domicilio=isset($_POST["domicilio"])? limpiarCadena($_POST["domicilio"]):"";
$imagenDniAdelanteActual=isset($_POST["imagenDniAdelanteActual"])? limpiarCadena($_POST["imagenDniAdelanteActual"]):"";
$imagenDniAtrasActual=isset($_POST["imagenDniAtrasActual"])? limpiarCadena($_POST["imagenDniAtrasActual"]):"";







switch ($_GET["op"]) {
	case 'guardarOeditar':
					session_start();
					$ultimaModificacion=$_SESSION['idUsuarioDefPos'];

					$imagenDniAdelante=$imagenDniAdelanteActual;
					$imagenDniAtras=$imagenDniAtrasActual;
					
					//guardando imagenes si se cargaron
					if(is_uploaded_file($_FILES['imagenDniAdelante']['tmp_name'])){
						$imagenDniAdelante=$dni.'_adelante';
						move_uploaded_file($_FILES['imagenDniAdelante']['tmp_name'], "../files/imagenesDni/".$imagenDniAdelante);					
					}

					if(is_uploaded_file($_FILES['imagenDniAtras']['tmp_name'])){
						$imagenDniAtras=$dni.'_atras';
						move_uploaded_file($_FILES['imagenDniAtras']['tmp_name'], "../files/imagenesDni/".$imagenDniAtras);					
					}

					//nuevo y editar
					if(empty($idPersonal)){
						$resultado=$r->insertar($apellidoNombre,$dni,$cuil,$situacionRevista,$categoria,$nroLegajo,$email,$telefono,$domicilio,$imagenDniAdelante,$imagenDniAtras,$ultimaModificacion);
						echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
					}else{
						$resultado=$r->editar($idPersonal,$apellidoNombre,$dni,$cuil,$situacionRevista,$categoria,$nroLegajo,$email,$telefono,$domicilio,$imagenDniAdelante,$imagenDniAtras,$ultimaModificacion);
						echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
					}
				
			

			
	break;
	
	case 'mostrar':
		$resultado=$r->buscar($idPersonal);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	case 'eliminar':
		$resultado=$r->buscarIdParaBorrar($idPersonal);
		while($reg=$resultado->fetch_object()){
			//eliminamos el archivo si tuviese uno (adelante)
			if($reg->imagen_dni_adelante!=""){
				$ubicacion='..\files\imagenesDni\\'.$reg->imagen_dni_adelante; //borra el archivo en la carpeta
				unlink($ubicacion);
			}
			//eliminamos el archivo si tuviese uno (atras)
			if($reg->imagen_dni_atras!=""){
				$ubicacion='..\files\imagenesDni\\'.$reg->imagen_dni_atras; //borra el archivo en la carpeta
				unlink($ubicacion);
			}
			
		}                     
		//eliminamos el registro en la bd
		$resultado=$r->eliminar($idPersonal);
		echo $resultado ? "¡Registro Eliminado con Exito!" : "¡Registro no se pudo Eliminar!";
		
	break;

	case 'listar':
		session_start();
		$varAlteracionRecursosHumanos=$_SESSION['alt_recursos_humanos'];
		$resultado=$r->listar();
			
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionRecursosHumanos==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_personal.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-danger" onclick="eliminar('.$reg->id_personal.')"><i class="fas fa-trash-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			//si tiene guardado un archivo lo muestra, sino no
			// utilizo un nro random para que no detecte la cache la misma imagen
			if(!$reg->imagen_dni_adelante==""){
				$dniAde="<a href='../files/imagenesDni/".$reg->imagen_dni_adelante."?".rand()."' target='_blank'><img src='../files/imagenesDni/".$reg->imagen_dni_adelante."?".rand()."' width='170px' height='100px'></i></a>";
			}else{
				$dniAde="";
			}

			if(!$reg->imagen_dni_atras==""){
				$dniAtr="<a href='../files/imagenesDni/".$reg->imagen_dni_atras."?".rand()."' target='_blank'><img src='../files/imagenesDni/".$reg->imagen_dni_atras."?".rand()."' width='170px' height='100px'></i></a>";
			}else{
				$dniAtr="";
			}

			if($reg->situacion_revista=="MONOTRIBUTISTA"){
				$situacionRevista="<span class='label bg-orange'>".$reg->situacion_revista."</span>";
			}else{
				if($reg->situacion_revista=="PERSONAL PERMANENTE"){
					$situacionRevista="<span class='label bg-green'>".$reg->situacion_revista."</span>";
				}else{
					if($reg->situacion_revista=="PERSONAL TEMPORARIO"){
						$situacionRevista="<span class='label bg-blue'>".$reg->situacion_revista."</span>";
					}else{
						$situacionRevista=$reg->situacion_revista;
					}
				}
			}



			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->cuil,
				"4"=>$situacionRevista,
				"5"=>$reg->categoria,
				"6"=>$reg->nro_legajo,
				"7"=>$reg->email,
				"8"=>$reg->telefono,
				"9"=>$reg->domicilio,
				"10"=>$dniAde,
				"11"=>$dniAtr,
				"12"=>$reg->ultimaModificacion,
				
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	


	
	
}
?>