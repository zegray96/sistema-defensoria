<?php 
require_once '../modelos/Reclamo.php';
$r=new Reclamo();

$idReclamo=isset($_POST["idReclamo"])? limpiarCadena($_POST["idReclamo"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$fecha=isset($_POST["fecha"])? limpiarCadena($_POST["fecha"]):"";
$apellidoNombre=isset($_POST["apellidoNombre"])? limpiarCadena($_POST["apellidoNombre"]):"";
$dni=isset($_POST["dni"])? limpiarCadena($_POST["dni"]):"";
$domicilio=isset($_POST["domicilio"])? limpiarCadena($_POST["domicilio"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";
$reclamo=isset($_POST["reclamo"])? limpiarCadena($_POST["reclamo"]):"";
$via=isset($_POST["via"])? limpiarCadena($_POST["via"]):"";
$viaOpcion=isset($_POST["viaOpcion"])? limpiarCadena($_POST["viaOpcion"]):"";
$respuesta=isset($_POST["respuesta"])? limpiarCadena($_POST["respuesta"]):"";
$tramite=isset($_POST["tramite"])? limpiarCadena($_POST["tramite"]):"";

switch ($_GET["op"]) {
	case 'guardarOeditarWeb':
	
				// controlamos que tengan valores los campos
				if($apellidoNombre=="" || $dni=="" || $domicilio=="" || $telefono=="" || $reclamo==""){
					echo '¡Complete campos obligatorios!';
				}else{
					date_default_timezone_set('America/Argentina/Buenos_Aires');
					$fecha = date('Y-m-d');
					/*$hora=date('H:i:s');*/
					$via="FORMULARIO WEB";
					$estado="EN ESPERA DE RESPUESTA";
					$resultado=$r->insertarWeb($apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$fecha,$estado,$via);
					echo $resultado;
				}
				
				
				
	
	break;


	case 'guardarOeditarSistema':
				session_start();
				$ultimaModificacion=$_SESSION['idUsuarioDefPos'];

				if($respuesta==""){
					$estado="EN ESPERA DE RESPUESTA";
				}else{
					$estado="RESPONDIDO";
				}

				//nuevo
				if(empty($idReclamo)){
					$resultado=$r->insertarSistema($apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$respuesta,$tramite,$fecha,$estado,$via,$ultimaModificacion);
					echo $resultado;
				}else{
					if($via==""){ //si viene vacio es porque es Formulario web
						$resultado=$r->editarWeb($idReclamo,$respuesta,$tramite,$estado,$ultimaModificacion);
						echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
					}else{
						$resultado=$r->editarSistema($idReclamo,$apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$respuesta,$tramite,$via,$estado,$ultimaModificacion);
						echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
					}
				}
				
	
	break;


	case 'listarPorFecha':
		session_start();
		$varAlteracionReclamos=$_SESSION['alt_reclamos'];
		$varFechaIniListar=$_REQUEST['varFechaIniListar'];
		$varFechaFinListar=$_REQUEST['varFechaFinListar'];

		$resultado=$r->listarEntreFechas($varFechaIniListar,$varFechaFinListar);
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN ESPERA DE RESPUESTA"){
				$estado='<span class="label bg-red">EN ESPERA DE RESPUESTA</span>';
			}else{
				if($reg->estado=="RESPONDIDO"){
					$estado='<span class="label bg-green">RESPONDIDO</span>';
				}
			}
			
			if($varAlteracionReclamos==1){
				$opciones=' <button class="btn btn-warning" onclick="mostrar('.$reg->id_reclamo.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimirFormulario('.$reg->id_reclamo.')"><i class="fas fa-print"></i></button>';
			}else{
				$opciones='';
			}

			if($reg->via=="FORMULARIO WEB"){
				$via='<span style="color:#40b8ab; font-weight:bold">FORMULARIO WEB</span>';
			}else{
				if($reg->via=="WHATSAPP"){
					$via='<span style="color:#0e900e; font-weight:bold">WHATSAPP</span>';
				}else{
					if($reg->via=="TELEFONO"){
						$via='<span style="color:#ce7a0e; font-weight:bold">TELEFONO</span>';
					}else{
						if($reg->via=="EMAIL"){
							$via='<span style="color:#00458e; font-weight:bold">EMAIL</span>';
						}else{
							if($reg->via=="VISITA"){
								$via='<span style="color:#a00094; font-weight:bold">VISITA</span>';
							}
						}
					}	
				}
			}

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_reclamo,
				"1"=>$opciones,
				"2"=>$reg->nro_reclamo,
				"3"=>$estado,
				"4"=>$via,
				"5"=>$fechaFormateada,
				"6"=>$reg->apellido_nombre,
				"7"=>$reg->dni,
				"8"=>"<p id='rec".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->reclamo."</p> <a style='cursor:pointer;' id='verRec".$reg->id_reclamo."' onclick='verMasReclamo(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRec".$reg->id_reclamo."' onclick='ocultarTextReclamo(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"9"=>"<p id='res".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->respuesta."</p> <a style='cursor:pointer;' id='verRes".$reg->id_reclamo."' onclick='verMasRespuesta(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRes".$reg->id_reclamo."' onclick='ocultarTextRespuesta(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"10"=>"<p id='tra".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->tramite."</p> <a style='cursor:pointer;' id='verTra".$reg->id_reclamo."' onclick='verMasTramite(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuTra".$reg->id_reclamo."' onclick='ocultarTextTramite(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"11"=>$reg->domicilio,
				"12"=>$reg->telefono,
				"13"=>$reg->email,
				"14"=>$reg->ultimaModificacion,
				

			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listarHoy':
		session_start();
		date_default_timezone_set('America/Argentina/Buenos_Aires'); //importante aclarar la ubicacion, sino toma otra fecha
		$varAlteracionReclamos=$_SESSION['alt_reclamos'];
		$fechaActual = date('Y-m-d');
		$resultado=$r->listarPorFecha($fechaActual);
		
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN ESPERA DE RESPUESTA"){
				$estado='<span class="label bg-red">EN ESPERA DE RESPUESTA</span>';
			}else{
				if($reg->estado=="RESPONDIDO"){
					$estado='<span class="label bg-green">RESPONDIDO</span>';
				}
			}
			
			if($varAlteracionReclamos==1){
				$opciones=' <button class="btn btn-warning" onclick="mostrar('.$reg->id_reclamo.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimirFormulario('.$reg->id_reclamo.')"><i class="fas fa-print"></i></button>';
			}else{
				$opciones='';
			}


			if($reg->via=="FORMULARIO WEB"){
				$via='<span style="color:#40b8ab; font-weight:bold">FORMULARIO WEB</span>';
			}else{
				if($reg->via=="WHATSAPP"){
					$via='<span style="color:#0e900e; font-weight:bold">WHATSAPP</span>';
				}else{
					if($reg->via=="TELEFONO"){
						$via='<span style="color:#ce7a0e; font-weight:bold">TELEFONO</span>';
					}else{
						if($reg->via=="EMAIL"){
							$via='<span style="color:#00458e; font-weight:bold">EMAIL</span>';
						}else{
							if($reg->via=="VISITA"){
								$via='<span style="color:#a00094; font-weight:bold">VISITA</span>';
							}
						}
					}	
				}
			}

			

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_reclamo,
				"1"=>$opciones,
				"2"=>$reg->nro_reclamo,
				"3"=>$estado,
				"4"=>$via,
				"5"=>$fechaFormateada,
				"6"=>$reg->apellido_nombre,
				"7"=>$reg->dni,
				"8"=>"<p id='rec".$reg->id_reclamo."' style='width: 400px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->reclamo."</p> <a style='cursor:pointer;' id='verRec".$reg->id_reclamo."' onclick='verMasReclamo(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRec".$reg->id_reclamo."' onclick='ocultarTextReclamo(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"9"=>"<p id='res".$reg->id_reclamo."' style='width: 350px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->respuesta."</p> <a style='cursor:pointer;' id='verRes".$reg->id_reclamo."' onclick='verMasRespuesta(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRes".$reg->id_reclamo."' onclick='ocultarTextRespuesta(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"10"=>"<p id='tra".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->tramite."</p> <a style='cursor:pointer;' id='verTra".$reg->id_reclamo."' onclick='verMasTramite(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuTra".$reg->id_reclamo."' onclick='ocultarTextTramite(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"11"=>$reg->domicilio,
				"12"=>$reg->telefono,
				"13"=>$reg->email,
				"14"=>$reg->ultimaModificacion,

			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;




	case 'listarEnEspera':
		session_start();
		$varAlteracionReclamos=$_SESSION['alt_reclamos'];
		$resultado=$r->listarEnEspera();
		
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN ESPERA DE RESPUESTA"){
				$estado='<span class="label bg-red">EN ESPERA DE RESPUESTA</span>';
			}else{
				if($reg->estado=="RESPONDIDO"){
					$estado='<span class="label bg-green">RESPONDIDO</span>';
				}
			}
			
			if($varAlteracionReclamos==1){
				$opciones=' <button class="btn btn-warning" onclick="mostrar('.$reg->id_reclamo.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimirFormulario('.$reg->id_reclamo.')"><i class="fas fa-print"></i></button>';
			}else{
				$opciones='';
			}


			if($reg->via=="FORMULARIO WEB"){
				$via='<span style="color:#40b8ab; font-weight:bold">FORMULARIO WEB</span>';
			}else{
				if($reg->via=="WHATSAPP"){
					$via='<span style="color:#0e900e; font-weight:bold">WHATSAPP</span>';
				}else{
					if($reg->via=="TELEFONO"){
						$via='<span style="color:#ce7a0e; font-weight:bold">TELEFONO</span>';
					}else{
						if($reg->via=="EMAIL"){
							$via='<span style="color:#00458e; font-weight:bold">EMAIL</span>';
						}else{
							if($reg->via=="VISITA"){
								$via='<span style="color:#a00094; font-weight:bold">VISITA</span>';
							}
						}
					}	
				}
			}

			

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_reclamo,
				"1"=>$opciones,
				"2"=>$reg->nro_reclamo,
				"3"=>$estado,
				"4"=>$via,
				"5"=>$fechaFormateada,
				"6"=>$reg->apellido_nombre,
				"7"=>$reg->dni,
				"8"=>"<p id='rec".$reg->id_reclamo."' style='width: 400px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->reclamo."</p> <a style='cursor:pointer;' id='verRec".$reg->id_reclamo."' onclick='verMasReclamo(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRec".$reg->id_reclamo."' onclick='ocultarTextReclamo(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"9"=>"<p id='res".$reg->id_reclamo."' style='width: 350px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->respuesta."</p> <a style='cursor:pointer;' id='verRes".$reg->id_reclamo."' onclick='verMasRespuesta(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRes".$reg->id_reclamo."' onclick='ocultarTextRespuesta(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"10"=>"<p id='tra".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->tramite."</p> <a style='cursor:pointer;' id='verTra".$reg->id_reclamo."' onclick='verMasTramite(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuTra".$reg->id_reclamo."' onclick='ocultarTextTramite(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"11"=>$reg->domicilio,
				"12"=>$reg->telefono,
				"13"=>$reg->email,
				"14"=>$reg->ultimaModificacion,


			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'buscarPorNro':
		session_start();
		$varAlteracionReclamos=$_SESSION['alt_reclamos'];
		$varNroReclamo=$_REQUEST['varNroReclamo'];
		$resultado=$r->buscarPorNro($varNroReclamo);
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN ESPERA DE RESPUESTA"){
				$estado='<span class="label bg-red">EN ESPERA DE RESPUESTA</span>';
			}else{
				if($reg->estado=="RESPONDIDO"){
					$estado='<span class="label bg-green">RESPONDIDO</span>';
				}
			}
			
			if($varAlteracionReclamos==1){
				$opciones=' <button class="btn btn-warning" onclick="mostrar('.$reg->id_reclamo.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimirFormulario('.$reg->id_reclamo.')"><i class="fas fa-print"></i></button>';
			}else{
				$opciones='';
			}

			if($reg->via=="FORMULARIO WEB"){
				$via='<span style="color:#40b8ab; font-weight:bold">FORMULARIO WEB</span>';
			}else{
				if($reg->via=="WHATSAPP"){
					$via='<span style="color:#0e900e; font-weight:bold">WHATSAPP</span>';
				}else{
					if($reg->via=="TELEFONO"){
						$via='<span style="color:#ce7a0e; font-weight:bold">TELEFONO</span>';
					}else{
						if($reg->via=="EMAIL"){
							$via='<span style="color:#00458e; font-weight:bold">EMAIL</span>';
						}else{
							if($reg->via=="VISITA"){
								$via='<span style="color:#a00094; font-weight:bold">VISITA</span>';
							}
						}
					}	
				}
			}

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_reclamo,
				"1"=>$opciones,
				"2"=>$reg->nro_reclamo,
				"3"=>$estado,
				"4"=>$via,
				"5"=>$fechaFormateada,
				"6"=>$reg->apellido_nombre,
				"7"=>$reg->dni,
				"8"=>"<p id='rec".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->reclamo."</p> <a style='cursor:pointer;' id='verRec".$reg->id_reclamo."' onclick='verMasReclamo(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRec".$reg->id_reclamo."' onclick='ocultarTextReclamo(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"9"=>"<p id='res".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->respuesta."</p> <a style='cursor:pointer;' id='verRes".$reg->id_reclamo."' onclick='verMasRespuesta(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuRes".$reg->id_reclamo."' onclick='ocultarTextRespuesta(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"10"=>"<p id='tra".$reg->id_reclamo."' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>".$reg->tramite."</p> <a style='cursor:pointer;' id='verTra".$reg->id_reclamo."' onclick='verMasTramite(".$reg->id_reclamo.")'>(Ver mas)</a> <a id='ocuTra".$reg->id_reclamo."' onclick='ocultarTextTramite(".$reg->id_reclamo.")' style='display:none; cursor:pointer;' >(Ocultar)</a>",
				"11"=>$reg->domicilio,
				"12"=>$reg->telefono,
				"13"=>$reg->email,
				"14"=>$reg->ultimaModificacion,

			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'mostrar':
		$resultado=$r->buscar($idReclamo);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	case 'buscarNroReclamo':
		$nroReclamoBuscar=isset($_POST["nroReclamoBuscar"])? limpiarCadena($_POST["nroReclamoBuscar"]):"";
		$resultado=$r->buscarNroReclamo($nroReclamoBuscar);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	case 'horaActual':
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		echo date('H:i');
	break;

	case 'traerAlteracion':
		session_start();
		$respuesta=$_SESSION['alt_reclamos'];
		echo $respuesta;
	break;

	




	
	
}
?>