<?php 
require_once '../modelos/TipoUsuario.php';
$t=new TipoUsuario();

$idTipoUsuario=isset($_POST["idTipoUsuario"])? limpiarCadena($_POST["idTipoUsuario"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$vistaConfiguracion=isset($_POST["vistaConfiguracion"])? 1:0;
$vistaRecursosHumanos=isset($_POST["vistaRecursosHumanos"])? 1:0;
$vistaExpedientes=isset($_POST["vistaExpedientes"])? 1:0;
$vistaTramitesRapidos=isset($_POST["vistaTramitesRapidos"])? 1:0;
$vistaPlanchetas=isset($_POST["vistaPlanchetas"])? 1:0;
$vistaReclamos=isset($_POST["vistaReclamos"])? 1:0;
$vistaRefPreOrg=isset($_POST["vistaRefPreOrg"])? 1:0;
$vistaAdministracion=isset($_POST["vistaAdministracion"])? 1:0;
$vistaTurnos=isset($_POST["vistaTurnos"])? 1:0;
$documentosModelo=isset($_POST["documentosModelo"])? 1:0;
$altConfiguracion=isset($_POST["altConfiguracion"])? 1:0;
$altRecursosHumanos=isset($_POST["altRecursosHumanos"])? 1:0;
$altExpedientes=isset($_POST["altExpedientes"])? 1:0;
$altTramitesRapidos=isset($_POST["altTramitesRapidos"])? 1:0;
$altPlanchetas=isset($_POST["altPlanchetas"])? 1:0;
$altReclamos=isset($_POST["altReclamos"])? 1:0;
$altRefPreOrg=isset($_POST["altRefPreOrg"])? 1:0;
$altAdministracion=isset($_POST["altAdministracion"])? 1:0;
$altTurnos=isset($_POST["altTurnos"])? 1:0;
$newConfiguracion=isset($_POST["newConfiguracion"])? 1:0;
$newRecursosHumanos=isset($_POST["newRecursosHumanos"])? 1:0;
$newExpedientes=isset($_POST["newExpedientes"])? 1:0;
$newTramitesRapidos=isset($_POST["newTramitesRapidos"])? 1:0;
$newPlanchetas=isset($_POST["newPlanchetas"])? 1:0;
$newReclamos=isset($_POST["newReclamos"])? 1:0;
$newRefPreOrg=isset($_POST["newRefPreOrg"])? 1:0;
$newAdministracion=isset($_POST["newAdministracion"])? 1:0;
$newTurnos=isset($_POST["newTurnos"])? 1:0;


switch ($_GET["op"]) {
	case 'guardarOeditar':
			if($vistaConfiguracion==0 && $vistaRecursosHumanos==0 && $vistaExpedientes==0 && $vistaTramitesRapidos==0 &&  $vistaPlanchetas==0 &&  $vistaReclamos==0 && $vistaRefPreOrg==0 && $vistaAdministracion==0 && $vistaTurnos==0 && $documentosModelo==0){
				echo "¡Debe seleccionar al menos una vista a la que tendra acceso!";
			}else{
				$resultadoNombre=null;

				$nombreOriginal="";
				//traemos el tipo de usuario si existe
				if(!empty($idTipoUsuario)){
					$resultado=$t->buscarId($idTipoUsuario);
					while($reg=$resultado->fetch_object()){
						$nombreOriginal=$reg->nombre;
					}   
				}

				if($nombre!=$nombreOriginal){
					$resultadoNombre=$t->comprobarExistenciaNombre($nombre);
				}
				if(!empty($resultadoNombre)){
					echo "¡Nombre para Tipo de Usuario ya Existe!";
				}else{
					if(empty($idTipoUsuario)){
						$resultado=$t->insertar($nombre,$vistaConfiguracion,$vistaRecursosHumanos,$vistaExpedientes,$vistaTramitesRapidos,$vistaPlanchetas,$vistaReclamos,$vistaRefPreOrg,$vistaAdministracion,$vistaTurnos,$documentosModelo,$altConfiguracion,$altRecursosHumanos,$altExpedientes,$altTramitesRapidos,$altPlanchetas,$altReclamos,$altRefPreOrg,$altAdministracion,$altTurnos,$newConfiguracion,$newRecursosHumanos,$newExpedientes,$newTramitesRapidos,$newPlanchetas,$newReclamos,$newRefPreOrg,$newAdministracion,$newTurnos);
						echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
					}else{
						$resultado=$t->editar($idTipoUsuario,$nombre,$vistaConfiguracion,$vistaRecursosHumanos,$vistaExpedientes,$vistaTramitesRapidos,$vistaPlanchetas,$vistaReclamos,$vistaRefPreOrg,$vistaAdministracion,$vistaTurnos,$documentosModelo,$altConfiguracion,$altRecursosHumanos,$altExpedientes,$altTramitesRapidos,$altPlanchetas,$altReclamos,$altRefPreOrg,$altAdministracion,$altTurnos,$newConfiguracion,$newRecursosHumanos,$newExpedientes,$newTramitesRapidos,$newPlanchetas,$newReclamos,$newRefPreOrg,$newAdministracion,$newTurnos);
						echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
					}
				}
			}

			
	break;
	
	case 'mostrar':
		$resultado=$t->buscar($idTipoUsuario);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;


	case 'listar':
		session_start();
		$varAlteracionTiposUsuarios=$_SESSION['alt_configuracion'];
		$resultado=$t->listar();
			
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionTiposUsuarios==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_tipo_usuario.')"><i class="fas fa-pencil-alt"></i>'; 
			}else{
				$opciones='';
			}
			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nombre,			
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	


	
	
}
?>