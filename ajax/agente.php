<?php 

require_once '../modelos/Agente.php';
$a=new Agente();

$idAgente=isset($_POST["idAgente"])? limpiarCadena($_POST["idAgente"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$estado=isset($_POST["estado"])? limpiarCadena($_POST["estado"]):"";

switch ($_GET["op"]) {
	case 'guardarOeditar':
		
			$resultadoNombre=null;
			$nombreOriginal="";
			//traemos el agente si existe
			if(!empty($idAgente)){
				$resultado=$a->buscarId($idAgente);
				while($reg=$resultado->fetch_object()){
					$nombreOriginal=$reg->nombre;
				}   
			}
			if($nombre!=$nombreOriginal){
				$resultadoNombre=$a->comprobarExistenciaNombre($nombre);
			}

			if(!empty($resultadoNombre)){
				echo "¡Agente Ya Existe!";
			}else{
	
				if(empty($idAgente)){
					$resultado=$a->insertar($nombre,$estado);
					echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
				}else{
					$resultado=$a->editar($idAgente,$nombre,$estado);
					echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
				}
			}
		
		


		
	break;
	
	case 'mostrar':
		$resultado=$a->buscar($idAgente);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	
	case 'listar':
		session_start();
		$varAlteracionAgentes=$_SESSION['alt_configuracion'];
		$resultado=$a->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			//el agente con nombre ADMINISTRACION NO VOY A PODER EDITARLO 
			if($varAlteracionAgentes==1 && $reg->nombre!="ADMINISTRACION"){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_agente.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="ACTIVO"){
				$status='<span class="label bg-green">'.$reg->estado.'</span>';
			}else{
				$status='<span class="label bg-red">'.$reg->estado.'</span>';
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nombre,
				"2"=>$status,
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	
	

	


	
	
}
?>