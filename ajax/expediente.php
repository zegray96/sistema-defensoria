<?php 
require_once '../modelos/Expediente.php';
$e=new Expediente();

$idExpediente=isset($_POST["idExpediente"])? limpiarCadena($_POST["idExpediente"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$fecha=isset($_POST["fecha"])? limpiarCadena($_POST["fecha"]):"";
$letra=isset($_POST["letra"])? limpiarCadena($_POST["letra"]):"";
$nro=isset($_POST["nro"])? limpiarCadena($_POST["nro"]):"";
$anio=isset($_POST["anio"])? limpiarCadena($_POST["anio"]):"";
$requirente=isset($_POST["requirente"])? limpiarCadena($_POST["requirente"]):"";
$tramite=isset($_POST["tramite"])? limpiarCadena($_POST["tramite"]):"";
$idAgente=isset($_POST["idAgente"])? limpiarCadena($_POST["idAgente"]):"";
$informeInicialActual=isset($_POST["informeInicialActual"])? limpiarCadena($_POST["informeInicialActual"]):"";
$informeQuinceActual=isset($_POST["informeQuinceActual"])? limpiarCadena($_POST["informeQuinceActual"]):"";
$informeTreintaActual=isset($_POST["informeTreintaActual"])? limpiarCadena($_POST["informeTreintaActual"]):"";
$informeCuarentaCincoActual=isset($_POST["informeCuarentaCincoActual"])? limpiarCadena($_POST["informeCuarentaCincoActual"]):"";
$informeMensualActual=isset($_POST["informeMensualActual"])? limpiarCadena($_POST["informeMensualActual"]):"";
$resolucionActual=isset($_POST["resolucionActual"])? limpiarCadena($_POST["resolucionActual"]):"";
$observacion=isset($_POST["observacion"])? limpiarCadena($_POST["observacion"]):"";
$estado=isset($_POST["estado"])? limpiarCadena($_POST["estado"]):"";



switch ($_GET["op"]) {
	case 'guardarOeditar':
			session_start();
			$ultimaModificacion=$_SESSION['idUsuarioDefPos'];

			if($nro=='' || $letra==''){
				echo '¡Ingrese N° y Letra de expediente!';
			}else{
				$informeInicial=$informeInicialActual;
				$informeQuince=$informeQuinceActual;
				$informeTreinta=$informeTreintaActual;
				$informeCuarentaCinco=$informeCuarentaCincoActual;
				$informeMensual=$informeMensualActual;
				$resolucion=$resolucionActual;

				$resultadoNro=null;
				$anioDosDigitos=substr($anio, -2); 
				$numeroConCeros = str_pad($nro, 2, "0", STR_PAD_LEFT);
				$nroCompleto=$nro.'-'.$letra.'-'.$anioDosDigitos;
				$nroYanio=$nro.$anio;
				$nroYanioOriginal="";
				//traemos el expediente si existe
				if(!empty($idExpediente)){
					$resultado=$e->buscarId($idExpediente);
					while($reg=$resultado->fetch_object()){
						$nroYanioOriginal=$reg->nro.$reg->anio;
						$agenteDesdeId=$reg->id_agente;
					}   
				}

				if($nroYanio!=$nroYanioOriginal){
					$resultadoNro=$e->comprobarExistenciaNro($nro,$anio);
				}


				
				if(!empty($resultadoNro)){
					echo "¡N° de Expediente ya Existe!";
				}else{
					if(is_uploaded_file($_FILES['informeInicial']['tmp_name'])){

						$informeInicial=$nroCompleto.'_informeInicial.pdf';
						move_uploaded_file($_FILES['informeInicial']['tmp_name'], "../files/informesExpedientes/".$informeInicial);

					}

					if(is_uploaded_file($_FILES['informeQuince']['tmp_name'])){
						$informeQuince=$nroCompleto.'_informe15dias.pdf';
						move_uploaded_file($_FILES['informeQuince']['tmp_name'], "../files/informesExpedientes/".$informeQuince);

					}

					if(is_uploaded_file($_FILES['informeTreinta']['tmp_name'])){
						$informeTreinta=$nroCompleto.'_informe30dias.pdf';
						move_uploaded_file($_FILES['informeTreinta']['tmp_name'], "../files/informesExpedientes/".$informeTreinta);
					}

					if(is_uploaded_file($_FILES['informeCuarentaCinco']['tmp_name'])){
						$informeCuarentaCinco=$nroCompleto.'_informe45dias.pdf';
						move_uploaded_file($_FILES['informeCuarentaCinco']['tmp_name'], "../files/informesExpedientes/".$informeCuarentaCinco);
					}

					if(is_uploaded_file($_FILES['informeMensual']['tmp_name'])){
						$informeMensual=$nroCompleto.'_informeMensual.pdf';
						move_uploaded_file($_FILES['informeMensual']['tmp_name'], "../files/informesExpedientes/".$informeMensual);
					}

					if(is_uploaded_file($_FILES['resolucion']['tmp_name'])){
						$resolucion=$nroCompleto.'_resolucion.pdf';
						move_uploaded_file($_FILES['resolucion']['tmp_name'], "../files/informesExpedientes/".$resolucion);
					}

					if($idAgente==17){
						$requirente="DEFENSORIA ADMINISTRACION";
					}	

					//nuevo
					if(empty($idExpediente)){
						$resultado=$e->insertar($fecha,$nroCompleto,$nro,$anio,$requirente,$tramite,$idAgente,$informeInicial,$informeQuince,$informeTreinta,$informeCuarentaCinco,$informeMensual,$resolucion,$observacion,$estado,$ultimaModificacion);
						echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
					}else{

						if($agenteDesdeId==17){
							if($_SESSION['id_tipo_usuario']==1 || $_SESSION['id_tipo_usuario']==5
								|| $_SESSION['id_tipo_usuario']==7){
								$resultado=$e->editar($idExpediente,$fecha,$nroCompleto,$nro,$anio,$requirente,$tramite,$idAgente,$informeInicial,$informeQuince,$informeTreinta,$informeCuarentaCinco,$informeMensual,$resolucion,$observacion,$estado,$ultimaModificacion);
								echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
							}else{
								echo "¡No posee permisos para modificar este registro!";
							}
						}else{
							$resultado=$e->editar($idExpediente,$fecha,$nroCompleto,$nro,$anio,$requirente,$tramite,$idAgente,$informeInicial,$informeQuince,$informeTreinta,$informeCuarentaCinco,$informeMensual,$resolucion,$observacion,$estado,$ultimaModificacion);
							echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
						}
						

						
					}
				}		

			}

			
	
	break;

	case 'ultimoNro':
		$anio=$_REQUEST['anio'];
		$resultado=$e->ultimoNro($anio);
		echo json_encode($resultado);
	break;
	
	case 'mostrar':
		$resultado=$e->buscar($idExpediente);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	case 'desactivar':
		$resultado=$e->desactivar($idExpediente);
		echo $resultado ? "¡Registro se cambio a estado 'EN TRAMITE'!" : "¡No se pudo cambiar el estado!";
	break;

	case 'activar':
		$resultado=$e->activar($idExpediente);
		echo $resultado ? "¡Registro se cambio a estado 'CERRADO'!" : "¡No se pudo cambiar el estado!";
	break;

	case 'contarRegistrosPorRevisar':
		$fechaActual = date('Y-m-d');
		$resultado=$e->contarRegistrosPorRevisar($fechaActual);
		echo json_encode($resultado);
	break;


	case 'listar':
		session_start();
		$varAlteracionExpedientes=$_SESSION['alt_expedientes'];
		$resultado=$e->listar();
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado==0){
				$estado='<span class="label bg-orange">EN TRAMITE</span>';
			}else{
				if($reg->estado==1){
					$estado='<span class="label bg-green">CERRADO</span>';
				}else{
					if($reg->estado==2){
						$estado='<span class="label bg-blue">JURIDICO</span>';
					}
						if($reg->estado==3){
							$estado='<span class="label bg-red">PARA CIERRE</span>';
						}		
				}
			}
			
			if($varAlteracionExpedientes==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_expediente.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_expediente,
				"1"=>$opciones,
				"2"=>$fechaFormateada,
				"3"=>$estado,
				"4"=>$reg->nro_expediente,
				"5"=>$reg->requirente,
				"6"=>$reg->tramite,
				"7"=>$reg->agente,
				"8"=>'<a href="../files/informesExpedientes/'.$reg->informe_inicial."?".rand().'" target="_blank">'.$reg->informe_inicial,
				"9"=>'<a href="../files/informesExpedientes/'.$reg->informe_quince."?".rand().'" target="_blank">'.$reg->informe_quince,
				"10"=>'<a href="../files/informesExpedientes/'.$reg->informe_treinta."?".rand().'" target="_blank">'.$reg->informe_treinta,
				"11"=>'<a href="../files/informesExpedientes/'.$reg->informe_cuarenta_cinco."?".rand().'" target="_blank">'.$reg->informe_cuarenta_cinco,
				"12"=>'<a href="../files/informesExpedientes/'.$reg->informe_mensual."?".rand().'" target="_blank">'.$reg->informe_mensual,
				"13"=>'<a href="../files/informesExpedientes/'.$reg->resolucion."?".rand().'" target="_blank">'.$reg->resolucion,
				"14"=>$reg->observacion,
				"15"=>$reg->ultimaModificacion,

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);


	break;


	case 'listarPorRevisar':
		session_start();
		$varAlteracionExpedientes=$_SESSION['alt_expedientes'];
		$fechaActual = date("Y-m-d");
		$resultado=$e->listarPorRevisar($fechaActual);
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN TRAMITE"){
				$estado='<span class="label bg-orange">EN TRAMITE</span>';
			}else{
				if($reg->estado=="CERRADO"){
					$estado='<span class="label bg-green">CERRADO</span>';
				}else{
					if($reg->estado=="JURIDICO"){
						$estado='<span class="label bg-blue">JURIDICO</span>';
					}
						if($reg->estado=="PARA CIERRE"){
							$estado='<span class="label bg-red">PARA CIERRE</span>';
						}		
				}
			}
			
			if($varAlteracionExpedientes==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_expediente.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			// diferencia de dias 
			$fecha1 = new DateTime($fechaActual);
			$fecha2 = new DateTime($reg->fecha);
			$diff = $fecha1->diff($fecha2);
			$diferenciaDias = $diff->days;

			if($diferenciaDias>7 && $reg->informe_inicial==""){
				$infInicial='<span class="label bg-red">FALTA CARGAR</span>';
			}else{
				$infInicial='<a href="../files/informesExpedientes/'.$reg->informe_inicial."?".rand().'" target="_blank">'.$reg->informe_inicial;
			}

			if($diferenciaDias>15 && $reg->informe_quince==""){
				$infQuince='<span class="label bg-red">FALTA CARGAR</span>';
			}else{
				$infQuince='<a href="../files/informesExpedientes/'.$reg->informe_quince."?".rand().'" target="_blank">'.$reg->informe_quince;
			}

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_expediente,
				"1"=>$opciones,
				"2"=>$fechaFormateada,
				"3"=>$estado,
				"4"=>$reg->nro_expediente,
				"5"=>$reg->requirente,
				"6"=>$reg->tramite,
				"7"=>$reg->agente,
				"8"=>$infInicial,
				"9"=>$infQuince,
				"10"=>'<a href="../files/informesExpedientes/'.$reg->informe_treinta."?".rand().'" target="_blank">'.$reg->informe_treinta,
				"11"=>'<a href="../files/informesExpedientes/'.$reg->informe_cuarenta_cinco."?".rand().'" target="_blank">'.$reg->informe_cuarenta_cinco,
				"12"=>'<a href="../files/informesExpedientes/'.$reg->informe_mensual."?".rand().'" target="_blank">'.$reg->informe_mensual,
				"13"=>'<a href="../files/informesExpedientes/'.$reg->resolucion."?".rand().'" target="_blank">'.$reg->resolucion,
				"14"=>$reg->observacion,
				"15"=>$reg->ultimaModificacion,

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listarPorAnio':
		session_start();
		$varAlteracionExpedientes=$_SESSION['alt_expedientes'];
		$varAnio=$_REQUEST['varAnio'];
		$fechaActual = date("Y-m-d");
		$resultado=$e->listarPorAnio($varAnio);
		//declaramos un array
			

		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($reg->estado=="EN TRAMITE"){
				$estado='<span class="label bg-orange">EN TRAMITE</span>';
			}else{
				if($reg->estado=="CERRADO"){
					$estado='<span class="label bg-green">CERRADO</span>';
				}else{
					if($reg->estado=="JURIDICO"){
						$estado='<span class="label bg-blue">JURIDICO</span>';
					}
						if($reg->estado=="PARA CIERRE"){
							$estado='<span class="label bg-red">PARA CIERRE</span>';
						}		
				}
			}
			
			if($varAlteracionExpedientes==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_expediente.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			// diferencia de dias 
			$fecha1 = new DateTime($fechaActual);
			$fecha2 = new DateTime($reg->fecha);
			$diff = $fecha1->diff($fecha2);
			$diferenciaDias = $diff->days;


			if($reg->anio>=2020){
				// solo se va mostrar el msj a exp mayores a 2020
				if($diferenciaDias>7 && $reg->informe_inicial=="" && $reg->id_agente!=17){
					$infInicial='<span class="label bg-red">FALTA CARGAR</span>';
				}else{
					$infInicial='<a href="../files/informesExpedientes/'.$reg->informe_inicial."?".rand().'" target="_blank">'.$reg->informe_inicial;
				}

				if($diferenciaDias>15 && $reg->informe_quince=="" && $reg->id_agente!=17){
					$infQuince='<span class="label bg-red">FALTA CARGAR</span>';
				}else{
					$infQuince='<a href="../files/informesExpedientes/'.$reg->informe_quince."?".rand().'" target="_blank">'.$reg->informe_quince;
				}

			}else{
				$infInicial='<a href="../files/informesExpedientes/'.$reg->informe_inicial."?".rand().'" target="_blank">'.$reg->informe_inicial;
				$infQuince='<a href="../files/informesExpedientes/'.$reg->informe_quince."?".rand().'" target="_blank">'.$reg->informe_quince;
			}
			

			// oculto o no el tramite y opciones
			if($reg->id_agente==17){
				if($_SESSION['id_tipo_usuario']==1 || $_SESSION['id_tipo_usuario']==5 || $_SESSION['id_tipo_usuario']==7){
					$tramite=$reg->tramite;

					if($varAlteracionExpedientes==1){
						$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_expediente.')"><i class="fas fa-pencil-alt"></i></button>'; 
					}else{
						$opciones='';
					}

				}else{
					$tramite="";
					$opciones='';
				}
			}else{
				$tramite=$reg->tramite;
				if($varAlteracionExpedientes==1){
					$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_expediente.')"><i class="fas fa-pencil-alt"></i></button>'; 
				}else{
					$opciones='';
				}
			}
			

			$fechaFormateada = date("Y-m-d", strtotime($reg->fecha));
			$data[]=array(
				"0"=>$reg->id_expediente,
				"1"=>$opciones,
				"2"=>$fechaFormateada,
				"3"=>$estado,
				"4"=>$reg->nro_expediente,
				"5"=>$reg->requirente,
				"6"=>$tramite,
				"7"=>$reg->agente,
				"8"=>$infInicial,
				"9"=>$infQuince,
				"10"=>'<a href="../files/informesExpedientes/'.$reg->informe_treinta."?".rand().'" target="_blank">'.$reg->informe_treinta,
				"11"=>'<a href="../files/informesExpedientes/'.$reg->informe_cuarenta_cinco."?".rand().'" target="_blank">'.$reg->informe_cuarenta_cinco,
				"12"=>'<a href="../files/informesExpedientes/'.$reg->informe_mensual."?".rand().'" target="_blank">'.$reg->informe_mensual,
				"13"=>'<a href="../files/informesExpedientes/'.$reg->resolucion."?".rand().'" target="_blank">'.$reg->resolucion,
				"14"=>$reg->observacion,
				"15"=>$reg->ultimaModificacion,

				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;



	case 'selectAgente':
		require_once '../modelos/Agente.php';
		$a=new Agente();
		$resultado=$a->select();
		$opcion="[SELECCIONAR]";
		echo '<option selected="true" disabled="disabled">'.$opcion.'</option>';
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->id_agente.'>'.$reg->nombre.'</option>';
		}
	break;


	case 'fecha':
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		echo date('Y-m-d');
	break;

	case 'borrarArchivo':
		session_start();
		$idEx=$_REQUEST['idEx'];
		$valor=$_REQUEST['valor'];
		$idUltimaModificacion=$_SESSION['idUsuarioDefPos'];

		//buscamos el expediente
		$resultado=$e->buscarId($idEx);
		while($reg=$resultado->fetch_object()){
			$informeInicial=$reg->informe_inicial;
			$informeQuince=$reg->informe_quince;
			$informeTreinta=$reg->informe_treinta;
			$informeCuarentaCinco=$reg->informe_cuarenta_cinco;
			$informeMensual=$reg->informe_mensual;
			$resolucion=$reg->resolucion;
		}   
		         
		//de acuerdo al valor que le pasemos, eliminamos el archivo
		if($valor=='inicial'){
			$ubicacion='..\files\informesExpedientes\\'.$informeInicial; //borra el archivo en la carpeta
			unlink($ubicacion);
			$resultado=$e->borrarInformeInicial($idEx,$idUltimaModificacion);
			echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
		}else{
			if($valor=='quince'){
				$ubicacion='..\files\informesExpedientes\\'.$informeQuince; //borra el archivo en la carpeta
				unlink($ubicacion);
				$resultado=$e->borrarInformeQuince($idEx,$idUltimaModificacion);
				echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
			}else{
				if($valor=='treinta'){
					$ubicacion='..\files\informesExpedientes\\'.$informeTreinta; //borra el archivo en la carpeta
					unlink($ubicacion);
					$resultado=$e->borrarInformeTreinta($idEx,$idUltimaModificacion);
					echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
				}else{
					if($valor=='cuarentaCinco'){
						$ubicacion='..\files\informesExpedientes\\'.$informeCuarentaCinco; //borra el archivo en la carpeta
						unlink($ubicacion);
						$resultado=$e->borrarInformeCuarentaCinco($idEx,$idUltimaModificacion);
						echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
					}else{
						if($valor=='mensual'){
							$ubicacion='..\files\informesExpedientes\\'.$informeMensual; //borra el archivo en la carpeta
							unlink($ubicacion);
							$resultado=$e->borrarInformeMensual($idEx,$idUltimaModificacion);
							echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
						}else{
							if($valor=='resolucion'){
								$ubicacion='..\files\informesExpedientes\\'.$resolucion; //borra el archivo en la carpeta
								unlink($ubicacion);
								$resultado=$e->borrarResolucion($idEx,$idUltimaModificacion);
								echo $resultado ? "¡Archivo eliminado con exito!" : "¡Archivo no se Pudo eliminar!";
							}
						}
					}
				}
			}

		}
	

	break;

	case 'traerAlteracion':
		session_start();
		$respuesta=$_SESSION['alt_expedientes'];
		echo $respuesta;
	break;

	case 'traerIdTipoUsuario':
		session_start();
		$respuesta=$_SESSION['id_tipo_usuario'];
		echo $respuesta;
	break;
	
	
}
?>