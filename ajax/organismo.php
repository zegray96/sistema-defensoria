<?php 

require_once '../modelos/Organismo.php';
$o=new Organismo();

$idOrganismo=isset($_POST["idOrganismo"])? limpiarCadena($_POST["idOrganismo"]):""; //si existe un envio por metodo post llamo a limpiar cadena
$idOrgNombre=isset($_POST["idOrgNombre"])? limpiarCadena($_POST["idOrgNombre"]):"";
$autoridad=isset($_POST["autoridad"])? limpiarCadena($_POST["autoridad"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";


switch ($_GET["op"]) {
	case 'guardarOeditar':
		
			if(empty($idOrganismo)){
				$resultado=$o->insertar($idOrgNombre,$autoridad,$telefono,$email);
				echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
			}else{
				$resultado=$o->editar($idOrganismo,$idOrgNombre,$autoridad,$telefono,$email);
				echo $resultado ? "¡Registro editado con exito!" : "¡Registro no se pudo editar!";
			}
		
		


		
	break;

	case 'guardarOrgNombre':
			$orgNombre=isset($_POST["orgNombre"])? limpiarCadena($_POST["orgNombre"]):"";
			
			$resultadoNombre=$o->comprobarExistenciaNombre($orgNombre);

			if(!empty($resultadoNombre)){
				echo "¡Nombre de Organismo ya existe!";
			}else{
				$resultado=$o->insertarOrgNombre($orgNombre);
				echo $resultado ? "¡Registro creado con exito!" : "¡Registro no se pudo crear!";
			}	
			
			
			
		
		


		
	break;
	
	case 'mostrar':
		$resultado=$o->buscar($idOrganismo);
		//Se codifica el resultado para poder mostrar
		echo json_encode($resultado);
	break;

	
	case 'listar':
		session_start();
		$varAlteracionRefPreOrg=$_SESSION['alt_ref_pre_org'];
		$resultado=$o->listar();
		//declaramos un array
		$data = Array();
		while($reg=$resultado->fetch_object()){
			if($varAlteracionRefPreOrg==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_organismo.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-danger" onclick="eliminar('.$reg->id_organismo.')"><i class="fas fa-trash-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->orgNombre,
				"2"=>$reg->autoridad,
				"3"=>$reg->telefono,
				"4"=>$reg->email,
				
			);

		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'selectOrgNombre':
		
		$resultado=$o->select();
		$opcion="[SELECCIONAR]";
		echo '<option selected="true" disabled="disabled">'.$opcion.'</option>';
		while($reg=$resultado->fetch_object()){

			echo '<option value='.$reg->id_org_nombre.'>'.$reg->nombre.'</option>';

		}


	break;


	case 'eliminar':		
		
		$resultado=$o->eliminar($idOrganismo);
		echo $resultado ? "¡Registro eliminado con exito!" : "¡Registro no se pudo eliminar!";
		
	break;

	
	

	


	
	
}
?>