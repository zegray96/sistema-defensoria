<!DOCTYPE html>
<html>
  <head>
    <?php
    include ('../config/version.php');
    ?>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Turnos Online</title>
    <!--icono de pagina-->
    <link rel="shortcut icon" href="../public/img/ico.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!--bootstrap select-->
    <link rel="stylesheet" href="../public/css/bootstrap-select.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/all.min.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="../public/css/bootstrap-datepicker.min.css">
    <!-- Mi css propio -->
    <link rel="stylesheet" href="../externo/css/estilos.css?ver=<?php echo $version; ?>">

   
  </head>
  <body>
    
    <div class="container" id="contenedor" style="text-align: center;">
      <!--imagen cargando-->
      <div id="cargandoGif">
        <img src="../public/img/cargando.gif">
      </div>
		
      <div id="contenidoHeader">
            <img id="logo" src="../public/img/logoLargoSinBorde.png">
            <h1 id="tituloTurnos">BIENVENIDO AL GESTOR DE TURNOS ONLINE</h1>
            <h3 id="subtituloTurnos">Seguí los pasos para programar tu visita a la institución</h3>
      </div>
      
      <!-- CONTENIDO FORM-->
      <div id="contenidoForm" class="col-s-12 col-lg-8 col-lg-offset-2">
      		

        <div class="stepwizard">
          <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
              <button id="step-1-recorrido" class="btn btn-primary">1</button>
              <p>Datos Personales</p>
            </div>
            <div class="stepwizard-step">
              <button id="step-2-recorrido" class="btn btn-default">2</button>
              <p>Datos de Contacto</p>
            </div>
            <div class="stepwizard-step">
              <button id="step-3-recorrido" type="button" class="btn btn-default">3</button>
              <p>Fecha y Hora</p>
            </div>
            <div class="stepwizard-step">
              <button id="step-4-recorrido" type="button" class="btn btn-default">4</button>
              <p>Motivo del Turno</p>
            </div>
          </div>
        </div>

        <!-- FORM RECLAMO -->
        <form role="form" id="formulario">
          <div class="row setup-content" id="step-1-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
              
                <h3> Datos Personales</h3>

                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Apellido y Nombre</label>
                  <input type="text" class="texto form-control" name="apellidoNombre" id="apellidoNombre" maxlength="100" placeholder="NOMBRE" required onkeyup="capitalizar('apellidoNombre')">
                </div>
                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Dni</label>
                  <input type="text" maxlength="10" class="texto form-control" name="dni" id="dni" maxlength="50" placeholder="DNI" required onkeyup="format(this)" onchange="format(this)">
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" id="btnNext1" type="button" >Siguiente</button>
              
            </div>
          </div>
          <div class="row setup-content" id="step-2-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
                <h3> Datos de Contacto</h3>
                 
                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Telefono</label>
                  <input type="text" class="texto form-control" name="telefono" id="telefono" maxlength="100" placeholder="TELEFONO" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Email</label>
                  <input type="text" class="texto form-control" name="email" id="email" maxlength="100" placeholder="EMAIL">
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" id="btnNext2" type="button" >Siguiente</button>
              
            </div>
          </div>

          <div class="row setup-content" id="step-3-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
                <h3> Fecha y Hora</h3>
                
                <div class="form-group" id="izquierdaCalendario">
                  <h4><label style="color: red;">(*)</label> Seleccione fecha</h4>
                  <div class="form-group">
                    <div class='input-group date' id='calendario'>
                      <input type='text' class="form-control" id="fecha" name="fecha" readonly>
                      <span class="input-group-addon" style="cursor:pointer;">
                        <span class="fas fa-calendar" ></span>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="form-group" id="derechaCalendario">
                  <input type="hidden" name="hora" id="hora">
                  <h4><label style="color: red;">(*)</label> Seleccione hora</h4>
                  <div id="horariosDisponibles">

                  </div>
                </div>
               
                
                

                <button class="btn btn-primary nextBtn btn-lg pull-right" id="btnNext3" type="button">Siguiente</button>
            </div>
          </div>

          <div class="row setup-content" id="step-4-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
                <h3> Motivo del Turno</h3>
                <label><label style="color: red;">(*)</label> Contamos el motivo por el cual solicitas el turno: <label style="font-weight: normal" id="contMotivo"> 0</label> <label style="font-weight: normal">/ 100 Caracteres</label> </label> 
                <div>
                  <textarea class="form-control" maxlength="100" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="motivo" id="motivo" rows="4" onkeyup="return contadorCaracteres('#motivo','#contMotivo')" placeholder="MOTIVO"></textarea>
                </div>

               

                <button class="btn btn-success btn-lg pull-right" id="btnConfirmar" type="submit">Confirmar</button>
            </div>
          </div>
        </form>
        <!-- FIN FORM RECLAMO -->
      </div>
      <!-- FIN CONTENIDO FORM -->
  
      <!--Resultado-->
      <div id="resultadoPanel" class="panel-body">
        <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6  col-lg-offset-3 col-lg-6">
          <div class="panel panel-default">
            <div class="panel-body">
              <div id="divImgResultado">
                <img id="imagenResultado">
              </div>
              <p id="resultadoTexto"></p>
              <p id="fechaTexto"></p>
              <p id="horaTexto"></p>
              <div id="volver">
                <a class="btn btn-primary" href="turnos"><i class="fas fa-arrow-circle-left"></i> Volver</a>
                <a class="btn btn-info" onclick="window.print()"><i class="fas fa-print"></i> Imprimir</a>
              </div>
              
            </div>  
          </div>  
        </div> 
      </div>
      <!--fin resultado-->

    </div>
    <!-- FIN CONTENEDOR -->


   



    <footer>
      <!-- jQuery 3.3.1 -->
      <script src="../public/js/jquery-3.3.1.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="../public/js/bootstrap.min.js"></script>
      <!--datatables-->
      <script src="../public/js/jquery.dataTables.min.js"></script>   
      <script src="../public/js/dataTables.bootstrap.min.js"></script>
      <script src="../public/js/datatables.min.js"></script>
      <script src="../public/js/dataTables.buttons.min.js"></script>
      <script src="../public/js/buttons.html5.min.js"></script>
      <script src="../public/js/buttons.colVis.min.js"></script>
      <script src="../public/js/jszip.min.js"></script>
      <script src="../public/js/pdfmake.min.js"></script>
      <script src="../public/js/vfs_fonts.js"></script>
      <!--bootstrap select-->
      <script src="../public/js/bootstrap-select.min.js"></script>
      <!-- sweet alert -->
      <script src="../public/js/sweetalert2.all.min.js"></script>
      <!-- Datepicker -->
      <script src="../public/js/bootstrap-datepicker.min.js"></script>
      <!-- Traduccion datepicker -->
      <script src="../public/js/bootstrap-datepicker.es.min.js"></script>


      <script type="text/javascript" src="../externo/js/turnos.js?ver=<?php echo $version; ?>"></script>


      
    </footer> 
    
  </body>
</html>
