<!DOCTYPE html>
<html>
  <head>
    <?php
    include ('../config/version.php');
    ?>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reclamos Online</title>
    <!--icono de pagina-->
    <link rel="shortcut icon" href="../public/img/ico.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!--bootstrap select-->
    <link rel="stylesheet" href="../public/css/bootstrap-select.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/all.min.css">
    <!-- Mi css propio -->
    <link rel="stylesheet" href="../externo/css/estilos.css?ver=<?php echo $version; ?>">




   
  </head>
  <body>
    
    <div class="container" id="contenedor" style="text-align: center;">
      <!--imagen cargando-->
      <div id="cargandoGif">
        <img src="../public/img/cargando.gif">
      </div>
		  
      <div id="contenidoHeader">
        <img id="logo" src="../public/img/logoLargoSinBorde.png">
        <h1 id="tituloReclamos">BIENVENIDO AL GESTOR DE RECLAMOS ONLINE</h1>
        <h3 id="subtituloReclamos">Seguí los pasos para realizar tu reclamo</h3>
      </div>
      
      <!-- CONTENIDO FORM-->
      <div id="contenidoForm" class="col-s-12 col-lg-8 col-lg-offset-2">

        <div class="stepwizard">
          <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
              <button id="step-1-recorrido" class="btn btn-primary">1</button>
              <p>Datos Personales</p>
            </div>
            <div class="stepwizard-step">
              <button id="step-2-recorrido" class="btn btn-default">2</button>
              <p>Datos de Contacto</p>
            </div>
            <div class="stepwizard-step">
              <button id="step-3-recorrido" type="button" class="btn btn-default">3</button>
              <p>Motivo del Reclamo</p>
            </div>
          </div>
        </div>

        <!-- FORM RECLAMO -->
        <form role="form" id="formulario">
          <div class="row setup-content" id="step-1-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
              
                <h3> Datos Personales</h3>

                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Apellido y Nombre</label>
                  <input type="text" class="texto form-control" name="apellidoNombre" id="apellidoNombre" maxlength="100" placeholder="Apellido y Nombre" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
                </div>
                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Dni</label>
                  <input type="text" maxlength="10" class="texto form-control" name="dni" id="dni" maxlength="10" placeholder="DNI" required onkeyup="format(this)" onchange="format(this)">
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" id="btnNext1" type="button" >Siguiente</button>
              
            </div>
          </div>
          <div class="row setup-content" id="step-2-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
                <h3> Datos de Contacto</h3>
                 <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Domicilio</label>
                  <input type="text" class="texto form-control" name="domicilio" id="domicilio" maxlength="100" placeholder="DOMICILIO" required onblur="this.value=this.value.toUpperCase();">
                </div>
                <div class="form-group">
                  <label class="control-label"><label style="color: red;">(*)</label> Telefono</label>
                  <input type="text" class="texto form-control" name="telefono" id="telefono" maxlength="35" placeholder="TELEFONO" required>
                </div>
                <div class="form-group">
                  <label class="control-label">Email</label>
                  <input type="text" class="texto form-control" name="email" id="email" maxlength="35" placeholder="EMAIL">
                </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" id="btnNext2" type="button" >Siguiente</button>
              
            </div>
          </div>
          <div class="row setup-content" id="step-3-form">
            <div class="col-s-12 col-s-offset-12 col-sm-8 col-sm-offset-2">
                <h3> Motivo del Reclamo</h3>
                <label><label style="color: red;">(*)</label> Reclamo: </label> <label style="font-weight: normal" id="contReclamo"> 0</label> <label style="font-weight: normal">/ 500 Caracteres</label>
                <div>
                  <textarea class="form-control" maxlength="500" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="reclamo" id="reclamo" rows="5" cols="65" onkeyup="return contadorCaracteres('#reclamo','#contReclamo')" placeholder="RECLAMO"></textarea>
                </div>

                <div style="margin-bottom: 10px;">
                  <span><em>Vecino, controle que la información de contacto sea la correcta. En caso de no poder comunicarnos con ud. En el transcurso de 7 días, su reclamo será desestimado.</em></span>
                </div>
                <div>
                 <label><input type="checkbox" id="checkAceptar" onchange="activarDesactivarBoton()"> He leído las <a href="https://defensoriaposadas.com.ar/reclamos/" target="blank"> Competencias y Atribuciones</a></label>
               </div>

                <button class="btn btn-success btn-lg pull-right" id="btnConfirmar" type="submit">Confirmar</button>
            </div>
          </div>
        </form>
        <!-- FIN FORM RECLAMO -->
      </div>
      <!-- FIN CONTENIDO FORM -->
  
      <!--Resultado-->
      <div id="resultadoPanel" class="panel-body">
        <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6  col-lg-offset-3 col-lg-6">
          <div class="panel panel-default">
            <div class="panel-body">
              <div id="divImgResultado">
                <img id="imagenResultado">
              </div>
              <p id="resultadoTexto"></p>
              <p id="nroReclamoTexto"></p>
              <!-- <p id="tomarNotaTexto"></p> -->

              <div id="volver">
                <a class="btn btn-primary" href="reclamos"><i class="fas fa-arrow-circle-left"></i> Volver</a>
                <a class="btn btn-info" onclick="window.print()"><i class="fas fa-print"></i> Imprimir</a>
              </div>
              
            </div>  
          </div>  
        </div> 
      </div>
      <!--fin resultado-->

    </div>
    <!-- FIN CONTENEDOR -->


   



    <footer>
      <!-- jQuery 3.3.1 -->
      <script src="../public/js/jquery-3.3.1.js"></script>
      <!-- Bootstrap 3.3.7 -->
      <script src="../public/js/bootstrap.min.js"></script>
      <!--datatables-->
      <script src="../public/js/jquery.dataTables.min.js"></script>   
      <script src="../public/js/dataTables.bootstrap.min.js"></script>
      <script src="../public/js/datatables.min.js"></script>
      <script src="../public/js/dataTables.buttons.min.js"></script>
      <script src="../public/js/buttons.html5.min.js"></script>
      <script src="../public/js/buttons.colVis.min.js"></script>
      <script src="../public/js/jszip.min.js"></script>
      <script src="../public/js/pdfmake.min.js"></script>
      <script src="../public/js/vfs_fonts.js"></script>
      <!--bootstrap select-->
      <script src="../public/js/bootstrap-select.min.js"></script>
      <!-- sweet alert -->
      <script src="../public/js/sweetalert2.all.min.js"></script>

      <script type="text/javascript" src="../externo/js/reclamos.js?ver=<?php echo $version; ?>"></script>


      
    </footer> 
    
  </body>
</html>
