function init(){

    $("#contenidoHeader").show(); //mostrmos el header
    $("#contenidoForm").show(); //mostrmos el contenido
    $("#step-1-form").show();
    $("#step-2-form").hide();
    $("#step-3-form").hide();
    $("#cargandoGif").hide(); //ocultamos el gif cargando
    $("#contenedor").css('text-align',''); 

 	$("#formulario").on('submit', function(e){
 		guardar(e);
 	});

	limpiar();

	//step recorrido
	$( "#step-1-recorrido" ).click(function() {
	 	$("#step-1-form").show();
	 	$("#step-2-form").hide();
	 	$("#step-3-form").hide();
	 	$("#step-1-recorrido").removeClass('btn-default').addClass('btn-primary');
	 	$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
	 	$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
	});

	$( "#step-2-recorrido" ).click(function() {
	 	$("#step-1-form").hide();
	 	$("#step-2-form").show();
	 	$("#step-3-form").hide();
	 	$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
	 	$("#step-2-recorrido").removeClass('btn-default').addClass('btn-primary');
	 	$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
	});

	$("#step-3-recorrido").click(function(){
		$("#step-1-form").hide();
		$("#step-2-form").hide();
		$("#step-3-form").show();
		$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
		$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
		$("#step-3-recorrido").removeClass('btn-default').addClass('btn-primary');
	});

	//botones next y controlar campos vacios
	$("#btnNext1").click(function(){
		if($("#apellidoNombre").val()=="" || $("#dni").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-1-form").hide();
		 	$("#step-2-form").show();
		 	
		 	$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-2-recorrido").prop('disabled',false);
		 	$("#step-2-recorrido").removeClass('btn-default').addClass('btn-primary');
		}
	});

	$("#btnNext2").click(function() {
		if($("#domicilio").val()=="" || $("#telefono").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-2-form").hide();
		 	$("#step-3-form").show();
		 	
		 	$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-3-recorrido").prop('disabled',false);
		 	$("#step-3-recorrido").removeClass('btn-default').addClass('btn-primary');
		}

	 	
	});

}


//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}


//funcion limpiar
function limpiar(){
	$("#step-1-recorrido").prop('disabled',false);
	$("#step-2-recorrido").prop('disabled',true);
	$("#step-3-recorrido").prop('disabled',true);

	$("#apellidoNombre").val("");
	$("#dni").val("");
	$("#domicilio").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#reclamo").val("");
	$("#btnConfirmar").prop("disabled",true);
	$("#contReclamo").text("0");
	$("#checkAceptar").prop("checked",false);	
}

//funcion para activar o desactivar boton
function activarDesactivarBoton(){
	if($("#checkAceptar").prop("checked")){
		$("#btnConfirmar").prop('disabled', false);
	}else{
		$("#btnConfirmar").prop('disabled', true);
	}
}

//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

function guardar(e){
	e.preventDefault();
	if($("#reclamo").val()==""){
		Swal.fire({
			icon: 'error',
			title: '¡Complete campos obligatorios!',
		});
	}else{

		var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
		$("#cargandoModal").modal('show');
		$.ajax({
			url:'../ajax/reclamo.php?op=guardarOeditarWeb',//lugar a donde se envia los datos obtenidos del formulario
			type: "POST",
			data: formData, //estos son los datos que envio
			contentType:false,
			processData:false,

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
					$("#cargandoModal").modal('hide');


					if(datos=="¡Complete campos obligatorios!"){
						Swal.fire({
							icon: 'error',
							title: datos,
						});
					}else{
						if(datos=="¡Reclamo no se pudo enviar!" || datos==0){
							$("#imagenResultado").attr("src","../public/img/false.png");
							$("#resultadoTexto").text("¡Reclamo no se pudo Enviar!");
						}else{
							if(datos>0){
								$("#imagenResultado").attr("src","../public/img/true.png");
								$("#resultadoTexto").text("¡Reclamo enviado con éxito!");
								$("#nroReclamoTexto").text("N° de reclamo: REC-"+datos+"");
								// $("#tomarNotaTexto").text("Tome nota para consultar sobre el mismo");
							}else{
								$("#imagenResultado").attr("src","../public/img/false.png");
								$("#resultadoTexto").text(datos);
							}

						}	
						$("#contenidoHeader").hide(); //mostrmos el header
						$("#contenidoForm").hide(); //ocultamos el form
						$("#resultadoPanel").show(); //mostrmos el contenido
						

						limpiar();
					}

					
			}
		});

	}
}



init();