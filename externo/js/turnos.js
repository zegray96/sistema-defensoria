var horaSeleccionada="";

function init(){
	$("#contenidoHeader").show(); //mostrmos el header
    $("#contenidoForm").show(); //mostrmos el contenido
    $("#step-1-form").show();
    $("#step-2-form").hide();
    $("#step-3-form").hide();
    $("#step-4-form").hide();
    $("#derechaCalendario").hide();
    $("#cargandoGif").hide(); //ocultamos el gif cargando
    $("#contenedor").css('text-align',''); 

 	$("#formulario").on('submit', function(e){
 		guardar(e);
 	});

	limpiar();
	inicializarCalendario();

	//step recorrido
	$( "#step-1-recorrido" ).click(function() {
	 	$("#step-1-form").show();
	 	$("#step-2-form").hide();
	 	$("#step-3-form").hide();
	 	$("#step-4-form").hide();
	 	$("#step-1-recorrido").removeClass('btn-default').addClass('btn-primary');
	 	$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
	 	$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
	 	$("#step-4-recorrido").removeClass('btn-primary').addClass('btn-default');
	});

	$( "#step-2-recorrido" ).click(function() {
		if($("#apellidoNombre").val()=="" || $("#dni").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-1-form").hide();
		 	$("#step-2-form").show();
		 	$("#step-3-form").hide();
		 	$("#step-4-form").hide();
		 	$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-2-recorrido").removeClass('btn-default').addClass('btn-primary');
		 	$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-4-recorrido").removeClass('btn-primary').addClass('btn-default');
		}	
	 	
	});

	$("#step-3-recorrido").click(function(){
		if($("#telefono").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-1-form").hide();
			$("#step-2-form").hide();
			$("#step-3-form").show();
			$("#step-4-form").hide();
			$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
			$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
			$("#step-3-recorrido").removeClass('btn-default').addClass('btn-primary');
			$("#step-4-recorrido").removeClass('btn-primary').addClass('btn-default');
		}	
		
	});

	$("#step-4-recorrido").click(function(){
		if($("#fecha").val()=="" || $("#hora").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-1-form").hide();
			$("#step-2-form").hide();
			$("#step-3-form").hide();
			$("#step-4-form").show();
			$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
			$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
			$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
			$("#step-4-recorrido").removeClass('btn-default').addClass('btn-primary');
		}	
		
	});

	//botones next y controlar campos vacios
	$("#btnNext1").click(function(){
		if($("#apellidoNombre").val()=="" || $("#dni").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-1-form").hide();
		 	$("#step-2-form").show();
		 	
		 	$("#step-1-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-2-recorrido").prop('disabled',false);
		 	$("#step-2-recorrido").removeClass('btn-default').addClass('btn-primary');
		}
	});

	$("#btnNext2").click(function() {
		if($("#telefono").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-2-form").hide();
		 	$("#step-3-form").show();
		 	
		 	$("#step-2-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-3-recorrido").prop('disabled',false);
		 	$("#step-3-recorrido").removeClass('btn-default').addClass('btn-primary');
		}

	 	
	});

	$("#btnNext3").click(function() {
		if($("#fecha").val()=="" || $("#hora").val()==""){
			Swal.fire({
				icon: 'error',
				title: '¡Complete campos obligatorios!',
			});
		}else{
			$("#step-3-form").hide();
		 	$("#step-4-form").show();
		 	
		 	$("#step-3-recorrido").removeClass('btn-primary').addClass('btn-default');
		 	$("#step-4-recorrido").prop('disabled',false);
		 	$("#step-4-recorrido").removeClass('btn-default').addClass('btn-primary');
		}

	 	
	});

	

}


// fecha y hora
function colorearHora(hora){
	if(hora=='09'){
		$("#h09").removeClass('btn-default').addClass('btn-primary');
		$("#h10").removeClass('btn-primary').addClass('btn-default');
		$("#h11").removeClass('btn-primary').addClass('btn-default');
		$("#hora").val($("#h09").val());
	}else{
		if(hora=='10'){
			$("#h09").removeClass('btn-primary').addClass('btn-default');
			$("#h10").removeClass('btn-default').addClass('btn-primary');
			$("#h11").removeClass('btn-primary').addClass('btn-default');
			$("#hora").val($("#h10").val());
		}else{
			if(hora=='11'){
				$("#h09").removeClass('btn-primary').addClass('btn-default');
				$("#h10").removeClass('btn-primary').addClass('btn-default');
				$("#h11").removeClass('btn-default').addClass('btn-primary');
				$("#hora").val($("#h11").val());
			}
		}
	}
	
	
}
	



//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(elemId) {
 	var txt = $("#" + elemId).val().toLowerCase();
 	$("#" + elemId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
 		return $1.toUpperCase(); }));
 }

function inicializarCalendario(){
	var inicio = new Date(); //fecha de hoy
	inicio.setDate(inicio.getDate() + 1);
	var dias = 14; // Número de días a agregar
	var fin = new Date();
	fin.setDate(fin.getDate() + dias);

	$('#calendario').datepicker({
		language: "es",
		daysOfWeekDisabled: '0,6', //desabilita dias 0 y 6, domingo y sabado
		startDate: inicio,
		endDate: fin,
	}).on('changeDate',function(e){
		$("#hora").val("");
		$("#derechaCalendario").show();
	  	var date = $("#calendario").data('datepicker').getFormattedDate('yyyy-mm-dd');

		 //verificar disponibilidad
		 $.ajax({
				url:'../ajax/turno.php?op=disponibilidad',//lugar a donde se envia los datos obtenidos del formulario
				type: "post",
				data: {'fecha': date}, //estos son los datos que envio


				success: function(datos){ //si la accion se hace de forma correcta se hace esto
					if(datos=='si'){
						// MOSTRAMOS LOS BOTONES CON LOS HORARIOS
						$.ajax({
							url:'../ajax/turno.php?op=botonesHorario',//lugar a donde se envia los datos obtenidos del formulario
							type: "post",
							data: {'fecha': date}, //estos son los datos que envio
							


							success: function(datos){ //si la accion se hace de forma correcta se hace esto
							 	$("#horariosDisponibles").html(datos); //con append, me agrega al final conservando lo anterior
							 	$(".datepicker").hide();

							 }
						});

					}else{
						Swal.fire({
							icon: 'error',
							title: datos,
							allowOutsideClick: false
						});
						$("#hora").val("");
						$("#horariosDisponibles").html("")
					}

				}


			});
	});
}


//funcion limpiar
function limpiar(){
	$("#step-1-recorrido").prop('disabled',false);
	$("#step-2-recorrido").prop('disabled',true);
	$("#step-3-recorrido").prop('disabled',true);
	$("#step-4-recorrido").prop('disabled',true);

	$("#apellidoNombre").val("");
	$("#fecha").val("");
	$("#hora").val("");
	$("#dni").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#motivo").val("");
	$("#contMotivo").text("0");
	$("#checkAceptar").prop("checked",false);	
}



//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

function guardar(e){
	
	e.preventDefault();
	if($("#motivo").val()==""){
		Swal.fire({
			icon: 'error',
			title: '¡Complete campos obligatorios!',
		});
	}else{

		var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
		$("#cargandoModal").modal('show');
		$.ajax({
			url:'../ajax/turno.php?op=guardar',//lugar a donde se envia los datos obtenidos del formulario
			type: "POST",
			data: formData, //estos son los datos que envio
			contentType:false,
			processData:false,

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
					$("#cargandoModal").modal('hide');


					if(datos=="¡Complete campos obligatorios!" || datos=='¡Horario ya fue reservado! Elija otro..'){
						Swal.fire({
							icon: 'error',
							title: datos,
						});
					}else{
						if(datos=="¡Turno agendado con exito!"){
							$("#imagenResultado").attr("src","../public/img/true.png");
							$("#resultadoTexto").text("¡Turno agendado con éxito!");
							$("#fechaTexto").text("Para el día: "+$("#fecha").val());
							$("#horaTexto").text("A las: "+$("#hora").val()+" hs");
						}else{
							$("#imagenResultado").attr("src","../public/img/false.png");
							$("#resultadoTexto").text(datos);
						}


						$("#contenidoHeader").hide(); //mostrmos el header
						$("#contenidoForm").hide(); //ocultamos el form
						$("#resultadoPanel").show(); //mostrmos el contenido
						

						limpiar();
					}

					
			}
		});

	}
}



init();