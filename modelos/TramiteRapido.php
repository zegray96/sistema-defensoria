<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class TramiteRapido{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($nroTramite,$anio,$fechaInicio,$fechaFinalizacion,$estado,$requirente,$dni,$telefono,$domicilio,$reclamo,$informe,$idAgente,$ultimaModificacion){
		$sql="INSERT INTO tramites_rapidos (nro_tramite,anio,fecha_inicio,fecha_finalizacion,estado,requirente,dni,telefono,domicilio,reclamo,informe,id_agente,id_ultima_modificacion) VALUES ('$nroTramite','$anio','$fechaInicio','$fechaFinalizacion','$estado','$requirente','$dni','$telefono','$domicilio','$reclamo','$informe','$idAgente','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idTramite,$nroTramite,$anio,$fechaInicio,$fechaFinalizacion,$estado,$requirente,$dni,$telefono,$domicilio,$reclamo,$informe,$idAgente,$ultimaModificacion){
		$sql="UPDATE tramites_rapidos SET nro_tramite='$nroTramite', anio='$anio', fecha_inicio='$fechaInicio', fecha_finalizacion='$fechaFinalizacion', estado='$estado', requirente='$requirente', dni='$dni', telefono='$telefono', domicilio='$domicilio', reclamo='$reclamo', informe='$informe', id_agente='$idAgente', id_ultima_modificacion='$ultimaModificacion'  WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}


	public function buscar($idTramite){
		$sql="SELECT * FROM tramites_rapidos WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idTramite){
		$sql="SELECT * FROM tramites_rapidos WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}

	public function ultimoNro($anio){
        $sql="SELECT nro_tramite+1 as nro_tramite FROM tramites_rapidos WHERE anio='$anio' ORDER BY nro_tramite DESC LIMIT 1";
		return ejecutarConsultaSimpleFila($sql);
    }

    public function comprobarExistenciaNro($nroTramite,$anio){
		$sql="SELECT * FROM tramites_rapidos WHERE nro_tramite='$nroTramite' AND anio='$anio'";
		return ejecutarConsultaSimpleFila($sql);
	}



	public function listar(){
        $sql="SELECT t.id_tramite_rapido, t.nro_tramite, t.anio, t.fecha_inicio, t.fecha_finalizacion, t.estado,
        t.requirente, t.dni, t.telefono, t.domicilio, t.reclamo, a.nombre as agente, t.informe, u.usuario as ultimaModificacion FROM tramites_rapidos t
        INNER JOIN agentes a ON a.id_agente=t.id_agente
        INNER JOIN usuarios u ON u.id_usuario=t.id_ultima_modificacion";
        return ejecutarConsulta($sql);
    }

     public function listarPorAnio($anio){
        $sql="SELECT t.id_tramite_rapido, t.nro_tramite, t.anio, t.fecha_inicio, t.fecha_finalizacion, t.estado,
        t.requirente, t.dni, t.telefono, t.domicilio, t.reclamo, a.nombre as agente, t.informe, u.usuario as ultimaModificacion FROM tramites_rapidos t
        INNER JOIN agentes a ON a.id_agente=t.id_agente
        INNER JOIN usuarios u ON u.id_usuario=t.id_ultima_modificacion
        WHERE t.anio='$anio'";
        return ejecutarConsulta($sql);
    }

    public function buscarPorNro($nroTramite){
        $sql="SELECT t.id_tramite_rapido, t.nro_tramite, t.anio, t.fecha_inicio, t.fecha_finalizacion, t.estado,
        t.requirente, t.dni, t.telefono, t.domicilio, t.reclamo, a.nombre as agente, t.informe, u.usuario as ultimaModificacion FROM tramites_rapidos t
        INNER JOIN agentes a ON a.id_agente=t.id_agente
        INNER JOIN usuarios u ON u.id_usuario=t.id_ultima_modificacion
        WHERE t.nro_tramite='$nroTramite'";
        return ejecutarConsulta($sql);
    }

    public function buscarEntreFechas($fechaIniBuscar,$fechaFinBuscar){
    	$sql="SELECT t.id_tramite_rapido, t.nro_tramite, t.anio, t.fecha_inicio, t.fecha_finalizacion, t.estado,
        t.requirente, t.dni, t.telefono, t.domicilio, t.reclamo, a.nombre as agente, t.informe, u.usuario as ultimaModificacion FROM tramites_rapidos t
        INNER JOIN agentes a ON a.id_agente=t.id_agente
        INNER JOIN usuarios u ON u.id_usuario=t.id_ultima_modificacion
        WHERE (t.fecha_inicio>='$fechaIniBuscar' AND t.fecha_inicio<='$fechaFinBuscar') OR (t.fecha_finalizacion>='$fechaIniBuscar' AND t.fecha_finalizacion<='$fechaFinBuscar') ";
    	return ejecutarConsulta($sql);
    }



    public function pasarAexpediente($idTramite,$idUltimaEdicion){
		$sql="UPDATE tramites_rapidos SET estado='2', id_ultima_modificacion='$idUltimaModificacion' WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}

	public function pasarAtramite($idTramite,$idUltimaEdicion){
		$sql="UPDATE tramites_rapidos SET estado='0', id_ultima_modificacion='$idUltimaModificacion' WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}

	public function buscarIdParaBorrar($idTramite){
		$sql="SELECT * FROM tramites_rapidos  WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}

	public function borrarInforme($idTramite,$ultimaModificacion){
		$sql="UPDATE tramites_rapidos SET informe='', id_ultima_modificacion='$ultimaModificacion' WHERE id_tramite_rapido='$idTramite'";
		return ejecutarConsulta($sql);
	}
   




	
}

?>