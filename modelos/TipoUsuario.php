<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class TipoUsuario{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($nombre,$vistaConfiguracion,$vistaRecursosHumanos,$vistaExpedientes,$vistaTramitesRapidos,$vistaPlanchetas,$vistaReclamos,$vistaRefPreOrg,$vistaAdministracion,$vistaTurnos,$documentosModelo,$altConfiguracion,$altRecursosHumanos,$altExpedientes,$altTramitesRapidos,$altPlanchetas,$altReclamos,$altRefPreOrg,$altAdministracion,$altTurnos,$newConfiguracion,$newRecursosHumanos,$newExpedientes,$newTramitesRapidos,$newPlanchetas,$newReclamos,$newRefPreOrg,$newAdministracion,$newTurnos){
		$sql="INSERT INTO tipos_usuarios (nombre, v_configuracion, v_recursos_humanos, v_expedientes, v_tramites_rapidos, v_planchetas, v_reclamos, v_ref_pre_org, v_administracion, v_turnos, documentos_modelo, alt_configuracion, alt_recursos_humanos, alt_expedientes, alt_tramites_rapidos, alt_planchetas, alt_reclamos, alt_ref_pre_org, alt_administracion, alt_turnos, new_configuracion, new_recursos_humanos, new_expedientes, new_tramites_rapidos, new_planchetas, new_reclamos, new_ref_pre_org, new_administracion, new_turnos) VALUES ('$nombre','$vistaConfiguracion','$vistaRecursosHumanos','$vistaExpedientes','$vistaTramitesRapidos','$vistaPlanchetas','$vistaReclamos','$vistaRefPreOrg','$vistaAdministracion','$vistaTurnos','$documentosModelo','$altConfiguracion','$altRecursosHumanos','$altExpedientes','$altTramitesRapidos','$altPlanchetas','$altReclamos','$altRefPreOrg','$altAdministracion','$altTurnos','$newConfiguracion','$newRecursosHumanos','$newExpedientes','$newTramitesRapidos','$newPlanchetas','$newReclamos','$newRefPreOrg','$newAdministracion','$newTurnos')";
		return ejecutarConsulta($sql);
	}

	public function editar($idTipoUsuario,$nombre,$vistaConfiguracion,$vistaRecursosHumanos,$vistaExpedientes,$vistaTramitesRapidos,$vistaPlanchetas,$vistaReclamos,$vistaRefPreOrg,$vistaAdministracion,$vistaTurnos,$documentosModelo,$altConfiguracion,$altRecursosHumanos,$altExpedientes,$altTramitesRapidos,$altPlanchetas,$altReclamos,$altRefPreOrg,$altAdministracion,$altTurnos,$newConfiguracion,$newRecursosHumanos,$newExpedientes,$newTramitesRapidos,$newPlanchetas,$newReclamos,$newRefPreOrg,$newAdministracion,$newTurnos){
		$sql="UPDATE tipos_usuarios SET nombre='$nombre', v_configuracion='$vistaConfiguracion', v_recursos_humanos='$vistaRecursosHumanos', v_expedientes='$vistaExpedientes', v_tramites_rapidos='$vistaTramitesRapidos', v_planchetas='$vistaPlanchetas', v_reclamos='$vistaReclamos', v_ref_pre_org='$vistaRefPreOrg', v_administracion='$vistaAdministracion', v_turnos='$vistaTurnos',documentos_modelo='$documentosModelo', alt_configuracion='$altConfiguracion', alt_recursos_humanos='$altRecursosHumanos', alt_expedientes='$altExpedientes', alt_tramites_rapidos='$altTramitesRapidos', alt_planchetas='$altPlanchetas', alt_reclamos='$altReclamos', alt_ref_pre_org='$altRefPreOrg', alt_administracion='$altAdministracion', alt_turnos='$altTurnos', new_configuracion='$newConfiguracion', new_recursos_humanos='$newRecursosHumanos', new_expedientes='$newExpedientes', new_tramites_rapidos='$newTramitesRapidos', new_planchetas='$newPlanchetas', new_reclamos='$newReclamos', new_ref_pre_org='$newRefPreOrg', new_administracion='$newAdministracion', new_turnos='$newTurnos' WHERE id_tipo_usuario='$idTipoUsuario'";
		return ejecutarConsulta($sql);
	}

	public function eliminar($idTipoUsuario){
		$sql="DELETE FROM tipos_usuarios WHERE id_tipo_usuario='$idTipoUsuario'";
		return ejecutarConsulta($sql);
	}

	public function buscar($idTipoUsuario){
		$sql="SELECT * FROM tipos_usuarios WHERE id_tipo_usuario='$idTipoUsuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idTipoUsuario){
		$sql="SELECT * FROM tipos_usuarios WHERE id_tipo_usuario='$idTipoUsuario'";
		return ejecutarConsulta($sql);
	}

	public function comprobarExistenciaNombre($nombre){
		$sql="SELECT * FROM tipos_usuarios WHERE nombre='$nombre'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar(){
        $sql="SELECT * FROM tipos_usuarios";
        return ejecutarConsulta($sql);
    }

    public function select(){
        $sql="SELECT * FROM tipos_usuarios";
        return ejecutarConsulta($sql);
    }




	
}

?>