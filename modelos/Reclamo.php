<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Reclamo{
	

	//constructor
	public function __construct(){
		
	}

	public function insertarWeb($apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$fecha,$estado,$via){
		$sql="INSERT INTO reclamos (apellido_nombre,dni,domicilio,telefono,email,reclamo,fecha,estado,via) VALUES ('$apellidoNombre','$dni','$domicilio','$telefono','$email','$reclamo','$fecha','$estado','$via')";
		$idReclamo=ejecutarConsulta_retornarID($sql);
		$nroReclamo="REC-".$idReclamo;
		$sqlAsignarNroReclamo="UPDATE reclamos SET nro_reclamo = '$nroReclamo' WHERE id_reclamo='$idReclamo'";
		$resp=ejecutarConsulta($sqlAsignarNroReclamo);
		if($resp){ //consultamos si el sql salio bien
			$retorno=$idReclamo;
		}else{
			$retorno="¡Reclamo no se pudo enviar!";
		}
		return $retorno;
	}

	public function editarWeb($idReclamo,$respuesta,$tramite,$estado,$ultimaModificacion){
		$sql="UPDATE reclamos SET respuesta='$respuesta', tramite='$tramite', estado='$estado', id_ultima_modificacion='$ultimaModificacion' WHERE id_reclamo='$idReclamo'";
		return ejecutarConsulta($sql);
	}

	public function insertarSistema($apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$respuesta,$tramite,$fecha,$estado,$via,$ultimaModificacion){
		$sql="INSERT INTO reclamos (apellido_nombre,dni,domicilio,telefono,email,reclamo,respuesta,tramite,fecha,estado,via,id_ultima_modificacion) VALUES ('$apellidoNombre','$dni','$domicilio','$telefono','$email','$reclamo','$respuesta','$tramite','$fecha','$estado','$via','$ultimaModificacion')";
		$idReclamo=ejecutarConsulta_retornarID($sql);
		$nroReclamo="REC-".$idReclamo;
		$sqlAsignarNroReclamo="UPDATE reclamos SET nro_reclamo = '$nroReclamo' WHERE id_reclamo='$idReclamo'";
		$resp=ejecutarConsulta($sqlAsignarNroReclamo);
		if($resp){ //consultamos si el sql salio bien
			$retorno=$idReclamo;
		}else{
			$retorno="¡Reclamo no se pudo Crear!";
		}
		return $retorno;
	}

	public function editarSistema($idReclamo,$apellidoNombre,$dni,$domicilio,$telefono,$email,$reclamo,$respuesta,$tramite,$via,$estado,$ultimaModificacion){
		$sql="UPDATE reclamos SET apellido_nombre='$apellidoNombre', dni='$dni', domicilio='$domicilio', telefono='$telefono', email='$email', reclamo='$reclamo', respuesta='$respuesta', tramite='$tramite', via='$via', estado='$estado', id_ultima_modificacion='$ultimaModificacion' WHERE id_reclamo='$idReclamo'";
		return ejecutarConsulta($sql);
	}

	public function buscar($idReclamo){
		$sql="SELECT * FROM reclamos WHERE id_reclamo='$idReclamo'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarNroReclamo($nroReclamo){
		$sql="SELECT * FROM reclamos WHERE nro_reclamo='$nroReclamo'";
		return ejecutarConsultaSimpleFila($sql);
	}



	public function listarPorFecha($fechaListar){
		 $sql="SELECT r.id_reclamo, r.nro_reclamo, r.fecha, r.estado, r.via, r.apellido_nombre, r.dni, r.domicilio, r.telefono, r.email, r.reclamo, r.respuesta, r.tramite, u.usuario as ultimaModificacion FROM reclamos r 
        LEFT JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion WHERE fecha='$fechaListar'";
		return ejecutarConsulta($sql);
	}


	public function listarEntreFechas($fechaIniListar,$fechaFinListar){
		 $sql="SELECT r.id_reclamo, r.nro_reclamo, r.fecha, r.estado, r.via, r.apellido_nombre, r.dni, r.domicilio, r.telefono, r.email, r.reclamo, r.respuesta, r.tramite, u.usuario as ultimaModificacion FROM reclamos r 
        LEFT JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion WHERE fecha>='$fechaIniListar' AND fecha<='$fechaFinListar'";
		return ejecutarConsulta($sql);
	}

	public function listarEnEspera(){
		 $sql="SELECT r.id_reclamo, r.nro_reclamo, r.fecha, r.estado, r.via, r.apellido_nombre, r.dni, r.domicilio, r.telefono, r.email, r.reclamo, r.respuesta, r.tramite, u.usuario as ultimaModificacion FROM reclamos r 
        LEFT JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion WHERE r.estado='EN ESPERA DE RESPUESTA'";
		return ejecutarConsulta($sql);
	}

	public function buscarPorNro($nroReclamo){
        $sql="SELECT r.id_reclamo, r.nro_reclamo, r.fecha, r.estado, r.via, r.apellido_nombre, r.dni, r.domicilio, r.telefono, r.email, r.reclamo, r.respuesta, r.tramite, u.usuario as ultimaModificacion FROM reclamos r 
        LEFT JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion WHERE r.nro_reclamo='$nroReclamo'";
        return ejecutarConsulta($sql);
    }


	

   
   




	
}

?>