<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Decreto{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($nroDecreto,$anio,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion){
		$sql="INSERT INTO decretos (nro_decreto, anio, fecha, detalle, estado, area, id_agente, id_ultima_modificacion) VALUES ('$nroDecreto','$anio','$fecha','$detalle','$estado','$area','$idAgente','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idDecreto,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion){
		$sql="UPDATE decretos SET fecha='$fecha', detalle='$detalle', estado='$estado', area='$area', id_agente='$idAgente', id_ultima_modificacion='$ultimaModificacion' WHERE id_decreto='$idDecreto'";
		return ejecutarConsulta($sql);
	}

	public function ultimoNro($anio){
        $sql="SELECT nro_decreto+1 as nro_decreto FROM decretos WHERE anio='$anio' ORDER BY nro_decreto DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }


	public function buscar($idDecreto){
		$sql="SELECT * FROM decretos WHERE id_decreto='$idDecreto'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idDecreto){
		$sql="SELECT * FROM decretos WHERE id_decreto='$idDecreto'";
		return ejecutarConsulta($sql);
	}

	public function comprobarExistenciaNro($nroDecreto,$anio){
		$sql="SELECT * FROM decretos WHERE nro_decreto='$nroDecreto' AND anio='$anio'";
		return ejecutarConsultaSimpleFila($sql);
	}



	public function listar(){
        $sql="SELECT d.id_decreto, d.nro_decreto, d.anio, d.fecha, d.detalle, d.estado, d.area, a.nombre as agente, u.usuario as ultimaModificacion FROM decretos d
        INNER JOIN usuarios u ON u.id_usuario=d.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=d.id_agente";

        return ejecutarConsulta($sql);
    }

    public function listarPorAnio($anio){
    	$sql="SELECT d.id_decreto, d.nro_decreto, d.anio, d.fecha, d.detalle, d.estado, d.area, a.nombre as agente, u.usuario as ultimaModificacion FROM decretos d
        INNER JOIN usuarios u ON u.id_usuario=d.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=d.id_agente
    	WHERE d.anio='$anio'";
    	return ejecutarConsulta($sql);
    }

    public function buscarPorNro($nroDecreto){
        $sql="SELECT d.id_decreto, d.nro_decreto, d.anio, d.fecha, d.detalle, d.estado, d.area, a.nombre as agente, u.usuario as ultimaModificacion FROM decretos d
        INNER JOIN usuarios u ON u.id_usuario=d.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=d.id_agente
        WHERE d.nro_decreto='$nroDecreto'";
        return ejecutarConsulta($sql);
    }

	
}

?>