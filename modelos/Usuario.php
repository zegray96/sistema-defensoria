<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Usuario{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($nombre,$idTipoUsuario,$usuario,$clave,$estado){
		$sql="INSERT INTO usuarios (nombre, id_tipo_usuario, usuario, clave, estado) VALUES ('$nombre', '$idTipoUsuario', '$usuario','$clave','$estado')";
		return ejecutarConsulta($sql);
	}

	public function editar($idUsuario,$nombre,$idTipoUsuario,$usuario,$estado){
		$sql="UPDATE usuarios SET nombre='$nombre', id_tipo_usuario='$idTipoUsuario', usuario='$usuario', estado='$estado' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	public function editarMiCuenta($idUsuario,$nombre,$usuario){
		$sql="UPDATE usuarios SET nombre='$nombre', usuario='$usuario' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	public function modificarClave($idUsuario,$clave){
		$sql="UPDATE usuarios SET clave='$clave' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	

	public function buscar($idUsuario){
		$sql="SELECT * FROM usuarios  WHERE id_usuario='$idUsuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idUsuario){
		$sql="SELECT * FROM usuarios WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}


	public function comprobarExistenciaUsuario($usuario){
		$sql="SELECT * FROM usuarios WHERE usuario='$usuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function comprobarTipoUsuario($idTipoUsuario){
		$sql="SELECT * FROM usuarios WHERE id_tipo_usuario='$idTipoUsuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar(){
        $sql="SELECT u.id_usuario, u.nombre, u.usuario, u.clave, u.estado, t.nombre as tipo_usuario FROM usuarios u 
		INNER JOIN tipos_usuarios t ON t.id_tipo_usuario=u.id_tipo_usuario";
        return ejecutarConsulta($sql);
    }

    //Funcion para verificar el acceso al sistema
    public function verificar($usuario,$clave){
    	$sql="SELECT u.id_usuario, u.nombre, u.usuario, u.clave, u.estado, tp.id_tipo_usuario as id_tipo_usuario, tp.nombre as tipo_usuario, tp.v_configuracion as v_configuracion, tp.v_recursos_humanos as v_recursos_humanos, tp.v_expedientes as v_expedientes, tp.v_tramites_rapidos as v_tramites_rapidos, tp.v_planchetas as v_planchetas, tp.v_reclamos as v_reclamos, tp.v_ref_pre_org as v_ref_pre_org, tp.v_administracion as v_administracion, tp.v_turnos as v_turnos, tp.documentos_modelo as documentos_modelo, tp.alt_configuracion as alt_configuracion, tp.alt_recursos_humanos as alt_recursos_humanos, tp.alt_expedientes as alt_expedientes, tp.alt_tramites_rapidos as alt_tramites_rapidos, tp.alt_planchetas as alt_planchetas, tp.alt_reclamos as alt_reclamos, tp.alt_ref_pre_org as alt_ref_pre_org, tp.alt_administracion as alt_administracion, tp.alt_turnos as alt_turnos, tp.new_configuracion as new_configuracion, tp.new_recursos_humanos as new_recursos_humanos, tp.new_expedientes as new_expedientes, tp.new_tramites_rapidos as new_tramites_rapidos, tp.new_planchetas as new_planchetas, tp.new_reclamos as new_reclamos, tp.new_ref_pre_org as new_ref_pre_org, tp.new_administracion as new_administracion, tp.new_turnos as new_turnos FROM usuarios u
    	INNER JOIN tipos_usuarios tp ON tp.id_tipo_usuario=u.id_tipo_usuario
    	 WHERE u.usuario='$usuario' AND u.clave='$clave' AND u.estado='ACTIVO'";
    	return ejecutarConsulta($sql);
    }

    




	
}

?>