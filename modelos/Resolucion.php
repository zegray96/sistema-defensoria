<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Resolucion{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($nroResolucion,$anio,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion){
		$sql="INSERT INTO resoluciones (nro_resolucion, anio, fecha, detalle, estado, area, id_agente, id_ultima_modificacion) VALUES ('$nroResolucion','$anio','$fecha','$detalle','$estado','$area','$idAgente','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idResolucion,$fecha,$detalle,$estado,$area,$idAgente,$ultimaModificacion){
		$sql="UPDATE resoluciones SET fecha='$fecha', detalle='$detalle', estado='$estado', area='$area', id_agente='$idAgente', id_ultima_modificacion='$ultimaModificacion' WHERE id_resolucion='$idResolucion'";
		return ejecutarConsulta($sql);
	}

	public function ultimoNro($anio){
        $sql="SELECT nro_resolucion+1 as nro_resolucion FROM resoluciones WHERE anio='$anio' ORDER BY nro_resolucion DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }


	public function buscar($idResolucion){
		$sql="SELECT * FROM resoluciones WHERE id_resolucion='$idResolucion'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idResolucion){
		$sql="SELECT * FROM resoluciones WHERE id_resolucion='$idResolucion'";
		return ejecutarConsulta($sql);
	}

	public function comprobarExistenciaNro($nroResolucion,$anio){
		$sql="SELECT * FROM resoluciones WHERE nro_resolucion='$nroResolucion' AND anio='$anio'";
		return ejecutarConsultaSimpleFila($sql);
	}


	public function listar(){
        $sql="SELECT r.id_resolucion, r.nro_resolucion, r.anio, r.fecha, r.detalle, r.estado, r.area, a.nombre as agente, u.usuario as ultimaModificacion FROM resoluciones r
        INNER JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=r.id_agente";
        return ejecutarConsulta($sql);
    }

    public function listarPorAnio($anio){
    	$sql="SELECT r.id_resolucion, r.nro_resolucion, r.anio, r.fecha, r.detalle, r.estado, r.area, a.nombre as agente, u.usuario as ultimaModificacion FROM resoluciones r
        INNER JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=r.id_agente
    	WHERE r.anio='$anio'";
    	return ejecutarConsulta($sql);
    }


    public function buscarPorNro($nroResolucion){
        $sql="SELECT r.id_resolucion, r.nro_resolucion, r.anio, r.fecha, r.detalle, r.estado, r.area, a.nombre as agente, u.usuario as ultimaModificacion FROM resoluciones r
        INNER JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion
        LEFT JOIN agentes a ON a.id_agente=r.id_agente
        WHERE r.nro_resolucion='$nroResolucion'";
        return ejecutarConsulta($sql);
    }

	
}

?>