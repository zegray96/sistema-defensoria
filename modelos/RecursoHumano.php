<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class RecursoHumano{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($apellidoNombre,$dni,$cuil,$situacionRevista,$categoria,$nroLegajo,$email,$telefono,$domicilio,$imagenDniAdelante,$imagenDniAtras,$ultimaModificacion){
		$sql="INSERT INTO recursos_humanos (apellido_nombre, dni, cuil, situacion_revista, categoria, nro_legajo, email, telefono, domicilio, imagen_dni_adelante, imagen_dni_atras, id_ultima_modificacion) VALUES ('$apellidoNombre','$dni','$cuil','$situacionRevista','$categoria','$nroLegajo','$email','$telefono','$domicilio','$imagenDniAdelante','$imagenDniAtras','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idPersonal,$apellidoNombre,$dni,$cuil,$situacionRevista,$categoria,$nroLegajo,$email,$telefono,$domicilio,$imagenDniAdelante,$imagenDniAtras,$ultimaModificacion){
		$sql="UPDATE recursos_humanos SET apellido_nombre='$apellidoNombre', dni='$dni', cuil='$cuil', situacion_revista='$situacionRevista', categoria='$categoria', nro_legajo='$nroLegajo', email='$email', telefono='$telefono', domicilio='$domicilio', imagen_dni_adelante='$imagenDniAdelante', imagen_dni_atras='$imagenDniAtras', id_ultima_modificacion='$ultimaModificacion' WHERE id_personal='$idPersonal'";
		return ejecutarConsulta($sql);
	}

	public function eliminar($idPersonal){
		
		$sql="DELETE FROM recursos_humanos WHERE id_personal='$idPersonal'";
		return ejecutarConsulta($sql);
	}

	public function buscar($idPersonal){
		$sql="SELECT * FROM recursos_humanos WHERE id_personal='$idPersonal'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarIdParaBorrar($idPersonal){
		$sql="SELECT * FROM recursos_humanos WHERE id_personal='$idPersonal'";
		return ejecutarConsulta($sql);
	}



	public function listar(){
        $sql="SELECT r.id_personal, r.apellido_nombre, r.dni, r.cuil, r.situacion_revista, r.categoria, r.nro_legajo, r.email, r.telefono, r.domicilio, r.imagen_dni_adelante, r.imagen_dni_atras, u.usuario as ultimaModificacion FROM recursos_humanos r
        INNER JOIN usuarios u ON u.id_usuario=r.id_ultima_modificacion";
        return ejecutarConsulta($sql);
    }

    




	
}

?>