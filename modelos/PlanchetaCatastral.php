<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class PlanchetaCatastral{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($referencia,$plancheta,$ultimaModificacion){
		$sql="INSERT INTO planchetas_catastrales (referencia,plancheta,id_ultima_modificacion) VALUES ('$referencia','$plancheta','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idPlancheta,$referencia,$plancheta,$ultimaModificacion){
		$sql="UPDATE planchetas_catastrales SET referencia = '$referencia', plancheta='$plancheta',id_ultima_modificacion='$ultimaModificacion' WHERE id_plancheta='$idPlancheta'";
		return ejecutarConsulta($sql);
	}


	public function buscar($idPlancheta){
		$sql="SELECT * FROM planchetas_catastrales WHERE id_plancheta='$idPlancheta'";
		return ejecutarConsultaSimpleFila($sql);
	}



	public function buscarId($idPlancheta){
		$sql="SELECT * FROM planchetas_catastrales WHERE id_plancheta='$idPlancheta'";
		return ejecutarConsulta($sql);
	}



	public function listar(){
        $sql="SELECT p.id_plancheta, p.referencia, p.plancheta, u.usuario as ultimaModificacion FROM planchetas_catastrales p
        INNER JOIN usuarios u ON u.id_usuario=p.id_ultima_modificacion"
        ;
        return ejecutarConsulta($sql);
    }

	
	public function comprobarExistenciaRef($referencia){
		$sql="SELECT * FROM planchetas_catastrales WHERE referencia='$referencia'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function borrarPlancheta($idPlancheta,$ultimaModificacion){
		$sql="UPDATE planchetas_catastrales SET plancheta='', id_ultima_modificacion='$ultimaModificacion' WHERE id_plancheta='$idPlancheta'";
		return ejecutarConsulta($sql);
	}


   




	
}

?>