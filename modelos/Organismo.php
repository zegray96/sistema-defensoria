<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Organismo{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($idOrgNombre,$autoridad,$telefono,$email){
		$sql="INSERT INTO organismos (autoridad,id_org_nombre,telefono,email) VALUES ('$autoridad','$idOrgNombre','$telefono','$email')";
		return ejecutarConsulta($sql);
	}

	public function insertarOrgNombre($orgNombre){
		$sql="INSERT INTO org_nombres (nombre) VALUES ('$orgNombre')";
		return ejecutarConsulta($sql);
	}

	public function editar($idOrganismo,$idOrgNombre,$autoridad,$telefono,$email){
		$sql="UPDATE organismos SET autoridad='$autoridad', id_org_nombre='$idOrgNombre', telefono='$telefono', email='$email' WHERE id_organismo='$idOrganismo'";
		return ejecutarConsulta($sql);
	}

	public function comprobarExistenciaNombre($nombre){
		$sql="SELECT * FROM org_nombres WHERE nombre='$nombre'";
		return ejecutarConsultaSimpleFila($sql);
	}



	public function buscar($idOrganismo){
		$sql="SELECT * FROM organismos WHERE id_organismo='$idOrganismo'";
		return ejecutarConsultaSimpleFila($sql);
	}


	public function listar(){
        $sql="SELECT o.id_organismo, org.nombre as orgNombre, o.autoridad, o.telefono, o.email FROM organismos o
        INNER JOIN org_nombres org ON org.id_org_nombre=o.id_org_nombre";
        return ejecutarConsulta($sql);
    }

    public function select(){
        $sql="SELECT * FROM org_nombres";
        return ejecutarConsulta($sql);
    }

    public function eliminar($idOrganismo){
		$sql="DELETE FROM organismos WHERE id_organismo='$idOrganismo'";
		return ejecutarConsulta($sql);
	}

	
}

?>