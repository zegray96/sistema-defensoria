<?php
//Inculuimos la conexion a la bd
require "../config/Conexion.php";
Class Expediente{
	

	//constructor
	public function __construct(){
		
	}

	public function insertar($fecha,$nroCompleto,$nro,$anio,$requirente,$tramite,$idAgente,$informeInicial,$informeQuince,$informeTreinta,$informeCuarentaCinco,$informeMensual,$resolucion,$observacion,$estado,$ultimaModificacion){
		$sql="INSERT INTO expedientes (fecha,nro_expediente,nro,anio,requirente,tramite,id_agente,informe_inicial,informe_quince,informe_treinta,informe_cuarenta_cinco,informe_mensual,resolucion,observacion,estado,id_ultima_modificacion) VALUES ('$fecha','$nroCompleto','$nro','$anio','$requirente','$tramite','$idAgente','$informeInicial','$informeQuince','$informeTreinta','$informeCuarentaCinco','$informeMensual','$resolucion','$observacion','$estado','$ultimaModificacion')";
		return ejecutarConsulta($sql);
	}

	public function editar($idExpediente,$fecha,$nroCompleto,$nro,$anio,$requirente,$tramite,$idAgente,$informeInicial,$informeQuince,$informeTreinta,$informeCuarentaCinco,$informeMensual,$resolucion,$observacion,$estado,$ultimaModificacion){
		$sql="UPDATE expedientes SET fecha = '$fecha', nro_expediente='$nroCompleto', nro='$nro', anio='$anio', requirente='$requirente', tramite='$tramite', id_agente='$idAgente', informe_inicial='$informeInicial', informe_quince='$informeQuince', informe_treinta='$informeTreinta', informe_cuarenta_cinco='$informeCuarentaCinco', informe_mensual='$informeMensual', resolucion='$resolucion', observacion='$observacion', estado='$estado', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}


	public function buscar($idExpediente){
		$sql="SELECT * FROM expedientes e WHERE id_expediente='$idExpediente'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscarId($idExpediente){
		$sql="SELECT * FROM expedientes e WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function listar(){
        $sql="SELECT e.id_expediente, e.fecha, e.nro_expediente, e.nro, e.anio, e.requirente, e.tramite, a.nombre as agente, e.estado, e.informe_inicial, e.informe_quince, e.informe_treinta, e.informe_cuarenta_cinco, e.informe_mensual, e.resolucion, e.observacion, u.usuario as ultimaModificacion FROM expedientes e
        INNER JOIN agentes a ON a.id_agente=e.id_agente
        INNER JOIN usuarios u ON u.id_usuario=e.id_ultima_modificacion"
        ;
        return ejecutarConsulta($sql);
    }

    public function desactivar($idExpediente){
		$sql="UPDATE expedientes SET estado='0' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function activar($idExpediente){
		$sql="UPDATE expedientes SET estado='1' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}
	
	public function comprobarExistenciaNro($nro,$anio){
		$sql="SELECT * FROM expedientes WHERE nro='$nro' AND anio='$anio'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function borrarInformeInicial($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET informe_inicial='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function borrarInformeQuince($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET informe_quince='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function borrarInformeTreinta($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET informe_treinta='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function borrarInformeCuarentaCinco($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET informe_cuarenta_cinco='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function borrarInformeMensual($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET informe_mensual='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function borrarResolucion($idExpediente,$ultimaModificacion){
		$sql="UPDATE expedientes SET resolucion='', id_ultima_modificacion='$ultimaModificacion' WHERE id_expediente='$idExpediente'";
		return ejecutarConsulta($sql);
	}

	public function ultimoNro($anio){
		$sql="SELECT nro+1 as nro FROM expedientes WHERE anio='$anio' ORDER BY nro DESC LIMIT 1";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listarPorRevisar($fechaActual){
		$sql="SELECT e.id_expediente, e.fecha, e.nro_expediente, e.nro, e.anio, e.requirente, e.tramite, a.nombre as agente, e.estado, e.informe_inicial, e.informe_quince, e.informe_treinta, e.informe_cuarenta_cinco, e.informe_mensual, e.resolucion, e.observacion, u.usuario as ultimaModificacion FROM expedientes e 
		INNER JOIN agentes a ON a.id_agente=e.id_agente
        INNER JOIN usuarios u ON u.id_usuario=e.id_ultima_modificacion
		WHERE (DATEDIFF( '$fechaActual',e.fecha)>7 AND e.informe_inicial='' AND e.id_agente!=17 AND e.fecha>='2020-01-01') 
		OR (DATEDIFF( '$fechaActual',e.fecha)>15 AND e.informe_quince='' AND e.id_agente!=17 AND e.fecha>='2020-01-01')";
		return ejecutarConsulta($sql);
	}


	public function listarPorAnio($varAnio){
		$sql="SELECT e.id_expediente, e.fecha, e.nro_expediente, e.nro, e.anio, e.requirente, e.tramite, a.nombre as agente, e.id_agente, e.estado, e.informe_inicial, e.informe_quince, e.informe_treinta, e.informe_cuarenta_cinco, e.informe_mensual, e.resolucion, e.observacion, u.usuario as ultimaModificacion FROM expedientes e 
		INNER JOIN agentes a ON a.id_agente=e.id_agente
        INNER JOIN usuarios u ON u.id_usuario=e.id_ultima_modificacion
		WHERE e.anio='$varAnio'";
		return ejecutarConsulta($sql);
	}


	public function contarRegistrosPorRevisar($fechaActual){
		$sql="SELECT COUNT(e.id_expediente) as suma FROM expedientes e 
		WHERE (DATEDIFF( '$fechaActual',e.fecha)>7 AND e.informe_inicial='' AND e.id_agente!=17 AND e.fecha>='2020-01-01') 
		OR (DATEDIFF( '$fechaActual',e.fecha)>15 AND e.informe_quince='' AND e.id_agente!=17 AND e.fecha>='2020-01-01')";
		return ejecutarConsultaSimpleFila($sql);
	}

	


	



	

   




	
}

?>