<?php

include '../config/global.php';
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */


 
// // DB table to use
$table = 'listar_tramites_rapidos';
 
// Table's primary key
$primaryKey = 'id_tramite_rapido';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id_tramite_rapido', 'dt' => 'id_tramite_rapido'),
    array( 'db' => 'nro_tramite',  'dt' => 'nro_tramite'),
    array( 'db' => 'anio',  'dt' => 'anio'),
    array( 'db' => 'fecha_inicio',  'dt' => 'fecha_inicio',
	'formatter' => function( $d, $row ) {
            return date('Y-m-d', strtotime($d));
        }
	),
    array( 'db' => 'fecha_finalizacion',  'dt' => 'fecha_finalizacion',
    'formatter' => function( $d, $row ) {
            return date('Y-m-d', strtotime($d));
        }
    ),
    array( 'db' => 'estado',  'dt' => 'estado'),
    array( 'db' => 'requirente',  'dt' => 'requirente'),
    array( 'db' => 'dni',  'dt' => 'dni'),
    array( 'db' => 'telefono',  'dt' => 'telefono'),
    array( 'db' => 'domicilio',  'dt' => 'domicilio'),
    array( 'db' => 'reclamo',  'dt' => 'reclamo'),
    array( 'db' => 'agente',  'dt' => 'agente'),
    array( 'db' => 'informe',  'dt' => 'informe'),
    array( 'db' => 'ultimaModificacion',  'dt' => 'ultimaModificacion'),


    
);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USERNAME,
    'pass' => DB_PASSWORD,
    'db'   => DB_NAME,
    'host' => DB_HOST
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
