<?php

include '../config/global.php';
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */


 
// // DB table to use
$table = 'listar_reclamos';
 
// Table's primary key
$primaryKey = 'id_reclamo';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id_reclamo', 'dt' => 'id_reclamo'),
    array( 'db' => 'nro_reclamo', 'dt' => 'nro_reclamo'),
    array( 'db' => 'estado',  'dt' => 'estado'),
    array( 'db' => 'via',  'dt' => 'via'),
    array( 'db' => 'fecha',  'dt' => 'fecha',
    'formatter' => function( $d, $row ) {
            return date('Y-m-d', strtotime($d));
        }
    ),
    array( 'db' => 'apellido_nombre',  'dt' => 'apellido_nombre'),
    array( 'db' => 'dni',  'dt' => 'dni'),
    array( 'db' => 'domicilio',  'dt' => 'domicilio'),
    array( 'db' => 'telefono',  'dt' => 'telefono'),
    array( 'db' => 'email',  'dt' => 'email'),
    array( 'db' => 'reclamo',  'dt' => 'reclamo'),
    array( 'db' => 'respuesta',  'dt' => 'respuesta'),
    array( 'db' => 'tramite',  'dt' => 'tramite'),
    array( 'db' => 'ultimaModificacion',  'dt' => 'ultimaModificacion'),
);
 
// SQL server connection information
$sql_details = array(
    'user' => DB_USERNAME,
    'pass' => DB_PASSWORD,
    'db'   => DB_NAME,
    'host' => DB_HOST
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
