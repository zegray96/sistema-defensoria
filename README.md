## Configuracion de entorno
- Tener instalado laragon, xampp, etc
- Tener instalado mysql, mariadb, etc

## Configuracion de proyecto
- Clonar proyecto: 
- - git clone
- Crear bd e importar datos de defensoria_bd.sql
- Cambiar datos de conexion y proyecto a bd en config/global.php
- Usuario para ingresar:
  - GZEGRAY
  - 40043456

