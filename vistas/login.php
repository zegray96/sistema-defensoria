
<!DOCTYPE html>
<html>
  <head>
    <?php
    include ('../config/version.php');
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Iniciar Sesión</title>
    <!--icono de pagina-->
    <link rel="shortcut icon" href="../public/img/ico.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/all.min.css">
   
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../public/css/blue.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="../public/img/logoLargoSinBorde.png" style="display:block; margin:auto; width: 300px; margin-bottom: -35px;" >
      </div><!-- /.login-logo -->
      <div class="login-box-body">
    
        <form method="post" id="frmAcceso">

         <div class="form-group">
          <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input autocomplete="off" type="text" class="form-control" id="usuarioAcceso" name="usuarioAcceso" placeholder="Usuario" required="required" onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">       
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" id="claveAcceso" name="claveAcceso" placeholder="CLAVE" required="required"><a style="cursor:pointer;" class="input-group-addon" onclick="mostrarClave()"><i id="iconoVer" class="fas fa-eye"></i></a>       
            </div>
        </div>  

        <div style="background-color: #f2dede !important; color: #a94442 !important" class="alert">
          <strong>AVISO:</strong> Clave distingue entre minúsculas y mayúsculas.
        </div>

          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-lg-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div><!-- /.col -->
          </div>
        </form>


                         
                          

        <!--Modal -->
        <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
              </div>
            </div>
          </div> 
        </div>

        
    
        

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->


    <!-- jQuery 3.3.1 -->
    <script src="../public/js/jquery-3.3.1.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- bOOTBOX -->
    <script src="../public/js/bootbox.min.js"></script>
    <!-- sweet alert -->
    <script src="../public/js/sweetalert2.all.min.js"></script>
    <!-- login.js -->
    <script type="text/javascript" src="scripts/login.js?ver=<?php echo $version; ?>"></script>
    
  </body>
</html>
