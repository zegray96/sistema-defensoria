<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{

require('../public/fpdf182/fpdf.php');

require_once "../modelos/Reclamo.php";



class PDF extends FPDF{
	// Cabecera de página
	function Header(){

		$r = new Reclamo();
		$respuesta = $r->buscar($_GET['id']);
		$fechaBd = $respuesta["fecha"];
		$fecha = date("d-m-Y", strtotime($fechaBd));
		$nroReclamo = $respuesta["nro_reclamo"];
		
		$this->SetFillColor(111,111,111);

		$this->Rect(5, 5, 200, 287, 'D'); //For A4
	    // Logo
	    $this->Image('../public/img/logoLargoSinBorde.png',7,3,60);
	    // Arial bold 10
	    $this->SetFont('Arial','B',20);

	    $this->Cell(70);
	    $this->SetTextColor(255,255,255);
	    $this->Cell(120,10,'Formulario de Reclamo',0,0,'C',1);

	    $this->SetTextColor(0,0,0);
	    $this->Ln(13);
	    $this->Cell(69);
	    $this->SetFont('Arial','B',12);
	    $this->Cell(10,9,utf8_decode('N°: '),0 ,0, 'C',false);
	    $this->SetFont('Arial','',12);
	    $this->Cell(-1);
	    $this->Cell(57,9,'  '.$nroReclamo,1,0,'L');

	    $this->Cell(10);
	    $this->SetFont('Arial','B',12);
	    $this->Cell(15,9,utf8_decode('Fecha: '),0 ,0, 'C');
	    $this->SetFont('Arial','',12);
	    $this->Cell(0.5);
	    $this->Cell(30,9,'  '.$fecha,1,0,'L');



	    

	    

	}

	// Pie de página
	function Footer(){
		
	   	// Posición: a 1,5 cm del final
	    $this->SetY(-20);
	    // Arial italic 8
	    $this->SetFont('Arial','B',14);
	    // Número de página
	    $this->Cell(50,10,'Firma Requirente: ........................................');
	}
}



// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->SetTitle('Formulario de Reclamo');
// Contenido de la tabla
$pdf->AddPage('portrait','A4');
$pdf->Ln(4);

// Comenzamos a crear las filas de los registros segun la consulta mysql
$r = new Reclamo();
$respuesta = $r->buscar($_GET['id']);

$nroReclamo = $respuesta["nro_reclamo"];
$apellidoNombre =$respuesta["apellido_nombre"];
$dni = $respuesta["dni"];
$domicilio = substr($respuesta["domicilio"], 0, 75);
$telefono = $respuesta["telefono"];
$email = $respuesta["email"];
$reclamo = html_entity_decode($respuesta["reclamo"],ENT_QUOTES);
$resp = html_entity_decode($respuesta["respuesta"],ENT_QUOTES);


$pdf->SetFillColor(111,111,111);

$pdf->SetFont('Arial','B',12);
$pdf->Ln(12);
$pdf->SetTextColor(255,255,255);
$pdf->Cell(130,9,utf8_decode(' Apellido y Nombre: '),0,0,'L',1);
$pdf->Cell(13);
$pdf->Cell(47,9,' Dni: ',0,0,'L',1);

$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(130,9,utf8_decode('  '.$apellidoNombre),1,0,'L');
$pdf->Cell(13);
$pdf->Cell(47,9,'  '.$dni,1,0,'L');

$pdf->SetFont('Arial','B',12);
$pdf->Ln(12);
$pdf->SetTextColor(255,255,255);
$pdf->Cell(190,9,utf8_decode(' Domicilio: '),0,0,'L',1);
$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(190,9,utf8_decode('  '.$domicilio),1,0,'L');

$pdf->SetFont('Arial','B',12);
$pdf->Ln(12);
$pdf->SetTextColor(255,255,255);
$pdf->Cell(90,9,utf8_decode(' Telefono: '),0,0,'L',1);
$pdf->Cell(13);
$pdf->Cell(87,9,' Email: ',0,0,'L',1);

$pdf->SetFont('Arial','',12);
$pdf->Ln(10);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(90,9,'  '.$telefono,1,0,'L');
$pdf->Cell(13);
$pdf->Cell(87,9,utf8_decode('  '.$email),1,0,'L');

$pdf->SetFont('Arial','B',12);
$pdf->Ln(18);
$pdf->SetTextColor(255,255,255);
$pdf->Cell(190,9,utf8_decode(' Reclamo: '),0,0,'L',1);

$pdf->SetFont('Arial','',12);
$pdf->Ln(12);
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell(190,9,utf8_decode($reclamo),1,'L',false);
$pdf->Cell(15);


if($resp==""){
	$pdf->SetFont('Arial','B',12);
	$pdf->Ln(10);
	$pdf->SetTextColor(255,255,255);
	$pdf->Cell(190,9,utf8_decode(' Respuesta: '),0,0,'L',1);

	$pdf->SetFont('Arial','',12);
	$pdf->Ln(12);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(190,40,'',1,'L',false);
	$pdf->Cell(15);
}else{
	$pdf->SetFont('Arial','B',12);
	$pdf->Ln(10);
	$pdf->SetTextColor(255,255,255);
	$pdf->Cell(190,9,utf8_decode(' Respuesta: '),0,0,'L',1);

	$pdf->SetFont('Arial','',12);
	$pdf->Ln(12);
	$pdf->SetTextColor(0,0,0);
	$pdf->MultiCell(190,9,utf8_decode($resp),1,'L',false);
	$pdf->Cell(15);
}










$pdf->Output('I','formulario'.$nroReclamo.'.pdf');





}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>
