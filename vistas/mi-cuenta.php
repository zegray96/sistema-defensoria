<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fa fa-cog"></i> Mi Cuenta</h1>
                    </div>

                    <div class="box-header with-border">
                      
                    </div>

                    <div class="panel-body" id="formularioRegistros"> 
                         
                          
                          <form name="formulario" id=formulario method="POST">

                            <div class="formMedio">  
                              
                              
                              <div class="formSelect">
                                <label style="color: red;  font-size:15px">(*) Campos Obligatorios </label>
                              </div>

                              <label><label style="color: red;">(*)</label> Nombre: </label>
                              <input value="<?php echo $_SESSION['nombre'];?>" type="text" class="texto form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
                            
                              

                              <label><label style="color: red;">(*)</label> Usuario: </label>

                              <input value="<?php echo $_SESSION['usuario'];?>" type="text" class="texto form-control" name="usuario" id="usuario" maxlength="50" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
                              

                              <div id="btnModificarClave" style="margin-bottom: 20px;"><a data-toggle="modal" href="#modalModificarClave" class="btn btn-info"><i class="fas fa-lock"></i> Modificar clave</a></div>

                              
                             
                                
                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="location.href='escritorio'"><i class="fa fa-arrow-circle-left"></i> Cancelar</button> 
                              
                              

                            </div>  
                            
                          </form>

                    
                      
                    </div>
                    <!--Fin centro -->

                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->


  <!--Modal modificar clave -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalModificarClave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div style="text-align: center; margin-top: 10px">
             <h3 style="text-align: center" class="modal-title">Ingrese nueva clave</h3>
          </div>
        </div>
        <div class="modal-body">
          <form name="formularioModificarClave" id="formularioModificarClave" method="POST">
            
            <input type="text" class="texto form-control" name="nuevaClave" id="nuevaClave" maxlength="100" placeholder="CLAVE" required>          
        </div> 
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarNuevaClave()"><i class="fas fa-times"></i> Cerrar</button>
            <button class="btn btn-primary" type="submit" id="btnGuardarClave"><i class="fa fa-save"></i> Guardar</button>  
          </form> 

        </div>
      </div>
    </div> 
  </div>

  <!--Fin Modal modificar clave-->

  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/miCuenta.js?ver=<?php echo $version; ?>"></script>
<?php

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>



