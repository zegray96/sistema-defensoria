<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>
<!-- CSS PARA IMPRIMIR CALENDARIO -->
<link rel="stylesheet" href="../public/css/fullcalendar.print.css">

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">   
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fas fa-user-clock"></i> Turnos</h1>
                    </div>
                    
                    <?php
                      if($_SESSION['v_turnos']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado

                      	
                    ?>

                    <div class="box-header with-border">
                      <div id="divSelectListar" class="box-header">
                        <select id="listar" class="form-control selectpicker">
                          <option value="todos" selected="true">TODOS</option>
                          <option value="hoy">HOY</option>
                        </select>
                      </div>

                      <button id="btnCalendario" class="btn btn-warning"><i class="far fa-calendar"></i> Calendario</button>  
                      <button id="btnListado" class="btn btn-primary"><i class="fa fa-table"></i> Listado</button>     

                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body" id="calendario">
 
                    </div>

                    <div class="panel-body table-responsive" id="listadoRegistros">
                      <table id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%">
                        <thead>
                            <th>&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Hora&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Telefono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Motivo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            
                        </thead>

                      </table>
                    </div>

                    <!--Fin centro -->

                    <?php
                              }
                              //Fin contenido autorizado
                    ?>  
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->


  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
	
  <!--Modal Dia -->
  <div class="modal fade" data-backdrop="static" id="modalDia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header bg-primary">
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true" style="color: white; font-size: 35px; display: inline-block;">&times;</span>
  				</button>	 
  				<h3 class="modal-title" id="tituloEvento"></h3> 
  				
  			</div>
  			<div class="modal-body">

  				<form name="formulario" id="formulario" method="POST">
            
            <div class="formSelect">
              <label style="color: red; font-size: 15px">(*) Campos Obligatorios</label>
            </div>

  					<input type="hidden" name="idTurno" id="idTurno">
  					
  					<label><label style="color: red;">(*)</label> Fecha: </label><input required readonly type="date" name="fecha" id="fecha" class="form-control" style="width:160px;" onkeydown="return false"><br>

  					<label><label style="color: red;">(*)</label> Hora: </label>
  					<div class="formSelect" id="hora">
                      
                <!-- horarios disponibles -->
                  
            </div>

  					<label><label style="color: red;">(*)</label> Apellido y Nombre: </label><input type="text" name="apellidoNombre" id="apellidoNombre" class="form-control" placeholder="NOMBRE" required onkeyup="capitalizar('apellidoNombre')"><br>

            <label><label style="color: red;">(*)</label> Dni: </label><input type="text" maxlength="10" name="dni" id="dni" class="form-control" placeholder="DNI" required onkeyup="format(this)" onchange="format(this)"><br>

            <label><label style="color: red;">(*)</label> Telefono: </label><input type="text" name="telefono" id="telefono" placeholder="TELEFONO" class="form-control" required><br>

            <label> Email: </label><input type="text" name="email" id="email" placeholder="EMAIL" class="form-control"><br>

  					<label><label style="color: red;">(*)</label> Motivo: </label> <label style="font-weight: normal" id="contMotivo">0</label> <label style="font-weight: normal">/ 100 Caracteres</label>
            <textarea placeholder="MOTIVO" maxlength="100" id="motivo" name="motivo" rows="2" class="form-control" style="resize: none" required onkeyup="return contadorCaracteres('#motivo','#contMotivo')"></textarea><br> 
  					
  				</div> 
  				<div class="modal-footer">
  					<button type="button" class="btn btn-danger" id="btnEliminar" onclick="eliminar()">Eliminar</button>
  					<button type="button" class="btn btn-warning" id="btnEditar" onclick="editar()">Editar</button>
  					<button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
  				</form>	

  			</div>
  		</div>
  	</div> 
  </div>

  <!--Fin Modal Dia -->



<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/turno.js?ver=<?php echo $version; ?>"></script>
<?php

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>



