    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> <?php echo $version; ?>
        </div>
        <strong>&copy; Defensoria del Pueblo - Posadas Misiones</strong> 
    </footer> 
  


    <!-- jQuery 3.3.1 -->
    <script src="../public/js/jquery-3.3.1.js"></script>
    <!--Moment js-->
    <script src="../public/js/moment.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../public/js/app.min.js"></script>

   <!--datatables-->
   <script src="../public/js/jquery.dataTables.min.js"></script>   
   <script src="../public/js/dataTables.bootstrap.min.js"></script>
   <script src="../public/js/datatables.min.js"></script>
   <script src="../public/js/dataTables.buttons.min.js"></script>
   <script src="../public/js/buttons.html5.min.js"></script>
   <script src="../public/js/buttons.colVis.min.js"></script>
   <script src="../public/js/jszip.min.js"></script>
   <script src="../public/js/pdfmake.min.js"></script>
   <script src="../public/js/vfs_fonts.js"></script>
   <!--bootboox-->
   <script src="../public/js/bootbox.min.js"></script>
   <!--bootstrap select-->
   <script src="../public/js/bootstrap-select.min.js"></script>
   <!-- sweet alert -->
   <script src="../public/js/sweetalert2.all.min.js"></script>
   <!--Full calendar-->
   <script src="../public/js/fullcalendar.min.js"></script> 
   <!--Traduccion al español de full calendar--> 
   <script src="../public/js/es.js"></script>
   


  </body>
</html>
