<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
  //Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title"  style="font-size:20px; font-weight: 600;" ><i class="fa fa-desktop"></i> Escritorio</h1>
                    </div>
                    

                    <div class="panel-body">

                      

					<?php 
					if($_SESSION['v_reclamos']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="reclamos">
							<div class="small-box btn-primary">
								<div class="inner">
									<i style="font-size: 30px;" class="far fa-clipboard"></i>
									<p style="font-size: 20px;">Reclamos</p>
								</div>
								<div class="icon">
									<i class="far fa-clipboard"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          

					<?php 
					if($_SESSION['v_tramites_rapidos']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="tramites-rapidos">
							<div class="small-box btn-warning">
								<div class="inner">
									<i style="font-size: 30px;" class="fas fa-book-open"></i>
									<p style="font-size: 20px;">Trámites Rápidos</p>
								</div>
								<div class="icon">
									<i class="fas fa-book-open"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          

					<?php 
					if($_SESSION['v_expedientes']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="expedientes">
							<div class="small-box btn-success">
								<div class="inner">
									<i style="font-size: 30px;" class="fa fa-folder-open"></i>
									<p style="font-size: 20px;">Expedientes</p>
								</div>
								<div class="icon">
									<i class="fa fa-folder-open"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			         


	
					<?php 
					if($_SESSION['v_planchetas']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="planchetas-catastrales">
							<div class="small-box btn-info">
								<div class="inner">
									<i style="font-size: 30px;" class="fa fa-map"></i>
									<p style="font-size: 20px;">Planchetas Catastrales</p>
								</div>
								<div class="icon">
									<i class="fa fa-map"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          

					<?php 
					if($_SESSION['v_recursos_humanos']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="recursos-humanos">
							<div class="small-box btn-danger">
								<div class="inner">
									<i style="font-size: 30px;" class="fa fa-users"></i>
									<p style="font-size: 20px;">RRHH</p>
								</div>
								<div class="icon">
									<i class="fa fa-users"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          


					<?php 
					if($_SESSION['v_ref_pre_org']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="referentes-presidentes">
							<div class="small-box btn-info">
								<div class="inner">
									<i style="font-size: 30px;" class="fas fa-user-friends"></i>
									<p style="font-size: 20px;">Ref/Presidentes</p>
								</div>
								<div class="icon">
									<i class="fas fa-user-friends"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-6">
						<a href="organismos">
							<div class="small-box btn-info">
								<div class="inner">
									<i style="font-size: 30px;" class="fas fa-landmark"></i>
									<p style="font-size: 20px;">Organismos</p>
								</div>
								<div class="icon">
									<i class="fas fa-landmark"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          

					<?php 
					if($_SESSION['v_turnos']==1){
					?>
					<div class="col-lg-3 col-6">
						<a href="turnos">
							<div class="small-box btn-info">
								<div class="inner">
									<i style="font-size: 30px;" class="fas fa-user-clock"></i>
									<p style="font-size: 20px;">Turnos</p>
								</div>
								<div class="icon">
									<i class="fas fa-user-clock"></i>
								</div>
								<div class="small-box-footer">
									Ingresar <i class="fa fa-arrow-circle-right"></i>
								</div>
							</div>
						</a>
					</div>
					<?php	
					}
					?>
			          

			        <?php 
			        if($_SESSION['v_administracion']==1){
			        ?>
			        <div class="col-lg-3 col-6">
			        	<a href="decretos">
			        		<div class="small-box btn-primary">
			        			<div class="inner">
			        				<i style="font-size: 30px;" class="fas fa-file-alt"></i>
			        				<p style="font-size: 20px;">Decretos</p>
			        			</div>
			        			<div class="icon">
			        				<i class="fas fa-file-alt"></i>
			        			</div>
			        			<div class="small-box-footer">
			        				Ingresar <i class="fa fa-arrow-circle-right"></i>
			        			</div>
			        		</div>
			        	</a>
			        </div>


			        <div class="col-lg-3 col-6">
			        	<a href="resoluciones">
			        		<div class="small-box btn-primary">
			        			<div class="inner">
			        				<i style="font-size: 30px;" class="fas fa-file-alt"></i>
			        				<p style="font-size: 20px;">Resoluciones</p>
			        			</div>
			        			<div class="icon">
			        				<i class="fas fa-file-alt"></i>
			        			</div>
			        			<div class="small-box-footer">
			        				Ingresar <i class="fa fa-arrow-circle-right"></i>
			        			</div>
			        		</div>
			        	</a>
			        </div>

			        <?php	
			        }
			        ?>

			          

			          



			        </div>

			        
                         
                         
                          
                      
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->
  

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
  
  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>

<?php

}
 //Fin llave
ob_end_flush(); //libera el espacio del buffer
?>


