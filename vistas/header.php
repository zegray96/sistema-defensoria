
<!DOCTYPE html>
<html>
  <head>
    <?php
    include ('../config/version.php')
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema Defensoría</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--icono del sitio-->
    <link rel="shortcut icon" href="../public/img/ico.png">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!--bootstrap select-->
    <link rel="stylesheet" href="../public/css/bootstrap-select.min.css">
    <!-- Font Awesome 5-->
    <link rel="stylesheet" href="../public/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../public/css/_all-skins.min.css">
    <!--Datatables-->
    <link rel="stylesheet" type="text/css" href="../public/css/datatables.bootstrap.min.css">
    <!--Full calendar-->
    <link rel="stylesheet" href="../public/css/fullcalendar.min.css">
    

    <!-- Mi css propio -->
    <link rel="stylesheet" href="../public/css/estilos.css?ver=<?php echo $version; ?>">
    

    





  </head>
  <body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="escritorio" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <!--<span class="logo-mini"><b>D</b></span>-->
          <img src="../public/img/logoCortoConBorde.png" class="logo-mini" style="height: 50px; width: 145px;">
          <!-- logo for regular state and mobile devices -->
          <!--<span class="logo-lg"><b>Defensoría</b></span>-->
          <img src="../public/img/logoLargoConBorde.png" style="width: 145px; margin: 0 auto;" class="logo-lg">
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../public/img/usuario.png" class="user-image" alt="User Image">
                  <?php
                 
                  echo '<span class="hidden-xs">'.$_SESSION['nombre'].'</span>';
                
                    
                  ?>
                  
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../public/img/usuario.png" class="img-circle" alt="User Image">
                   
                      <?php
                        echo '<p> <strong>Usuario: </strong>'.$_SESSION['usuario'].'</p>
                        <p> <strong>Tipo Usu.: </strong>'.$_SESSION['tipo_usuario'].'</p>';
                        
                      ?>         
                    
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left" style="width: 50px;">
                      <a href="mi-cuenta" class="btn btn-default">Mi Cuenta</a>
                    </div>
                    
                    <div class="pull-right">
                      <a onclick="salir();"  class="btn btn-default">Cerrar Sesión</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">       
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>

            <li>
              <a href="escritorio">
               <i class="fa fa-desktop"></i> <span>Escritorio</span>
              </a>
            </li>
            
            <!-- VERIFICACION DE ACCESO CONFIGURACION -->
            <?php 
              if($_SESSION['v_configuracion']==1){
            ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Configuración</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="usuarios"><i class="fa fa-user"></i> Usuarios</a></li>
                <li><a href="tipos-usuarios"><i class="fa fa-user-plus"></i> Tipos Usuarios</a></li>
                <li><a href="agentes"><i class="fa fa-users"></i><span> Agentes</span></a></li>
                
              </ul>
            </li>            
            <?php    
              }
            ?>
            
            <!-- VERIFICACION DE ACCESO RECLAMOS -->
            <?php 
            if($_SESSION['v_reclamos']==1){
            ?>
            <li>
              <a href="reclamos">
                <i class="far fa-clipboard"></i> <span>&nbsp;&nbsp;Reclamos</span>
              </a>   
            </li> 
            <?php
            }
            ?>
            

            <!-- VERIFICACION DE ACCESO T RAPIDOS -->
            <?php 
            if($_SESSION['v_tramites_rapidos']==1){
            ?>
            <li class="treeview">
              <a href="tramites-rapidos">
                <i class="fas fa-book-open"></i> <span>&nbsp;Trámites Rápidos</span>
              </a>
            </li>
            <?php
            }
            ?>
            
            <!-- VERIFICACION DE ACCESO EXPEDIENTES -->
            <?php 
            if($_SESSION['v_expedientes']==1){
            ?>
            <li>
              <a href="expedientes">
                <i class="fa fa-folder-open"></i> <span>Expedientes</span>
              </a>   
            </li>
            <?php  
            }
            ?>
            
            <!-- VERIFICACION DE ACCESO A PLANCHETAS -->
            <?php 
            if($_SESSION['v_planchetas']==1){
            ?>
            <li>
              <a href="planchetas-catastrales">
                <i class="fa fa-map"></i> <span> P. Catastrales</span>
              </a>   
            </li>
            <?php  
            }
            ?>
            
            <!-- VERIFICACION ACCESO RRHH -->
            <?php 
            if($_SESSION['v_recursos_humanos']==1){
            ?>
            <li>
              <a href="recursos-humanos">
                <i class="fa fa-users"></i> <span>RRHH</span>
              </a>   
            </li>
            <?php  
            }
            ?>
    
          

          <?php 
          // inicio agenda
          if($_SESSION['v_ref_pre_org']==1 || $_SESSION['v_turnos']==1){
          ?>
          <li class="treeview">
            <a href="#">
              <i class="fas fa-address-book"></i><span>&nbsp;&nbsp;&nbsp;Agenda</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">

              <!-- VERIFICACION DE ACCESO REF PRE ORG  -->
              <?php 
              if($_SESSION['v_ref_pre_org']==1){
                ?>           
                <li><a href="referentes-presidentes"><i class="fas fa-user-friends"></i>&nbsp;&nbsp;Ref/Presidentes</a></li>
                <li><a href="organismos"><i class="fas fa-landmark"></i>&nbsp;&nbsp;&nbsp;Organismos</a></li> 
                <?php  
              }
              ?>

              <!-- VERIFICACION DE ACCESO TURNOS -->
              <?php 
              if($_SESSION['v_turnos']==1){
                ?>           
                <li><a href="turnos"><i class="fas fa-user-clock"></i>&nbsp;&nbsp;Turnos</a></li>
                <?php  
              }
              ?>

            </ul>
          </li>
          <?php
          // fin llave agenda
          }
          ?>

          
          
          
            
    
          <!-- VERIFIACION DE ACCESO ADMINISTRACION -->
          <?php 
          if($_SESSION['v_administracion']==1){
          ?>
          <li class="treeview">
            <a href="#">
              <i class="fas fa-book"></i><span>&nbsp;&nbsp;&nbsp;Administración</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="decretos"><i class="fas fa-file-alt"></i>&nbsp;&nbsp;Decretos</a></li>
              <li><a href="resoluciones"><i class="fas fa-file-alt"></i>&nbsp;&nbsp;Resoluciones</a></li> 

            </ul>
          </li>
          <?php  
          }
          ?>

          
          <!-- VERIFIACION DE ACCESO DOC MODELOS -->
          <?php
            //Documentos modelo
          if($_SESSION['documentos_modelo']==1){
            ?>  

            <li class="treeview">
              <a href="#">
                <i class="fas fa-file-word"></i><span>&nbsp;&nbsp;&nbsp;Docs. Modelo</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">

                <li class="treeview menu-open" >
                  <a href="#"><i class="fas fa-arrow-circle-right"></i>
                    <span class="pull-right-container" style="margin-left: 10px"> Informes
                      <i style="padding-right: 15px !important;" class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <!--contenido -->
                    <li><a href="../files/documentosModelo/informeTramiteRapido.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe T. Rápido</a></li>
                    <li><a href="../files/documentosModelo/informeInicial.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe Inicial</a></li>
                    <li><a href="../files/documentosModelo/informe15dias.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe 15 Días</a></li>
                    <li><a href="../files/documentosModelo/informe30dias.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe 30 Días</a></li>
                    <li><a href="../files/documentosModelo/informe45dias.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe 45 Días</a></li>
                    <li><a href="../files/documentosModelo/informeMensual.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Informe Mensual</a></li>
                  </ul>
                </li>

                <li class="treeview menu-open">
                  <a href="#"><i class="fas fa-arrow-circle-right"></i>
                    <span class="pull-right-container" style="margin-left: 10px"> Resoluciones
                      <i style="padding-right: 15px !important;" class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <!--contenido -->
                    <li><a href="../files/documentosModelo/resolucionCierre.docx?<?php echo rand(); ?>" ><i class="fas fa-file-word"></i> Cierre Expediente</a></li>
                  </ul>
                </li>

                <li class="treeview menu-open">
                  <a href="#"><i class="fas fa-arrow-circle-right"></i>
                    <span class="pull-right-container" style="margin-left: 10px"> Notas y Cedulas
                      <i style="padding-right: 15px !important;" class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <!--contenido -->
                    <li><a href="../files/documentosModelo/notaConArticulado.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Nota Con Art.</a></li>
                    <li><a href="../files/documentosModelo/notaSinArticulado.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Nota Sin Art.</a></li>    
                    <li><a href="../files/documentosModelo/cedula.docx?<?php echo rand(); ?>"><i class="fas fa-file-word"></i> Cedula</a></li>  
                  </ul>
                </li>

                <li class="treeview menu-open">
                  <a href="#"><i class="fas fa-arrow-circle-right"></i>
                    <span class="pull-right-container" style="margin-left: 10px"> Planillas
                      <i style="padding-right: 15px !important;" class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <!--contenido -->
                    <li><a href="../files/planillas/planilla_tramites_rapidos.pdf?<?php echo rand(); ?>" target="blank"><i class="fas fa-file-pdf"></i> Planilla T. Rápido</a></li>
                  </ul>
                </li>



              </ul>
            </li>






            <?php
          }
            //Fin documentos modelo
          ?>   

            
            
                        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>


<script type="text/javascript" src="scripts/salir.js?ver=<?php echo $version; ?>"></script>
