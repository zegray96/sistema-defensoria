var tabla;


//Funcion que se ejecuta al inicio
function init(){

	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})

	$("#formularioModificarClave").on("submit", function(e){ //si en el form se activa el submit se hara esto
		modificarClave(e);
	})

	cargarSelect();

	

	
}

//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//funcion limpiar
function limpiar(){
	$("#idUsuario").val("");
	$("#idModificarClave").val("");
	$("#nombre").val("");
	$("#idTipoUsuario").val("");
	$("#usuario").val("");
	$("#clave").val("");
	$("#estado").val("1");
	$('#estado').selectpicker('refresh');

}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnNuevo").hide();
		$("#btnGuardar").prop("disabled",false);
		$("#clave").attr("readonly", false);
		$("#divClave").show();
		$("#btnModificarClave").hide();
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		cargarSelect();

	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}




//funcion para listar
function listar(){
	
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[5, 100, 1000], [5, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla

		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		], 
		"ajax":{
			url:'../ajax/usuario.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
	

}

function limpiarNuevaClave(){
	$("#nuevaClave").val("");
	$("#modalModificarClave").modal('hide');
}



//funcion para modificar clave
function modificarClave(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($("#formularioModificarClave")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/usuario.php?op=modificarClave',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			
			if(datos=="¡Clave modificada con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
				});
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}
			limpiarNuevaClave();
			
		}
	});

}


//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/usuario.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡Usuario Ya Existe!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
				


			}
			
		}
	});

}

//funcion mostrar
function mostrar(idUsuario){
	$("#cargandoModal").modal('show');
	$.post("../ajax/usuario.php?op=mostrar", {idUsuario: idUsuario}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idUsuario").val(data.id_usuario);
		$("#idModificarClave").val(data.id_usuario);
		$("#nombre").val(data.nombre);
		$("#idTipoUsuario").val(data.id_tipo_usuario);
		$('#idTipoUsuario').selectpicker('refresh');
		$("#usuario").val(data.usuario);
		$("#clave").val("********");
		$("#clave").attr("readonly", true); 
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');

		$("#btnModificarClave").show();


      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}
	//funcion para desactivar
	function desactivar(idUsuario){
		Swal.fire({
		  title: '¿Esta seguro que desea cambiar el estado?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  
		}).then((result) => {
		  if (result.value) {
		    	$("#myModal").modal('show');
				$.post("../ajax/usuario.php?op=desactivar", {idUsuario : idUsuario}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Registro se cambio a estado 'INACTIVO'!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#myModal").modal('hide');

				});
		  }
		});

		
	}


	//funcion para activar
	function activar(idUsuario){
		Swal.fire({
		  title: '¿Esta seguro que desea cambiar el estado?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true
		}).then((result) => {
		  if (result.value) {
		    	$("#myModal").modal('show');
				$.post("../ajax/usuario.php?op=activar", {idUsuario : idUsuario}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Registro se cambio a estado 'ACTIVO'!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#myModal").modal('hide');

				});
		  }
		});
	}

	function cargarSelect(){
	//cargamos los items al select
	$.post("../ajax/usuario.php?op=selectTipoUsuario", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idTipoUsuario").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idTipoUsuario').selectpicker('refresh'); //refresco el select

	});	
}


init();