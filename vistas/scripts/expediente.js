var tabla;
var alteracion;
var idTipoUsuario;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	$("#divTituloRegistrosPorRevisar").hide();
	
	mostrarForm(false);
	listarPorAnio();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});

	cargarSelectAgentes();

	registrosParaRevisar();
	


	
}

// cambio en select agente
$("#idAgente").change(function (){
	if ($(this).val() == 17){
		$("#requirente").val("DEFENSORIA ADMINISTRACION");
	}
});

// listar por anio
$("#listarPorAnio").change(function (){
	if ($(this).val() === 'todos') {
		listar();
	}else{
		if($(this).val() === 'porRevisar'){
			listarPorRevisar();
		}else{
			listarPorAnio();
		}
	}

});

// traemos los registros que faltan revisar
function registrosParaRevisar(){	
	$.ajax({
			url:'../ajax/expediente.php?op=contarRegistrosPorRevisar',//lugar a donde se envia los datos obtenidos del formulario
			type: "get",

			success: function(data){ //si la accion se hace de forma correcta se hace esto
			 	data = JSON.parse(data);
			 	var cantRegistrosPorRevisar = data.suma;

			 	Swal.fire({
			 		title: 'Existen ('+cantRegistrosPorRevisar+') registros por revisar',
			 		icon: 'warning',
			 		showCancelButton: true,
			 		confirmButtonColor: '#3085d6',
			 		cancelButtonColor: '#d33',
			 		confirmButtonText: 'Revisar',
			 		cancelButtonText: 'Cancelar',
			 		allowOutsideClick: false,
			 		reverseButtons: true
			 	}).then((result) => {
			 		if (result.value) {
			 			listarPorRevisar();
			 		}
			 	}); 

			 	if(cantRegistrosPorRevisar>0){
			 		$("#divTituloRegistrosPorRevisar").show();
			 		$("#cantRegistrosPorRevisar").text('('+cantRegistrosPorRevisar+') registros por revisar');
			 	}
			}
	});

}

// traemos los registros que faltan revisar despues de guardar
function registrosParaRevisarDespuesDeGuardar(){	
	$.ajax({
			url:'../ajax/expediente.php?op=contarRegistrosPorRevisar',//lugar a donde se envia los datos obtenidos del formulario
			type: "get",

			success: function(data){ //si la accion se hace de forma correcta se hace esto
			 	data = JSON.parse(data);
			 	var cantRegistrosPorRevisar = data.suma;

			 	if(cantRegistrosPorRevisar>0){
			 		$("#divTituloRegistrosPorRevisar").show();
			 		$("#cantRegistrosPorRevisar").text('('+cantRegistrosPorRevisar+') registros por revisar');
			 	}
			}
	});

}


//cambio en la fecha
$("#anio").change(function(){
	$.ajax({
			url:'../ajax/expediente.php?op=ultimoNro',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'anio': $("#anio").val()}, //estos son los datos que envio
			


			success: function(data){ //si la accion se hace de forma correcta se hace esto
			 	data = JSON.parse(data);
			 	if(data===null){
					var nro=1;
					var nroMostrar=nro.toString().padStart(3, "0");
					$("#nro").val(nroMostrar);
				}else{
					var nroMostrar=data.nro.toString().padStart(3, "0");
					$("#nro").val(nroMostrar);
				}
				$("#divNro").show();
				$("#divLetra").show();
				$("#nro").focus();
			 	

			}
	});

	


});


//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//validar informe inicial
function validarInfInicial(){
        var archivo = $("#informeInicial").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informeInicial").val(null);
		}
}

//validar informe 15 dias
function validarInfQuince(){
        var archivo = $("#informeQuince").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informeQuince").val(null);
		}
}

//validar informe 30 dias
function validarInfTreinta(){
        var archivo = $("#informeTreinta").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informeTreinta").val(null);
		}
}

//validar informe 45 dias
function validarInfCuarentaCinco(){
        var archivo = $("#informeCuarentaCinco").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informeCuarentaCinco").val(null);
		}
}

//validar informe mensual
function validarInfMensual(){
        var archivo = $("#informeMensual").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informeMensual").val(null);
		}
}

//validar informe mensual
function validarRes(){
        var archivo = $("#resolucion").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#resolucion").val(null);
		}
}




	







//borrar pdf
function borrarArchivo(valor){
	var nombreArchivo;
	Swal.fire({
		  title: '¿Esta seguro que desea eliminar el archivo?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');				
				$.ajax({
						url:'../ajax/expediente.php?op=borrarArchivo',//lugar a donde se envia los datos obtenidos del formulario
						type: "get",
						data: {'valor': valor, 'idEx': $("#idExpediente").val()}, //estos son los datos que envio
						

						success: function(datos){ //si la accion se hace de forma correcta se hace esto
							$("#cargandoModal").modal('hide');			
								if(datos=="¡Archivo eliminado con exito!"){

									Swal.fire({
										icon: 'success',
										title: datos,
									});
									if(valor=='inicial'){
										$("#borrarInformeInicial").hide();
										$("#enlaceInformeInicial").hide();
										$("#informeInicialActual").val("");
									}else{
										if(valor=='quince'){
											$("#borrarInformeQuince").hide();
											$("#enlaceInformeQuince").hide();
											$("#informeQuinceActual").val("");
										}else{
											if(valor=='treinta'){
												$("#borrarInformeTreinta").hide();
												$("#enlaceInformeTreinta").hide();
												$("#informeTreintaActual").val("");
											}else{
												if(valor=='cuarentaCinco'){
													$("#borrarInformeCuarentaCinco").hide();
													$("#enlaceInformeCuarentaCinco").hide();
													$("#informeCuarentaCincoActual").val("");
												}else{
													if(valor=='mensual'){
														$("#borrarInformeMensual").hide();
														$("#enlaceInformeMensual").hide();
														$("#informeMensualActual").val("");
													}else{
														if(valor='resolucion'){
															$("#borrarResolucion").hide();
															$("#enlaceResolucion").hide();
															$("#resolucionActual").val("");
														}
													}
												}
											}
										}
									}
									
									
								}else{

									Swal.fire({
										icon: 'error',
										title: datos,
									});

								}
							
						}
					});

				
		  }
	});
}




//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//funcion limpiar
function limpiar(){
	$("#idExpediente").val("");
	$("#fecha").val("");

	$("#letra").val("");
	$('#letra').selectpicker('refresh');
	$("#nro").val("");
	$('#nro').selectpicker('refresh');
	$("#anio").val("");
	$('#anio').selectpicker('refresh');

	$("#requirente").val("");
	$("#tramite").val("");
	$("#idAgente").val("");
	$("#informeInicialActual").val("");
	$("#informeQuinceActual").val("");
	$("#informeTreintaActual").val("");
	$("#informeCuarentaCincoActual").val("");
	$("#informeMensualActual").val("");
	$("#resolucionActual").val("");
	$("#observacion").val("");
	$("#informeInicial").val("");
	$("#informeQuince").val("");
	$("#informeTreinta").val("");
	$("#informeCuarentaCinco").val("");
	$("#informeMensual").val("");
	$("#resolucion").val("");
	$("#estado").val("");
	$('#estado').selectpicker('refresh');
	$("#contTramite").text("0");
	$("#contObservacion").text("0");



	
}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#divSelectListar").hide();
		$("#btnNuevo").hide();
		$("#divNro").hide();
		$("#divLetra").hide();
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		$("#divSelectListar").show();
		$("#enlaceInformeInicial").hide();		
		$("#enlaceInformeQuince").hide();
		$("#enlaceInformeTreinta").hide();
		$("#enlaceInformeCuarentaCinco").hide();
		$("#enlaceInformeMensual").hide();
		$("#enlaceResolucion").hide();
		$("#borrarInformeInicial").hide();
		$("#borrarInformeQuince").hide();
		$("#borrarInformeTreinta").hide();
		$("#borrarInformeCuarentaCinco").hide();
		$("#borrarInformeMensual").hide();
		$("#borrarResolucion").hide();
		cargarSelectAgentes();
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}



//funcion para listar registros por anio
function listarPorAnio(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,6,7,14]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/expediente.php?op=listarPorAnio',
			data: {'varAnio': $("#listarPorAnio").val()},
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

		"bDestroy": true,
		"order":[[4,"desc"]] //para ordenar los registros
	}).DataTable();
}



//funcion para listar registros por revisar
function listarPorRevisar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,6,7,14]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/expediente.php?op=listarPorRevisar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

		"bDestroy": true,
		"order":[[2,"desc"]] //para ordenar los registros
	}).DataTable();
}



//funcion para listar
function listar(){
	//traemos con ajax la variable de sesion de alteracion
	$.get("../ajax/expediente.php?op=traerAlteracion", function(data, status){
		alteracion=data; //variable para comprobar la alteracion
	});

	//traemos con ajax la variable de sesion de tipo de usuario
	$.get("../ajax/expediente.php?op=traerIdTipoUsuario", function(data, status){
		idTipoUsuario=data; 
	});

	
	tabla=$("#tblListado").dataTable({
		"processing": true, //Activamos el procesamiento de datatables
		"serverSide": true, //Paginacion y filtrado relizados por el servidor
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,6,7,14]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		], 


		"ajax": "../serverSide/server-side-expedientes.php",
		

		//datos de la tabla
		columns: [
			{"data": "id_expediente"}, //va el nombre del dt de server side personas
			{"data": null, //columna opciones
			      "render": function(data, type, full) {

			      	if(idTipoUsuario==1 || idTipoUsuario==5 || idTipoUsuario==7){
			      		if(alteracion==1){ //comprobacion de si puede alterar o no, para mostrar el boton
				      		data="<button class='btn btn-warning' onclick=mostrar("+full["id_expediente"]+")><i class='fas fa-pencil-alt'></i></button>";
				      	}else{
				      		data=null;
				      	}
				    }else{ // si no es de esos tipos de usuarios
				    	if(full["id_agente"]==17){ //comprobamos si el agente es administracion
				    		data=null;
				    	}else{ //si no es el agente administracion
				    		if(alteracion==1){ //comprobacion de si puede alterar o no, para mostrar el boton
					      		data="<button class='btn btn-warning' onclick=mostrar("+full["id_expediente"]+")><i class='fas fa-pencil-alt'></i></button>";
					      	}else{
					      		data=null;
					      	}
				    	}

				    }

			      	
			      	return data;
			      }
			},      
			{"data": "fecha"},
			{"data": "estado",
					"render": function ( data, type, row ) {
                                    if(data=="EN TRAMITE"){
										data='<span class="label bg-orange">'+data+'</span>';
									}else{
										if(data=="CERRADO"){
											data='<span class="label bg-green">'+data+'</span>';
										}else{
											if(data=="JURIDICO"){
												data='<span class="label bg-blue">'+data+'</span>';
											}
												if(data=="PARA CIERRE"){
													data='<span class="label bg-red">'+data+'</span>';
												}		
										}
									}	
                                  	return data;
						},

			},
			{"data": "nro_expediente"},
			{"data": "requirente"},
			{"data": "tramite", //columna tramite
			      "render": function(data, type, full) {
			      	if(idTipoUsuario==1 || idTipoUsuario==5 || idTipoUsuario==7){ // si es uno de esos tipos de usuarios mostramos el tramite
			      		data=full["tramite"];
			      	}else{
			      		if(full["agente"]=='ADMINISTRACION'){ //sino, ocultamos los tramites de administracion
			      			data=null;
			      		}else{
			      			data=full["tramite"];
			      		}
			      		
			      	}
			      	return data;
			      }
			}, 
			{"data": "agente"},
			{"data": "informe_inicial",
				"render": function ( data, type, row ) {
					var fechaInforme = row['fecha'];
					var fechaHoy = moment(new Date());
					var diferencia = fechaHoy.diff(fechaInforme, 'days');
					// se contara a partir del 2020
					if(row['fecha']>='2020-01-01' && diferencia>7 && data=="" && row['id_agente']!=17){
						return '<span class="label bg-red">FALTA CARGAR</span>';
					}else{
						return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
					}
				},

			},
			{"data": "informe_quince",
				"render": function ( data, type, row ) {
					var fechaInforme = row['fecha'];
					var fechaHoy = moment(new Date());
					var diferencia = fechaHoy.diff(fechaInforme, 'days');
					// se contara a partir del 2020 
					if(row['fecha']>='2020-01-01' && diferencia>15 && data=="" && row['id_agente']!=17){
						return '<span class="label bg-red">FALTA CARGAR</span>';
					}else{
						return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
					}
				},
			},
			{"data": "informe_treinta",
				"render": function ( data, type, row ) {
                    return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},
			},
			{"data": "informe_cuarenta_cinco",
				"render": function ( data, type, row ) {
                    return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},
			},
			{"data": "informe_mensual",
				"render": function ( data, type, row ) {
                    return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},
			},
			{"data": "resolucion",
				"render": function ( data, type, row ) {
                    return '<a href="../files/informesExpedientes/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},
			},
			{"data": "observacion"},
			{"data": "ultimaModificacion"},
			
		],


		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

        "bDestroy": true,
		"order":[[2,"desc"]], //para ordenar los registros



	}).DataTable();

	
}





//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);

	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/expediente.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');

			if(datos=="¡N° de Expediente ya Existe!" || datos=="¡Ingrese N° y Letra de expediente!" 
				|| datos=="¡No posee permisos para modificar este registro!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
				$("#nroExpediente").focus();
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
					registrosParaRevisarDespuesDeGuardar();
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
			}
			
		}
	});
	
	
	
	

}

//funcion mostrar
function mostrar(idExpediente){
	$("#cargandoModal").modal('show');

	$.post("../ajax/expediente.php?op=mostrar", {idExpediente: idExpediente}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }


        //traemos con ajax la variable de sesion de tipo de usuario
		$.get("../ajax/expediente.php?op=traerIdTipoUsuario", function(data, status){
			idTipoUsuario=data; 
		});

		$("#cargandoModal").modal('hide');

		// si el agente es administracion
        if(data.id_agente==17){
        	// si pertenece a uno de estos tipos de usuario
        	if(idTipoUsuario==1 || idTipoUsuario==5 || idTipoUsuario==7){
        		/* Trabajamos habitualmente con la respuesta */
				mostrarForm(true);
				// mostramos los divs ocultos
				$("#divNro").show();
				$("#divLetra").show();

				$("#idExpediente").val(data.id_expediente);
				$("#fecha").val(data.fecha);
				
				var n=data.nro_expediente;
				var array = n.split('-');
				
				var nro=array[0];
				var letra=array[1];

				$("#nro").val(nro);
				$('#nro').selectpicker('refresh');

				$("#letra").val(letra);
				$('#letra').selectpicker('refresh');
				
				$("#anio").val(data.anio);
				$('#anio').selectpicker('refresh');

				$("#requirente").val(data.requirente);
				$("#tramite").val(data.tramite);
				$("#idAgente").val(data.id_agente);
				$('#idAgente').selectpicker('refresh');
				$("#informeInicialActual").val(data.informe_inicial);
				$("#informeQuinceActual").val(data.informe_quince);
				$("#informeTreintaActual").val(data.informe_treinta);
				$("#informeCuarentaCincoActual").val(data.informe_cuarenta_cinco);
				$("#informeMensualActual").val(data.informe_mensual);
				$("#resolucionActual").val(data.resolucion);
				$("#observacion").val(data.observacion);
				$('#estado').val(data.estado);
				$('#estado').selectpicker('refresh');
				contadorCaracteres('#tramite','#contTramite');
				contadorCaracteres('#observacion','#contObservacion');

				
				if(!data.informe_inicial==""){
					$("#enlaceInformeInicial").show();
					$("#enlaceInformeInicial").attr('href', '../files/informesExpedientes/'+data.informe_inicial+"?"+Math.random());
					$("#borrarInformeInicial").show();
					//document.getElementById("enlaceInforme").href ="../files/informes/"+data.informe;
				}

				if(!data.informe_quince==""){
					$("#enlaceInformeQuince").show();
					//document.getElementById("#enlaceInformeQuince").href ="../files/informes/"+data.informeQuince;
					$("#enlaceInformeQuince").attr('href', '../files/informesExpedientes/'+data.informe_quince+"?"+Math.random());
					$("#borrarInformeQuince").show();
				}

				if(!data.informe_treinta==""){
					$("#enlaceInformeTreinta").show();
					//document.getElementById("enlaceInformeTreinta").href ="../files/informes/"+data.informeTreinta;
					$("#enlaceInformeTreinta").attr("href", "../files/informesExpedientes/"+data.informe_treinta+"?"+Math.random());
					$("#borrarInformeTreinta").show();
				}

				if(!data.informe_cuarenta_cinco==""){
					$("#enlaceInformeCuarentaCinco").show();
					//document.getElementById("enlaceInformeCuarentaCinco").href ="../files/informes/"+data.informeCuarentaCinco;
					$("#enlaceInformeCuarentaCinco").attr("href", "../files/informesExpedientes/"+data.informe_cuarenta_cinco+"?"+Math.random());
					$("#borrarInformeCuarentaCinco").show();
				}

				if(!data.informe_mensual==""){
					$("#enlaceInformeMensual").show();
					//document.getElementById("enlaceInformeSesenta").href ="../files/informes/"+data.informeSesenta;
					$("#enlaceInformeMensual").attr("href", "../files/informesExpedientes/"+data.informe_mensual+"?"+Math.random());
					$("#borrarInformeMensual").show();
				}

				if(!data.resolucion==""){
					$("#enlaceResolucion").show();
					//document.getElementById("enlaceResolucion").href ="../files/informes/"+data.resolucion;
					$("#enlaceResolucion").attr("href", "../files/informesExpedientes/"+data.resolucion+"?"+Math.random());
					$("#borrarResolucion").show();
				}
        	}
        // si no es el agente administracion 
        }else{
        	
        	/* Trabajamos habitualmente con la respuesta */
			mostrarForm(true);
			// mostramos los divs ocultos
			$("#divNro").show();
			$("#divLetra").show();

			$("#idExpediente").val(data.id_expediente);
			$("#fecha").val(data.fecha);
			
			var n=data.nro_expediente;
			var array = n.split('-');
			
			var nro=array[0];
			var letra=array[1];

			$("#nro").val(nro);
			$('#nro').selectpicker('refresh');

			$("#letra").val(letra);
			$('#letra').selectpicker('refresh');
			
			$("#anio").val(data.anio);
			$('#anio').selectpicker('refresh');

			$("#requirente").val(data.requirente);
			$("#tramite").val(data.tramite);
			$("#idAgente").val(data.id_agente);
			$('#idAgente').selectpicker('refresh');
			$("#informeInicialActual").val(data.informe_inicial);
			$("#informeQuinceActual").val(data.informe_quince);
			$("#informeTreintaActual").val(data.informe_treinta);
			$("#informeCuarentaCincoActual").val(data.informe_cuarenta_cinco);
			$("#informeMensualActual").val(data.informe_mensual);
			$("#resolucionActual").val(data.resolucion);
			$("#observacion").val(data.observacion);
			$('#estado').val(data.estado);
			$('#estado').selectpicker('refresh');
			contadorCaracteres('#tramite','#contTramite');
			contadorCaracteres('#observacion','#contObservacion');

			
			if(!data.informe_inicial==""){
				$("#enlaceInformeInicial").show();
				$("#enlaceInformeInicial").attr('href', '../files/informesExpedientes/'+data.informe_inicial+"?"+Math.random());
				$("#borrarInformeInicial").show();
				//document.getElementById("enlaceInforme").href ="../files/informes/"+data.informe;
			}

			if(!data.informe_quince==""){
				$("#enlaceInformeQuince").show();
				//document.getElementById("#enlaceInformeQuince").href ="../files/informes/"+data.informeQuince;
				$("#enlaceInformeQuince").attr('href', '../files/informesExpedientes/'+data.informe_quince+"?"+Math.random());
				$("#borrarInformeQuince").show();
			}

			if(!data.informe_treinta==""){
				$("#enlaceInformeTreinta").show();
				//document.getElementById("enlaceInformeTreinta").href ="../files/informes/"+data.informeTreinta;
				$("#enlaceInformeTreinta").attr("href", "../files/informesExpedientes/"+data.informe_treinta+"?"+Math.random());
				$("#borrarInformeTreinta").show();
			}

			if(!data.informe_cuarenta_cinco==""){
				$("#enlaceInformeCuarentaCinco").show();
				//document.getElementById("enlaceInformeCuarentaCinco").href ="../files/informes/"+data.informeCuarentaCinco;
				$("#enlaceInformeCuarentaCinco").attr("href", "../files/informesExpedientes/"+data.informe_cuarenta_cinco+"?"+Math.random());
				$("#borrarInformeCuarentaCinco").show();
			}

			if(!data.informe_mensual==""){
				$("#enlaceInformeMensual").show();
				//document.getElementById("enlaceInformeSesenta").href ="../files/informes/"+data.informeSesenta;
				$("#enlaceInformeMensual").attr("href", "../files/informesExpedientes/"+data.informe_mensual+"?"+Math.random());
				$("#borrarInformeMensual").show();
			}

			if(!data.resolucion==""){
				$("#enlaceResolucion").show();
				//document.getElementById("enlaceResolucion").href ="../files/informes/"+data.resolucion;
				$("#enlaceResolucion").attr("href", "../files/informesExpedientes/"+data.resolucion+"?"+Math.random());
				$("#borrarResolucion").show();
			}
        }
        


		
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}


function cargarSelectAgentes(){
	//cargamos los items al select
	$.post("../ajax/expediente.php?op=selectAgente", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idAgente").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idAgente').selectpicker('refresh'); //refresco el select

	});	
}



//funcion para desactivar
function desactivar(idExpediente){
		Swal.fire({
		  title: '¿Esta seguro que desea cambiar el estado?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.post("../ajax/expediente.php?op=desactivar", {idExpediente : idExpediente}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Registro se cambio a estado 'EN TRAMITE'!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						$("#cargandoModal").modal('hide');
						tabla.ajax.reload();
						

				});

				
		  }
	});
}

//funcion para activar
function activar(idExpediente){
		Swal.fire({
		  title: '¿Esta seguro que desea cambiar el estado?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.post("../ajax/expediente.php?op=activar", {idExpediente : idExpediente}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Registro se cambio a estado 'CERRADO'!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						$("#cargandoModal").modal('hide');
						tabla.ajax.reload();
						

				});

				
		  }
	});
}

init();