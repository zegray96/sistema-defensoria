var tabla;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})


	

	
}

//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//funcion limpiar
function limpiar(){
	$("#idAgente").val("");
	$("#nombre").val("");
	$("#estado").val("1");
	$('#estado').selectpicker('refresh');

}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnNuevo").hide();
		$("#btnGuardar").prop("disabled",false);
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();

	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}

//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[5, 100, 1000], [5, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/agente.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength":5, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
}




//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/agente.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡Agente Ya Existe!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
				


				


			}
			
		}
	});

}



//funcion mostrar
function mostrar(idAgente){
	$("#cargandoModal").modal('show');
	$.post("../ajax/agente.php?op=mostrar", {idAgente: idAgente}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idAgente").val(data.id_agente);
		$("#nombre").val(data.nombre);
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}
	


init();