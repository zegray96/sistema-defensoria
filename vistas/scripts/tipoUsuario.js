var tabla;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})


	

	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});



//funcion limpiar
function limpiar(){
	$("#idTipoUsuario").val("");
	$("#nombre").val("");
	$("#vistaConfiguracion").prop("checked",false);
	$("#vistaRecursosHumanos").prop("checked",false);
	$("#vistaExpedientes").prop("checked",false);
	$("#vistaTramitesRapidos").prop("checked",false);
	$("#vistaPlanchetas").prop("checked",false);
	$("#vistaReclamos").prop("checked",false);
	$("#vistaRefPreOrg").prop("checked",false);
	$("#vistaAdministracion").prop("checked",false);
	$("#vistaTurnos").prop("checked",false);

	$("#documentosModelo").prop("checked",false);
	$("#altConfiguracion").prop("checked",false);
	$("#altRecursosHumanos").prop("checked",false);
	$("#altExpedientes").prop("checked",false);
	$("#altTramitesRapidos").prop("checked",false);
	$("#altPlanchetas").prop("checked",false);
	$("#altReclamos").prop("checked",false);
	$("#altRefPreOrg").prop("checked",false);
	$("#altAdministracion").prop("checked",false);
	$("#altTurnos").prop("checked",false);

	$("#newConfiguracion").prop("checked",false);
	$("#newRecursosHumanos").prop("checked",false);
	$("#newExpedientes").prop("checked",false);
	$("#newTramitesRapidos").prop("checked",false);
	$("#newPlanchetas").prop("checked",false);
	$("#newReclamos").prop("checked",false);
	$("#newRefPreOrg").prop("checked",false);
	$("#newAdministracion").prop("checked",false);
	$("#newTurnos").prop("checked",false);

}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnNuevo").hide();
		$("#btnGuardar").prop("disabled",false);
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();

	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}


//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x

		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		], 
		"ajax":{
			url:'../ajax/tipoUsuario.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
}


//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/tipoUsuario.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');

			if(datos=="¡Nombre para Tipo de Usuario ya Existe!" || datos=="¡Debe seleccionar al menos una vista a la que tendra acceso!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
			}
			
		}
	});

}

//funcion mostrar
function mostrar(idTipoUsuario){
	$("#cargandoModal").modal('show');
	$.post("../ajax/tipoUsuario.php?op=mostrar", {idTipoUsuario: idTipoUsuario}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idTipoUsuario").val(data.id_tipo_usuario);
		$("#nombre").val(data.nombre);
		
		if(data.v_configuracion==1){
			$('#vistaConfiguracion').prop('checked', true);
		}else{
			$('#vistaConfiguracion').prop('checked', false);

		}

		if(data.v_recursos_humanos==1){
			$('#vistaRecursosHumanos').prop('checked', true);
		}else{
			$('#vistaRecursosHumanos').prop('checked', false);

		}

		if(data.v_expedientes==1){
			$('#vistaExpedientes').prop('checked', true);
		}else{
			$('#vistaExpedientes').prop('checked', false);
		}

		if(data.v_tramites_rapidos==1){
			$('#vistaTramitesRapidos').prop('checked', true);
		}else{
			$('#vistaTramitesRapidos').prop('checked', false);
		}


		if(data.v_planchetas==1){
			$('#vistaPlanchetas').prop('checked', true);
		}else{
			$('#vistaPlanchetas').prop('checked', false);
		}

		if(data.v_reclamos==1){
			$('#vistaReclamos').prop('checked',true);
		}else{
			$('#vistaReclamos').prop('checked',false);
		}

		if(data.v_ref_pre_org==1){
			$('#vistaRefPreOrg').prop('checked',true);
		}else{
			$('#vistaRefPreOrg').prop('checked',false);
		}

		if(data.v_administracion==1){
			$('#vistaAdministracion').prop('checked',true);
		}else{
			$('#vistaAdministracion').prop('checked',false);
		}

		if(data.v_turnos==1){
			$('#vistaTurnos').prop('checked',true);
		}else{
			$('#vistaTurnos').prop('checked',false);
		}

		if(data.documentos_modelo==1){
			$('#documentosModelo').prop('checked', true);
		}else{
			$('#documentosModelo').prop('checked', false);
		}

		if(data.alt_configuracion==1){
			$('#altConfiguracion').prop('checked', true);
		}else{
			$('#altConfiguracion').prop('checked', false);
		}

		if(data.alt_recursos_humanos==1){
			$('#altRecursosHumanos').prop('checked', true);
		}else{
			$('#altRecursosHumanos').prop('checked', false);
		}

		if(data.alt_expedientes==1){
			$('#altExpedientes').prop('checked', true);
		}else{
			$('#altExpedientes').prop('checked', false);
		}

		if(data.alt_tramites_rapidos==1){
			$('#altTramitesRapidos').prop('checked', true);
		}else{
			$('#altTramitesRapidos').prop('checked', false);
		}


		if(data.alt_planchetas==1){
			$('#altPlanchetas').prop('checked', true);
		}else{
			$('#altPlanchetas').prop('checked', false);
		}

		if(data.alt_reclamos==1){
			$('#altReclamos').prop('checked',true);
		}else{
			$('#altReclamos').prop('checked',false);
		}

		if(data.alt_ref_pre_org==1){
			$('#altRefPreOrg').prop('checked',true);
		}else{
			$('#altRefPreOrg').prop('checked',false);
		}

		if(data.alt_administracion==1){
			$('#altAdministracion').prop('checked',true);
		}else{
			$('#altAdministracion').prop('checked',false);
		}

		if(data.alt_turnos==1){
			$('#altTurnos').prop('checked',true);
		}else{
			$('#altTurnos').prop('checked',false);
		}

		if(data.new_configuracion==1){
			$('#newConfiguracion').prop('checked',true);
		}else{
			$('#newConfiguracion').prop('checked',false);
		}

		if(data.new_recursos_humanos==1){
			$('#newRecursosHumanos').prop('checked',true);
		}else{
			$('#newRecursosHumanos').prop('checked',false);
		}

		if(data.new_expedientes==1){
			$('#newExpedientes').prop('checked',true);
		}else{
			$('#newExpedientes').prop('checked',false);
		}

		if(data.new_tramites_rapidos==1){
			$('#newTramitesRapidos').prop('checked',true);
		}else{
			$('#newTramitesRapidos').prop('checked',false);
		}

		if(data.new_planchetas==1){
			$('#newPlanchetas').prop('checked',true);
		}else{
			$('#newPlanchetas').prop('checked',false);
		}

		if(data.new_reclamos==1){
			$('#newReclamos').prop('checked',true);
		}else{
			$('#newReclamos').prop('checked',false);
		}

		if(data.new_ref_pre_org==1){
			$('#newRefPreOrg').prop('checked',true);
		}else{
			$('#newRefPreOrg').prop('checked',false);
		}

		if(data.new_administracion==1){
			$('#newAdministracion').prop('checked',true);
		}else{
			$('#newAdministracion').prop('checked',false);
		}

		if(data.new_turnos==1){
			$('#newTurnos').prop('checked',true);
		}else{
			$('#newTurnos').prop('checked',false);
		}


      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}

//funcion para eliminar
	function eliminar(idTipoUsuario){
		Swal.fire({
		  title: '¿Esta seguro que desea eliminar el registro?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.post("../ajax/tipoUsuario.php?op=eliminar", {idTipoUsuario : idTipoUsuario}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Tipo Usuario esta asociado a 1 o más Usuarios!"){
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#cargandoModal").modal('hide');

				});
		  }
		});

	}
	

	


init();