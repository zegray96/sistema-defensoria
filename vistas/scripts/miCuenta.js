

//Funcion que se ejecuta al inicio
function init(){

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})

	$("#formularioModificarClave").on("submit", function(e){ //si en el form se activa el submit se hara esto
		modificarClave(e);
	})


	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});



function limpiarNuevaClave(){
	$("#nuevaClave").val("");
	$("#modalModificarClave").modal('hide');
}



//funcion para modificar clave
function modificarClave(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($("#formularioModificarClave")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/usuario.php?op=modificarClaveMiCuenta',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			
			if(datos=="¡Clave modificada con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
				});
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}
			limpiarNuevaClave();
			
		}
	});

}


//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/usuario.php?op=editarMiCuenta',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡Registro editado con exito!"){
				Swal.fire({
				  title: 'La sesion se cerrará para actualizar sus datos...',
				  icon: 'warning',
				  confirmButtonColor: '#3085d6',
				  showConfirmButton:true,
				  confirmButtonText: 'Aceptar',
				  allowOutsideClick: false,
				}).then((result) => {
				  if (result.value) {
				   		salir();

						
				  }
				});
				
			}else{

				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}
			
		}
	});

}

function salir(){
	$("#cargandoModal").modal('show');
	$.get('../ajax/sesion.php?op=salir', function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#cargandoModal").modal('hide');
		//redirigimos a index.php
		setTimeout("location.href='../index.php'");
					
		
	});	

}



init();