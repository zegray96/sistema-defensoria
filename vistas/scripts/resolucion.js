var tabla;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listarPorAnio();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});

	
	cargarSelectAgentes();
	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});


// select agente
function cargarSelectAgentes(){
	//cargamos los items al select
	$.post("../ajax/decreto.php?op=selectAgente", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idAgente").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idAgente').selectpicker('refresh'); //refresco el select

	});	
}

// listar por anio
$("#listarPorAnio").change(function (){
	listarPorAnio();
});


//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//funcion limpiar
function limpiar(){
	$("#idResolucion").val("");
	$("#nroResolucion").val("");
	$("#anio").val("");
	$('#anio').selectpicker('refresh');
	$("#fecha").val("");
	$("#detalle").val("");
	$("#area").val("");
	$("#area").selectpicker("refresh");
	$("#estado").val("");
	$('#estado').selectpicker('refresh');
	$("#idAgente").val("");
	$("#contDetalle").text("0");

}

//funcion para mostrar formulario
function mostrarFormNuevo(mostrar){
	$("#anio").prop('disabled', false);
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnNuevo").hide();
		$("#btnBuscar").hide();
		$("#divSelectListar").hide();
		$("#divNro").hide();

		

	}else{
		$("#listadoRegistros").show();
		$("#divSelectListar").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		$("#btnBuscar").show();
		cargarSelectAgentes();
	}	
}

function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnNuevo").hide();
		$("#btnBuscar").hide();	
		$("#divSelectListar").hide();
		$("#divNro").hide();	
			
	}else{
		$("#divSelectListar").show();
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		$("#btnBuscar").show();
		cargarSelectAgentes();
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}


//validarNumericos
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}


//limpiar nro
function limpiarNro(){
	$("#nroResolucionBuscar").val("");
}

//funcion para listar por fecha
function buscarPorNro(){
	
	if($("#nroResolucionBuscar").val()==""){
		Swal.fire({
			icon: 'error',
			title: "¡Ingrese n° de resolución!",
		}); //si no ingreso ninguna fecha me mostrara este msj, de lo contrario listara los reclamos
	}else{
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},

				

			],  
			"ajax":{
				url:'../ajax/resolucion.php?op=buscarPorNro',
				type: 'get',
				data: {'varNroResolucion': $("#nroResolucionBuscar").val()},
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},


			"bDestroy": true,
			"order":[[1,"desc"]] //para ordenar los registros
		}).DataTable();
		$("#nroResolucionBuscar").val("");
		$('#modalBuscarPorNro').modal('hide');

	}

	
}


//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/resolucion.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"desc"]] //para ordenar los registros
	}).DataTable();
}





//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/resolucion.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡N° de resolucion ya existe!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
			}
			
			
		}
	});

}



//funcion mostrar
function mostrar(idResolucion){
	$("#cargandoModal").modal('show');
	$.post("../ajax/resolucion.php?op=mostrar", {idResolucion: idResolucion}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#anio").prop('disabled', true);
		$("#cargandoModal").modal('hide');
		$("#idResolucion").val(data.id_resolucion);
		$("#nroResolucion").val(data.nro_resolucion);
		$("#anio").val(data.anio);
		$('#anio').selectpicker('refresh');
		$('#divNro').show();
		$("#fecha").val(data.fecha);
		$("#detalle").val(data.detalle);
		$("#estado").val(data.estado);
		$('#estado').selectpicker('refresh');
		$("#area").val(data.area);
		$("#area").selectpicker("refresh");
		$("#idAgente").val(data.id_agente);
		$('#idAgente').selectpicker('refresh');
		contadorCaracteres('#detalle','#contDetalle');
		
		
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}


//cambio en la fecha
$("#anio").change(function(){
	$.ajax({
			url:'../ajax/resolucion.php?op=ultimoNro',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'anio': $("#anio").val()}, //estos son los datos que envio
			


			success: function(data){ //si la accion se hace de forma correcta se hace esto
				data = JSON.parse(data);
				if(data===null){
					var nro_resolucion=1;
					$("#nroResolucion").val(nro_resolucion);
				}else{
					$("#nroResolucion").val(data.nro_resolucion);
				}

				$("#divNro").show();
				$("#nroResolucion").focus();			 	
			}
	});

});


//funcion para listar registros por anio
function listarPorAnio(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,3,4,5,6,7]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/resolucion.php?op=listarPorAnio',
			data: {'varAnio': $("#listarPorAnio").val()},
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"order":[[1,"desc"]] //para ordenar los registros
	}).DataTable();
}







init();