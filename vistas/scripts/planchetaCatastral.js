var tabla;
var alteracion;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});
	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//validar plancheta
function validarPlancheta(){
        var archivo = $("#plancheta").val();
		var extension = archivo.substring(archivo.lastIndexOf("."));
		if(extension != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extension + " no es válido",
			});
		    $("#plancheta").val(null);
		}
}

//borrar pdf
function borrarArchivo(valor){

	Swal.fire({
		  title: '¿Esta seguro que desea eliminar el archivo?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.ajax({
						url:'../ajax/planchetaCatastral.php?op=borrarArchivo',//lugar a donde se envia los datos obtenidos del formulario
						type: "get",
						data: {'idPlan': $("#idPlancheta").val(), 'ref': $("#referencia").val()}, //estos son los datos que envio
						

						success: function(datos){ //si la accion se hace de forma correcta se hace esto
							$("#cargandoModal").modal('hide');			
								if(datos=="¡Archivo eliminado con exito!"){
									Swal.fire({
										icon: 'success',
										title: datos,
									});
		
									$("#borrarPlancheta").hide();
									$("#enlacePlancheta").hide();
									$("#planchetaActual").val("");
									
								}else{

									Swal.fire({
										icon: 'error',
										title: datos,
									});
	

								}
							
						}
					});

				
		  }
	});
}





//funcion limpiar
function limpiar(){
	$("#idPlancheta").val("");
	$("#referencia").val("");
	$("#planchetaActual").val("");	
	$("#plancheta").val("");
}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnNuevo").hide();

	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		$("#enlacePlancheta").hide();		
		$("#borrarPlancheta").hide();
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}


//funcion para listar
function listar(){
	//traemos con ajax la variable de sesion de alteracion
	$.get("../ajax/planchetaCatastral.php?op=traerAlteracion", function(data, status){
		alteracion=data; //variable para comprobar la alteracion
	});

	tabla=$("#tblListado").dataTable({
		"processing": true, //Activamos el procesamiento de datatables
		"serverSide": true, //Paginacion y filtrado relizados por el servidor
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
		"lengthMenu": [[5, 100, 1000], [5, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla

		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},


		],  

		"ajax": "../serverSide/server-side-planchetas.php",

		//datos de la tabla
		columns: [
			{"data": "id_plancheta"}, //va el nombre del dt de server side personas
			{"data": null, //columna opciones
			      "render": function(data, type, full) {
			      	if(alteracion==1){ //comprobacion de si puede alterar o no, para mostrar el boton
			      		data="<button class='btn btn-warning' onclick=mostrar("+full["id_plancheta"]+")><i class='fas fa-pencil-alt'></i></button>";
			      	}else{
			      		data=null;
			      	}
			      	return data;
			      }
			},      
			{"data": "referencia"},
			{"data": "plancheta",
				"render": function ( data, type, row ) {
                                    return '<a href="../files/planchetas/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},

			},
			{"data": "ultimaModificacion"},
			
			
		],

		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

        "bDestroy": true,
		"order":[[2,"desc"]], //para ordenar los registros


		
	}).DataTable();
}





//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	$ref=$("#referencia").val();
	if ($ref.indexOf("/")>=0 || $ref.indexOf("?")>=0 || $ref.indexOf(":")>=0 || $ref.indexOf("*")>=0) {
		Swal.fire({
				icon: 'error',
				title: "¡Referencia contiene signos invalidos!",
		});
	$("#btnGuardar").prop("disabled",false);
	}else{
		var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
		$("#cargandoModal").modal('show');
		$.ajax({
			url:'../ajax/planchetaCatastral.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
			type: "POST",
			data: formData, //estos son los datos que envio
			contentType:false,
			processData:false,

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
				$("#cargandoModal").modal('hide');

				if(datos=="¡Referencia ya Existe!"){
					Swal.fire({
						icon: 'error',
						title: datos,
					});
					$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
					$("#referencia").focus();
				}else{
					if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
						Swal.fire({
							icon: 'success',
							title: datos,
						});
					}else{
						Swal.fire({
							icon: 'error',
							title: datos,
						});
					}
					mostrarForm(false);
					tabla.ajax.reload(); 
					limpiar();
				}
				
			}
		});
	}
	
	
	

}

//funcion mostrar
function mostrar(idPlancheta){
	$("#cargandoModal").modal('show');
	$.post("../ajax/planchetaCatastral.php?op=mostrar", {idPlancheta: idPlancheta}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idPlancheta").val(data.id_plancheta);
		$("#referencia").val(data.referencia);
		$("#referenciaOriginal").val(data.referencia);
		$("#planchetaActual").val(data.plancheta);
		
		
		if(!data.plancheta==""){
			$("#enlacePlancheta").show();
			$("#enlacePlancheta").attr('href', '../files/planchetas/'+data.plancheta);
			$("#borrarPlancheta").show();
			//document.getElementById("enlaceInforme").href ="../files/informes/"+data.informe;
		}

		


		
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}




init();