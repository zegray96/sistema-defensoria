$("#frmAcceso").on('submit',function(e){
	$("#cargandoModal").modal('show');
	e.preventDefault(); //cuando toca submit no va hacer lo por defualt, sino esto
	usuarioAcceso=$("#usuarioAcceso").val();
	claveAcceso=$("#claveAcceso").val();
	$.post("../ajax/sesion.php?op=verificar",
		{"usuarioAcceso":usuarioAcceso,"claveAcceso":claveAcceso},
		function(datos){
			$("#cargandoModal").modal('hide');
			
			if(datos!="null"){
				$(location).attr("href","escritorio");
			}else{
				Swal.fire({
						icon: 'error',
						title: '¡Usuario o Clave incorrectos!',
				});
			}

	});
})





//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
    if(e.keyCode == 13) {
      e.preventDefault();
    }
  }))
});

//funcion para mostrar y ocultar clave
function mostrarClave(){
    var tipo = document.getElementById("claveAcceso");
    if(tipo.type == "password"){
        tipo.type = "text";
        $("#iconoVer").removeClass("fas fa-eye");
        $("#iconoVer").addClass("fas fa-eye-slash");
    }else{
        tipo.type = "password";
        $("#iconoVer").removeClass("fas fa-eye-slash");
        $("#iconoVer").addClass("fas fa-eye");
    }
}

	 
