var tabla;
var alteracion;
var anioActual;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);

	listarPorAnio();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});

	cargarSelectAgentes();
	

	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});


//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

//validar informe 
function validarInf(){
        var archivo = $("#informe").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".pdf"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#informe").val(null);
		}
}



//borrar pdf
function borrarArchivo(valor){
	var nombreArchivo;
	Swal.fire({
		  title: '¿Esta seguro que desea eliminar el archivo?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');				
				$.ajax({
						url:'../ajax/tramiteRapido.php?op=borrarArchivo',//lugar a donde se envia los datos obtenidos del formulario
						type: "get",
						data: {'valor': valor, 'idTra': $("#idTramite").val()}, //estos son los datos que envio
						

						success: function(datos){ //si la accion se hace de forma correcta se hace esto
							$("#cargandoModal").modal('hide');			
								if(datos=="¡Archivo eliminado con exito!"){

									Swal.fire({
										icon: 'success',
										title: datos,
									});
									if(valor=='informe'){
										$("#borrarInforme").hide();
										$("#enlaceInforme").hide();
										$("#informeActual").val("");
									}	
								}else{

									Swal.fire({
										icon: 'error',
										title: datos,
									});

								}
							
						}
					});

				
		  }
	});
}



//validarNumericos
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}



//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//funcion limpiar
function limpiar(){
	$("#idTramite").val("");
	$("#fechaInicio").val("");
	$("#fechaFinalizacion").val("");
	$("#nroTramite").val("");
	$("#anio").val("");
	$("#anio").selectpicker("refresh");
	$("#requirente").val("");
	$("#dni").val("");
	$("#telefono").val("");
	$("#domicilio").val("");
	$("#reclamo").val("");
	$("#idAgente").val("");
	$("#estado").val("");
	$('#estado').selectpicker('refresh');
	$("#informeActual").val("");
	$("#informe").val("");
	$("#contReclamo").text("0");

}

//funcion para mostrar formulario
function mostrarFormNuevo(mostrar){

	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#divSelectListar").hide();
		$("#btnNuevo").hide();
		$("#btnBuscarNro").hide();
		$("#btnBuscarFecha").hide();
		$("#divNro").hide();
		
			
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#divSelectListar").show();
		$("#btnNuevo").show();
		$("#btnBuscarNro").show();
		$("#btnBuscarFecha").show();
		$("#enlaceInforme").hide();
		$("#borrarInforme").hide();
		cargarSelectAgentes();
	}
}

function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#divSelectListar").hide();
		$("#btnNuevo").hide();
		$("#btnBuscarNro").hide();
		$("#btnBuscarFecha").hide();
		$("#divNro").hide();
		
			
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#divSelectListar").show();
		$("#btnNuevo").show();
		$("#btnBuscarNro").show();
		$("#btnBuscarFecha").show();
		$("#enlaceInforme").hide();
		$("#borrarInforme").hide();
		cargarSelectAgentes();
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}

//limpiar nro
function limpiarNro(){
	$("#nroTramiteBuscar").val("");
}



//funcion para listar por fecha
function buscarPorNro(){
	
	if($("#nroTramiteBuscar").val()==""){
		Swal.fire({
			icon: 'error',
			title: "¡Ingrese un n° de tramite!",
		}); //si no ingreso ninguna fecha me mostrara este msj, de lo contrario listara los reclamos
	}else{
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,7,9,11,12]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

			],   
			"ajax":{
				url:'../ajax/tramiteRapido.php?op=buscarPorNro',
				type: 'get',
				data: {'varNroTramite': $("#nroTramiteBuscar").val()},
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[5,"desc"]] //para ordenar los registros
		}).DataTable();
		$("#nroTramiteBuscar").val("");
		$('#modalBuscarPorNro').modal('hide');

	}

	
}

//limpiar fecha
function limpiarFecha(){
	$("#fechaIniBuscar").val("");
	$("#fechaFinBuscar").val("");
}

//buscar por fecha
function buscarPorFecha(){

	if($("#fechaIniBuscar").val()=="" || $("#fechaFinBuscar").val()==""){
		Swal.fire({
			icon: 'error',
			title: "¡Seleccione fecha inicio y fin!",
		}); //si no ingreso ninguna fecha me mostrara este msj, de lo contrario listara los reclamos
	}else{
		if($("#fechaIniBuscar").val() > $("#fechaFinBuscar").val()){
			Swal.fire({
				icon: 'error',
				title: "¡Fecha inicio no puede ser mayor a fecha fin!",
			}); 

		}else{
			tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,7,9,11,12]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

			],  
			"ajax":{
				url:'../ajax/tramiteRapido.php?op=buscarPorFecha',
				type: 'get',
				data: {'varFechaIniBuscar': $("#fechaIniBuscar").val(), 'varFechaFinBuscar': $("#fechaFinBuscar").val()},
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[5,"desc"]] //para ordenar los registros
		}).DataTable();
		$('#modalBuscarPorFecha').modal('hide');
		$("#fechaIniBuscar").val("");
		$("#fechaFinBuscar").val("");
		}
		
		

	}

}

//funcion para listar
function listar(){
	//traemos con ajax la variable de sesion de alteracion
	$.get("../ajax/tramiteRapido.php?op=traerAlteracion", function(data, status){
		alteracion=data; //variable para comprobar la alteracion
	});

	tabla=$("#tblListado").dataTable({
		"processing": true, //Activamos el procesamiento de datatables
		"serverSide": true, //Paginacion y filtrado relizados por el servidor
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,7,9,11,12]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],     
		"ajax": "../serverSide/server-side-tramites-rapidos.php",

		//datos de la tabla
		columns: [
			{"data": "id_tramite_rapido"},
			{"data": null, //columna opciones
			      "render": function(data, type, full) {
			      	if(alteracion==1){ //comprobacion de si puede alterar o no, para mostrar el boton
			      		data="<button class='btn btn-warning' onclick=mostrar("+full["id_tramite_rapido"]+")><i class='fas fa-pencil-alt'></i></button>";
			      	}else{
			      		data=null;
			      	}
			      	return data;
			      }
			},      
			{"data": "fecha_inicio"},
			{"data": "fecha_finalizacion",
				"render": function ( data, type, row ) {
                                    if(data!="-0001-11-30"){
										data=data;
									}else{
										data="";
									}	
                                  	return data;
				},

			},
			{"data": "estado",
				"render": function ( data, type, row ) {
                            if(data=="EN TRAMITE"){
								data='<span class="label bg-red">'+data+'</span>';
							}else{
								if(data=="CERRADO-NOTIFICADO"){
									data='<span class="label bg-green">'+data+'</span>';
								}else{
									if(data=="CERRADO-SIN NOTIFICAR"){
										data='<span class="label bg-orange">'+data+'</span>'
									}else{
										if(data=="PASO A EXPEDIENTE"){
											data='<span class="label bg-blue">'+data+'</span>';
										}
									}
								}
							}
							return data;
				},

			},
			{"data": "nro_tramite"},
			{"data": "anio"},
			{"data": "requirente"},
			{"data": "dni"},
			{"data": "telefono"},
			{"data": "domicilio"},
			{"data": "reclamo"},
			{"data": "agente"},
			{"data": "informe",
				"render": function ( data, type, row ) {
                                    return '<a href="../files/informesTramitesRapidos/'+data+"?"+Math.random()+'" target="_blank">'+data;
				},

			},		
			{"data": "ultimaModificacion"},
			
			
		],

		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

		"bDestroy": true,
		"order":[[5,"desc"]], //para ordenar los registros

	}).DataTable();
}





//funcion para guardar o editar
function guardarOeditar(e){
	
		e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
		if($("#estado").val()==0  && $("#fechaFinalizacion").val()!=""){
			Swal.fire({
					icon: 'error',
					title: "¡Debe colocar estado 'CERRADO' si desea ingresar fecha de finalizacion!",
			});
		}else{
			if($("#estado").val()==2  && $("#fechaFinalizacion").val()!=""){
				Swal.fire({
						icon: 'error',
						title: "¡Debe colocar estado 'CERRADO' si desea ingresar fecha de finalizacion!",
				});
			}else{

				if($("#estado").val()==1 && $("#fechaFinalizacion").val()==""){
					Swal.fire({
							icon: 'error',
							title: "¡Debe ingresar fecha de finalizacion si desea colocar estado 'CERRADO'!",
					});
				}else{
					$("#btnGuardar").prop("disabled",true);
					var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
					$("#cargandoModal").modal('show');
					$.ajax({
						url:'../ajax/tramiteRapido.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
						type: "POST",
						data: formData, //estos son los datos que envio
						contentType:false,
						processData:false,

						success: function(datos){ //si la accion se hace de forma correcta se hace esto
							$("#cargandoModal").modal('hide');

								if(datos=="¡N° de Tramite ya Existe!"){
									Swal.fire({
										icon: 'error',
										title: datos,
									});
									$("#btnGuardar").prop("disabled",false); //vuelvo a activar el boton
								}else{
									if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
										Swal.fire({
											icon: 'success',
											title: datos,
										});
									}else{
										Swal.fire({
											icon: 'error',
											title: datos,
										});
									}
									mostrarForm(false);
									tabla.ajax.reload(); 
									limpiar();
								}
							
							
						}
					});
				}
			}		
		}
}	



//funcion mostrar
function mostrar(idTramite){
	$("#cargandoModal").modal('show');
	$.post("../ajax/tramiteRapido.php?op=mostrar", {idTramite: idTramite}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idTramite").val(data.id_tramite_rapido);
		$("#nroTramite").val(data.nro_tramite);
		$("#anio").val(data.anio);
		$('#anio').selectpicker('refresh');
		$("#fechaInicio").val(data.fecha_inicio);
		$("#fechaFinalizacion").val(data.fecha_finalizacion);
		$("#requirente").val(data.requirente);
		$("#dni").val(data.dni);
		$("#telefono").val(data.telefono);
		$("#domicilio").val(data.domicilio);
		$("#reclamo").val(data.reclamo);
		$("#idAgente").val(data.id_agente);
		$('#idAgente').selectpicker('refresh');
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');
		$("#informeActual").val(data.informe);
		contadorCaracteres('#reclamo','#contReclamo');

		if(!data.informe==""){
			$("#enlaceInforme").show();
			$("#enlaceInforme").attr('href', '../files/informesTramitesRapidos/'+data.informe+'?'+Math.random());
			$("#borrarInforme").show();
		}
		


		
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}


function cargarSelectAgentes(){
	//cargamos los items al select
	$.post("../ajax/tramiteRapido.php?op=selectAgente", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idAgente").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idAgente').selectpicker('refresh'); //refresco el select

	});	
}


//cambio en la fecha
$("#anio").change(function(){
	$.ajax({
			url:'../ajax/tramiteRapido.php?op=ultimoNro',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'anio': $("#anio").val()}, //estos son los datos que envio
			


			success: function(data){ //si la accion se hace de forma correcta se hace esto
				data = JSON.parse(data);
				if(data===null){
					var nro_tramite=1;
					$("#nroTramite").val(nro_tramite);
				}else{
					$("#nroTramite").val(data.nro_tramite);
				}

				$("#divNro").show();
				$("#nroTramite").focus();			 	
			}
	});

});

// listar por anio
$("#listarPorAnio").change(function (){
	listarPorAnio();
});

//funcion para listar registros por anio
function listarPorAnio(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,7,9,11,12]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/tramiteRapido.php?op=listarPorAnio',
			data: {'varAnio': $("#listarPorAnio").val()},
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

		"bDestroy": true,
		"order":[[5,"desc"]] //para ordenar los registros
	}).DataTable();
}










init();