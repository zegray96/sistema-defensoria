var tabla;
var alteracion;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);

	
	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditarSistema(e)
	});
	
	

	listarHoy();
	$("#fechaIniListar").val("");
	$("#fechaFinListar").val("");

	

	
}


// imprimir formulario
function imprimirFormulario(idReclamo){
	var url = '../vistas/formulario-reclamo.php?id='+idReclamo;
	window.open(url, '_blank');
}



$('#listar').change(function () {
  if ($(this).val() === 'todos') {
    listarTodos();
  }else{
  	if($(this).val() === 'porFechas'){
  		$('#modalSeleccionarFecha').modal('show');
  	}else{
  		if($(this).val() === 'hoy'){
  			listarHoy();
  		}else{
  			if($(this).val() === 'enEsperaDeRespuesta'){
  				listarEnEspera();
  			}
  		}
  	}
  }
});

//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});



//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//funcion limpiar
function limpiar(){
	$("#idReclamo").val("");
	$("#fecha").val("");
	$("#apellidoNombre").val("");
	$("#dni").val("");
	$("#domicilio").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#reclamo").val("");
	$("#respuesta").val("");
	$("#tramite").val("");
	$("#via").val("");
	$('#via').selectpicker('refresh');
	$("#contReclamo").text("0");
	$("#contRespuesta").text("0");
	$("#contTramite").text("0");

}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#cabecera").hide();
		$("#btnGuardar").prop("disabled",false);
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#cabecera").show();
		$("#fecha").prop('disabled', false);
		$("#via").prop('disabled', false);
		$("#apellidoNombre").prop('disabled', false);
		$("#dni").prop('disabled', false);
		$("#domicilio").prop('disabled', false);
		$("#telefono").prop('disabled', false);
		$("#email").prop('disabled', false);
		$("#reclamo").prop('disabled', false);
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}

//limpiar fecha
function limpiarFecha(){
	$("#fechaIniListar").val("");
	$("#fechaFinListar").val("");

}


//funcion para ver la observacion en la tabla
function verMasReclamo(idReclamo){

	$("#rec"+idReclamo).css("white-space","normal");
	$("#ocuRec"+idReclamo).css("display","inline");
	$("#verRec"+idReclamo).css("display","none");
}


//funcion para ocultar la observacion en la tabla
function ocultarTextReclamo(idReclamo){
	$("#rec"+idReclamo).css("white-space","nowrap");
	$("#ocuRec"+idReclamo).css("display","none");
	$("#verRec"+idReclamo).css("display","inline");
}

//funcion para ver la respuesta en la tabla
function verMasRespuesta(idReclamo){

	$("#res"+idReclamo).css("white-space","normal");
	$("#ocuRes"+idReclamo).css("display","inline");
	$("#verRes"+idReclamo).css("display","none");
}


//funcion para ocultar la respuesta en la tabla
function ocultarTextRespuesta(idReclamo){
	$("#res"+idReclamo).css("white-space","nowrap");
	$("#ocuRes"+idReclamo).css("display","none");
	$("#verRes"+idReclamo).css("display","inline");
}

//funcion para ver el tramite en la tabla
function verMasTramite(idReclamo){

	$("#tra"+idReclamo).css("white-space","normal");
	$("#ocuTra"+idReclamo).css("display","inline");
	$("#verTra"+idReclamo).css("display","none");
}


//funcion para ocultar el tramite en la tabla
function ocultarTextTramite(idReclamo){
	$("#tra"+idReclamo).css("white-space","nowrap");
	$("#ocuTra"+idReclamo).css("display","none");
	$("#verTra"+idReclamo).css("display","inline");
}






//funcion para listar
function listarTodos(){
	//traemos con ajax la variable de sesion de alteracion
	$.get("../ajax/reclamo.php?op=traerAlteracion", function(data, status){
		alteracion=data; //variable para comprobar la alteracion
	});

	tabla=$("#tblListado").dataTable({
		"processing": true, //Activamos el procesamiento de datatables
		"serverSide": true, //Paginacion y filtrado relizados por el servidor
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla

		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},

			{
				extend: 'print',
				exportOptions: {
					columns: [2,3,4,5,6,7,8,10]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax": "../serverSide/server-side-reclamos.php",
		

		//datos de la tabla
		columns: [
			{"data": "id_reclamo"}, //va el nombre del dt de server side personas
			{"data": null, //columna opciones
			      "render": function(data, type, full) {
			      	if(alteracion==1){ //comprobacion de si puede alterar o no, para mostrar el boton
			      		data=' <button class="btn btn-warning" onclick="mostrar('+full["id_reclamo"]+')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimirFormulario('+full["id_reclamo"]+')"><i class="fas fa-print"></i></button>';
			      	}else{
			      		data=null;
			      	}
			      	return data;
			      }
			},      
			{"data": "nro_reclamo"},	
			{"data": "estado",
			"render": function ( data, type, row ) {
                                    if(data=="EN ESPERA DE RESPUESTA"){
										data='<span class="label bg-red">'+data+'</span>';
									}else{
										if(data=="RESPONDIDO"){
											data='<span class="label bg-green">'+data+'</span>';
										}
									}	
                                  	return data;
						},

			},
			{"data": "via",
			"render": function ( data, type, row ) {
                                    if(data=="FORMULARIO WEB"){
										data='<span style="color:#40b8ab; font-weight:bold">'+data+'</span>';
									}else{
										if(data=="WHATSAPP"){
											data='<span style="color:#0e900e; font-weight:bold">'+data+'</span>';
										}else{
											if(data=="TELEFONO"){
												data='<span style="color:#ce7a0e; font-weight:bold">'+data+'</span>';
											}else{
												if(data=="EMAIL"){
													data='<span style="color:#00458e; font-weight:bold">'+data+'</span>';
												}else{
													if(data=="VISITA"){
														data='<span style="color:#a00094; font-weight:bold">'+data+'</span>';
													}
												}
											}
										}
									}	
                                  	return data;
						},

			},
			{"data": "fecha"},
			{"data": "apellido_nombre"},
			{"data": "dni"},
			{"data": "reclamo",
				"render": function(data, type, full) {
					if(data!=""){
						data= "<p id='rec"+full["id_reclamo"]+"' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>"+data+"</p> <a style='cursor:pointer;' id='verRec"+full["id_reclamo"]+"' onclick='verMasReclamo("+full["id_reclamo"]+")'>(Ver mas)</a> <a id='ocuRec"+full["id_reclamo"]+"' onclick='ocultarTextReclamo("+full["id_reclamo"]+")' style='display:none; cursor:pointer;' >(Ocultar)</a>";
					}else{
						data="";
					}
					return data;
			      }

			},
			{"data": "respuesta",
				"render": function(data, type, full) {
					if(data!=""){
						data= "<p id='res"+full["id_reclamo"]+"' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>"+data+"</p> <a style='cursor:pointer;' id='verRes"+full["id_reclamo"]+"' onclick='verMasRespuesta("+full["id_reclamo"]+")'>(Ver mas)</a> <a id='ocuRes"+full["id_reclamo"]+"' onclick='ocultarTextRespuesta("+full["id_reclamo"]+")' style='display:none; cursor:pointer;' >(Ocultar)</a>";
					}else{
						data="";
					}
					return data;
			      }

			},

			{"data": "tramite",
				"render": function(data, type, full) {
					if(data!=""){
						data= "<p id='tra"+full["id_reclamo"]+"' style='width: 500px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;'>"+data+"</p>"+
						"<a style='cursor:pointer;' id='verTra"+full["id_reclamo"]+"' onclick='verMasTramite("+full["id_reclamo"]+")'>(Ver mas)</a>"+
						" <a id='ocuTra"+full["id_reclamo"]+"' onclick='ocultarTextTramite("+full["id_reclamo"]+")' style='display:none; cursor:pointer;' >(Ocultar)</a>";
					}else{
						data="";
					}
					return data;
			      }

			},
			
			{"data": "domicilio"},
			{"data": "telefono"},
			{"data": "email"},	
			{"data": "ultimaModificacion"},		
			
			
		],


		//columna oculta 
		"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            
        ],

        "bDestroy": true,
		"order":[[0,"desc"]], //para ordenar los registros
	}).DataTable();
	$("#mostrando").text("Mostrando Todos");
}




//funcion para listar por fecha
function listarPorFecha(){
	
	if($("#fechaIniListar").val()=="" || $("#fechaFinListar").val()==""){
		Swal.fire({
			icon: 'error',
			title: "¡Seleccione fecha inicio y fin!",
		}); //si no ingreso ninguna fecha me mostrara este msj, de lo contrario listara los reclamos
	}else{
		if($("#fechaIniListar").val() > $("#fechaFinListar").val()){
			Swal.fire({
				icon: 'error',
				title: "¡Fecha inicio no puede ser mayor a fecha fin!",
			}); 

		}else{
			tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},

				{
					extend: 'print',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},

				

			],  
			"ajax":{
				url:'../ajax/reclamo.php?op=listarPorFecha',
				type: 'get',
				data: {'varFechaIniListar': $("#fechaIniListar").val(), 'varFechaFinListar': $("#fechaFinListar").val()},
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[0,"desc"]] //para ordenar los registros
		}).DataTable();
		$('#modalSeleccionarFecha').modal('hide');
		var fechaIni=$("#fechaIniListar").val();
		var fechaFin=$("#fechaFinListar").val();
		$("#mostrando").text("Desde "+fechaIni+" Hasta "+fechaFin);
		$("#fechaIniListar").val("");
		$("#fechaFinListar").val("");
		}
		
		

	}

	
}

//funcion para listar hoy
function listarHoy(){
	
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},

				{
					extend: 'print',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},

			],  
			"ajax":{
				url:'../ajax/reclamo.php?op=listarHoy',
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[0,"desc"]] //para ordenar los registros
		}).DataTable();
		
		var horaActual;
		$.get("../ajax/reclamo.php?op=horaActual", function(e){
			$("#mostrando").text("Mostrando Hoy (Ultima Revision "+e+")");
		});
		
		

	

	
}

//funcion para listar en espera
function listarEnEspera(){
	
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},

				{
					extend: 'print',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},
				

			],  
			"ajax":{
				url:'../ajax/reclamo.php?op=listarEnEspera',
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[0,"desc"]] //para ordenar los registros
		}).DataTable();
		$("#mostrando").text("Mostrando En Espera de Respuesta");
		
		
		

	

	
}

//limpiar nro
function limpiarNro(){
	$("#nroTramiteBuscar").val("");
}


//funcion para listar por fecha
function buscarPorNro(){
	
	if($("#nroReclamoBuscar").val()==""){
		Swal.fire({
			icon: 'error',
			title: "¡Ingrese N° de reclamo!",
		}); //si no ingreso ninguna fecha me mostrara este msj, de lo contrario listara los reclamos
	}else{
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,9,10,11,12,13,14]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},

				{
					extend: 'print',
					exportOptions: {
						columns: [2,3,4,5,6,7,8,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},

				

			],  
			"ajax":{
				url:'../ajax/reclamo.php?op=buscarPorNro',
				type: 'get',
				data: {'varNroReclamo': $("#nroReclamoBuscar").val()},
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			//columna oculta 
			"columnDefs": [
	            {
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
	            },
	            
	        ],

			"bDestroy": true,
			"order":[[0,"desc"]] //para ordenar los registros
		}).DataTable();
		$("#nroReclamoBuscar").val("");
		$('#modalBuscarPorNro').modal('hide');

	}

	
}


//funcion mostrar
function mostrar(idReclamo){
	$("#cargandoModal").modal('show');
	$.post("../ajax/reclamo.php?op=mostrar", {idReclamo: idReclamo}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idReclamo").val(data.id_reclamo);
		$("#viaOpcion").val(data.via);
		$("#fecha").val(data.fecha);
		$("#via").val(data.via);
		$("#via").selectpicker('refresh');
		$("#apellidoNombre").val(data.apellido_nombre);
		$("#dni").val(data.dni);
		$('#domicilio').val(data.domicilio);
		$('#telefono').val(data.telefono);
		$('#email').val(data.email);
		$('#reclamo').val(data.reclamo);
		$('#respuesta').val(data.respuesta);
		$('#tramite').val(data.tramite);
		contadorCaracteres("#reclamo","#contReclamo");
		contadorCaracteres("#respuesta","#contRespuesta");
		contadorCaracteres("#tramite","#contTramite");	
		if(data.via=="FORMULARIO WEB"){
			$("#fecha").prop('disabled', true);
			$("#via").prop('disabled', true);
			$("#apellidoNombre").prop('disabled', true);
			$("#dni").prop('disabled', true);
			$("#domicilio").prop('disabled', true);
			$("#telefono").prop('disabled', true);
			$("#email").prop('disabled', true);
			$("#reclamo").prop('disabled', true);
		}
		


      } catch (error){
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}


//funcion para guardar o editar
function guardarOeditarSistema(e){
	var idRec=$("#idReclamo").val();
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/reclamo.php?op=guardarOeditarSistema',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
				$("#cargandoModal").modal('hide');
				if(datos=="¡Reclamo no se pudo crear!" || datos==0){
					Swal.fire({
						icon: 'error',
						title: '¡Reclamo no se pudo crear!',
					});
				}else{
					if(datos=="¡Registro no se pudo editar!"){
						Swal.fire({
							icon: 'error',
							title: '¡Reclamo no se pudo crear!',
						});
					}else{
						if(datos=="¡Registro editado con exito!"){
							Swal.fire({
								icon: 'success',
								title: '¡REC-'+idRec+' editado con exito!',
							});	
						}else{
							if(datos>0){
								Swal.fire({
									icon: 'success',
									title: '¡Reclamo creado con exito! <br> N° Reclamo: REC-'+datos,
								});	
							}else{
								Swal.fire({
									icon: 'error',
									title: datos,
								});
							}
							
						}
					}
					
					
				}


				mostrarForm(false);
				tabla.ajax.reload(); 
				limpiar();
		}
	});

}












init();