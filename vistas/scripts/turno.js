var tabla;
var alteracion;
var nuevo;
var fechaOriginal;
var horaOriginal;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});

	mostrarCalendario();

	$("#divSelectListar").hide();



}

// cclick en botones 
$("#btnListado").click(function (){
	$("#divSelectListar").show();
	$("#listar").val('todos');
	$("#listar").selectpicker('refresh');
	listarTodos();

});

$("#btnCalendario").click(function (){
	$("#divSelectListar").hide();
	mostrarCalendario();
});

// cambios en el select
$('#listar').change(function () {
  if ($(this).val() === 'todos') {
    listarTodos();
  }else{
  	if($(this).val() === 'hoy'){
  		listarHoy();
  	}
  }
});


 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});




//detecat cambio en fecha
$('#fecha').change(function(){
	 var date = $("#fecha").val();
	 //verificar disponibilidad
	 $.ajax({
			url:'../ajax/turno.php?op=disponibilidad',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'fecha': date}, //estos son los datos que envio


			success: function(datos){ //si la accion se hace de forma correcta se hace esto
				if(datos=='si'){
					cargarRadioHoraEditar(date,fechaOriginal,horaOriginal);
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
						allowOutsideClick: false
					});
					$("#fecha").val("");
				}
			 	
			 }


		});





    
});

//funcion mostrar cantidad caracteres escritos
function contadorCaracteres(campo,label){
	$(label).text($(campo).val().length);
}

//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}



//colocar la primer letra de palabra en mayusuclas 
function capitalizar(elemId) {
 	var txt = $("#" + elemId).val().toLowerCase();
 	$("#" + elemId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
 		return $1.toUpperCase(); }));
 }



//funcion para listar hoy
function listarHoy(){
	
		tabla=$("#tblListado").dataTable({
			//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../public/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					title: '',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'boton-csv',
					titleAttr: 'Exportar a CSV',
					title: '',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [1,2,3,4,5,6,7]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					title: '',
					
				},

				

			],  
			"ajax":{
				url:'../ajax/turno.php?op=listarHoy',
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},


			"bDestroy": true,
			"order":[[1,"desc"]] //para ordenar los registros
		}).DataTable();
		
	
}


//listado registros
function listarTodos(){

	
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x


		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla

		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6,7],
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		], 
		"ajax":{
			url:'../ajax/turno.php?op=listarTabla',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"desc"]] //para ordenar los registros
	}).DataTable();

	$("#calendario").hide();
	$("#listadoRegistros").show();

}

//calendario
function mostrarCalendario(){
	
	//traemos con ajax la variable de sesion de alteracion
	$.get("../ajax/turno.php?op=traerAlteracion", function(data, status){
		alteracion=data; //variable para comprobar la alteracion
	});

	//traemos con ajax la variable de sesion de nuevo
	$.get("../ajax/turno.php?op=traerNuevo", function(data, status){
		nuevo=data; //variable para comprobar la alteracion
	});

	var hoy = new Date();
	hoy.setDate(hoy.getDate()-1); //obtengo fecha de hoy

	$("#calendario").fullCalendar({
		
		// themeSystem: 'bootstrap3',
		validRange: {
			start: hoy,	//me mostrara solo a partir del dia de hoy
		},
		
		showNonCurrentDates: false,

		customButtons: {
			btnImprimir: {
				text: 'Imprimir',
				click: function() {
					window.print();
				}
			}
		},

		header: {
			left: 'prev,next,btnImprimir',
			center: 'title',
			right: 'month,basicWeek,basicDay,agendWeek,agendDay,listWeek'
		},

		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],      

		dayClick: function(date,jsEvent,view){
		//verificar si existen horarios disponibles en el dia
		var fecha = date.format();
		$.ajax({
			url:'../ajax/turno.php?op=disponibilidad',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'fecha': fecha}, //estos son los datos que envio


			success: function(datos){ //si la accion se hace de forma correcta se hace esto
				if(datos=='si'){
					if(nuevo==1){
						$("#fecha").val(date.format());
						cargarRadioHora(date.format());
						$("#modalDia").modal('show');

						$("#tituloEvento").html("Nuevo");

						$("#idTurno").val("");
						$("#apellidoNombre").val("");
						$("#dni").val("");
						$("#telefono").val("");		
						$("#email").val("");				
						$("#motivo").val("");
						$("#contMotivo").text("0");
						$("#fecha").prop('readonly',true);
						$("#apellidoNombre").prop('readonly',false);
						$("#dni").prop('readonly',false);
						$("#telefono").prop('readonly',false);
						$("#email").prop('readonly',false);
						$("#motivo").prop('readonly',false);
						$("#btnEditar").prop('disabled',true);
						$("#btnEditar").hide();
						$("#btnEliminar").prop('disabled',true);
						$("#btnEliminar").hide();
						$("#btnGuardar").prop('disabled',false);
						$("#btnGuardar").show();


					}
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
						allowOutsideClick: false
					});
				}
			 	
			 }


		});
			
			
			
			
		},

		eventClick:function(calEvent,jsEvent,view){
			$("#tituloEvento").html(calEvent.title);
			$("#idTurno").val(calEvent.id_turno);
			$("#apellidoNombre").val(calEvent.apellido_nombre);
			$("#dni").val(calEvent.dni);
			$("#telefono").val(calEvent.telefono);		
			$("#email").val(calEvent.email);		
			$("#motivo").val(calEvent.motivo);
			contadorCaracteres('#motivo','#contMotivo');
			

			fechaHora=calEvent.start._i.split(" ");
			$("#fecha").val(fechaHora[0]);

			var horaRadio = fechaHora[1].substring(0, 5); //cortamos el string para mostrar formato 00:00

			// aca traemos la hora seleccionada
			document.getElementById("hora").innerHTML = '<label class="radio-inline"><input type="radio" id="hora" name="hora" value='+fechaHora[1]+' checked>'+horaRadio+'</label>';
			
			

			//desabilitamos los input
			$("#fecha").prop("readonly", true);
			$("#apellidoNombre").prop("readonly", true);
			$("#dni").prop("readonly", true);
			$("#telefono").prop("readonly", true);
			$("#email").prop("readonly", true);
			$("#motivo").prop("readonly", true);



			$("#modalDia").modal('show');

			//si puede alterar mostramos botones 
			if(alteracion==1){
				
				$("#btnEditar").prop('disabled',false);
				$("#btnEditar").show();
				$("#btnEliminar").prop('disabled',false);
				$("#btnEliminar").show();
				$("#btnGuardar").prop('disabled',true);
				$("#btnGuardar").hide();
			}else{
				$("#btnEditar").prop('disabled',true);
				$("#btnEditar").hide();
				$("#btnEliminar").prop('disabled',true);
				$("#btnEliminar").hide();
				$("#btnGuardar").prop('disabled',true);
				$("#btnGuardar").hide();
			}
			
		},

		events:'../ajax/turno.php?op=listarCalendario',
		navLinks: true, //me agrega un link a la fecha
		eventLimit: true,

		
	});

	$("#calendario").show();
	$("#listadoRegistros").hide();
	$("#calendario").fullCalendar('refetchEvents');
}


function seleccionarFecha(){
	$('#calendario').fullCalendar('gotoDate', '2020-05-10'); //va a una fecha especifica
}




function cargarRadioHora(fecha){
	$.ajax({
			url:'../ajax/turno.php?op=radioHora',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'fecha': fecha}, //estos son los datos que envio
		

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
			 	$("#hora").html(datos); //con html me reemplaza todo el contenido
			}


	});
}

function cargarRadioHoraEditar(fecha,fechaOriginal,horaOriginal){
	$.ajax({
			url:'../ajax/turno.php?op=radioHoraEditar',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'fecha': fecha, 'fechaOriginal': fechaOriginal, 'horaOriginal': horaOriginal}, //estos son los datos que envio
		

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
			 	$("#hora").html(datos); //con html me reemplaza todo el contenido
			}


	});
}

function cargarRadioHoraEvento(fecha){
	$.ajax({
			url:'../ajax/turno.php?op=radioHoraEvento',//lugar a donde se envia los datos obtenidos del formulario
			type: "post",
			data: {'fecha': fecha}, //estos son los datos que envio
			


			success: function(datos){ //si la accion se hace de forma correcta se hace esto
			 	$("#hora").append(datos); //con append, me agrega al final conservando lo anterior

			}
	});
}


//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/turno.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			if(datos=="¡Horario ya fue reservado! Elija otro.."){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
				$("#btnGuardar").prop("disabled",false);

			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}

				$("#modalDia").modal('hide');				
			}

			$("#calendario").fullCalendar('refetchEvents');

			
		}
	});

}

//editar turno
function editar(){
	// aca traemos los horarios disponibles si hubiesen
	var fecha = $("#fecha").val();
	cargarRadioHoraEvento(fecha);

	//damos valores a estas variables
	fechaOriginal=$("#fecha").val();
	horaOriginal=$("#hora").text();
	



	//habilitamos cosas
	$("#fecha").prop("readonly",false);
	$("#apellidoNombre").prop("readonly",false);
	$("#dni").prop("readonly",false);
	$("#telefono").prop("readonly",false);
	$("#email").prop("readonly",false);
	$("#motivo").prop("readonly",false);
	$("#btnGuardar").prop('disabled',false);
	$("#btnGuardar").show();
	$("#btnEditar").prop('disabled',true);
	$("#btnEditar").hide();
	$("#btnEliminar").prop('disabled',true);
	$("#btnEliminar").hide();
}


//funcion para eliminar
function eliminar(){
	var idTurno = $("#idTurno").val();
	Swal.fire({
		title: '¿Esta seguro que desea eliminar el registro?',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		showConfirmButton:true,
		confirmButtonText: 'Confirmar',
		cancelButtonText: 'Cancelar',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$.post("../ajax/turno.php?op=eliminar", {idTurno : idTurno}, function(e){
				if(e=="¡Registro eliminado con exito!"){
					Swal.fire({
						icon: 'success',
						title: e,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: e,
					});
				}

				$("#modalDia").modal('hide');	
				$("#calendario").fullCalendar('refetchEvents');

			});
		}
	});

}


//funcion para eliminar desde tabla
function eliminarDesdeTabla(idTurno){
	Swal.fire({
		title: '¿Esta seguro que desea eliminar el registro?',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		showConfirmButton:true,
		confirmButtonText: 'Confirmar',
		cancelButtonText: 'Cancelar',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			$("#cargandoModal").modal('show');
			$.post("../ajax/turno.php?op=eliminarDesdeTabla", {idTurno : idTurno}, function(e){
						if(e=="¡Registro eliminado con exito!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#cargandoModal").modal('hide');

					});
		}
	});

}



init();