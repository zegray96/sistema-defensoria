var tabla;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})


	

	
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});


//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}



//funcion limpiar
function limpiar(){
	$("#idRefPre").val("");
	$("#apellidoNombre").val("");
	$("#barrio").val("");
	$("#dni").val("");
	$("#domicilio").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#cargo").val("1");
	$('#cargo').selectpicker('refresh');

}



//funcion para mostrar formulario
function mostrarForm(mostrar){
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnNuevo").hide();
		$("#btnGuardar").prop("disabled",false);
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();

	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}


//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6,7]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/referentePresidente.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
}


//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/referentePresidente.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
				});
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}
			mostrarForm(false);
			tabla.ajax.reload(); 
			limpiar();
				


				


			
			
		}
	});

}


//funcion mostrar
function mostrar(idRefPre){
	$("#cargandoModal").modal('show');
	$.post("../ajax/referentePresidente.php?op=mostrar", {idRefPre: idRefPre}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idRefPre").val(data.id_ref_pre);
		$("#apellidoNombre").val(data.apellido_nombre);
		$("#barrio").val(data.barrio);
		$("#dni").val(data.dni);
		$("#domicilio").val(data.domicilio);
		$("#telefono").val(data.telefono);
		$("#email").val(data.email);
		$('#cargo').val(data.cargo);
		$('#cargo').selectpicker('refresh');

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}

//funcion para eliminar
	function eliminar(idRefPre){
		Swal.fire({
		  title: '¿Esta seguro que desea eliminar el registro?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.post("../ajax/referentePresidente.php?op=eliminar", {idRefPre : idRefPre}, function(e){
						if(e=="¡Registro eliminado con exito!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#cargandoModal").modal('hide');

				});
		  }
		});
	}	

	


init();