var tabla;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	})

	$("#formularioNuevoOrgNombre").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOrgNombre(e);
	})

	cargarSelect();


	

	
}

//evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//funcion limpiar
function limpiar(){
	$("#idOrganismo").val("");
	$("#idOrgNombre").val("");
	$("#autoridad").val("");
	$("#telefono").val("");
	$("#email").val("");
	

}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnNuevo").hide();
		$("#btnGuardar").prop("disabled",false);
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
		cargarSelect();

	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}

//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "lengthMenu": [[5, 100, 1000], [5, 100, 1000]],

		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		],  
		"ajax":{
			url:'../ajax/organismo.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
}




//funcion para guardar o editar
function guardarOeditar(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/organismo.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
				});
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}
			mostrarForm(false);
			tabla.ajax.reload(); 
			limpiar();
				
				


				


			
			
		}
	});

}

function limpiarOrgNombre(){
	$("#orgNombre").val("");
}



//funcion para guardar
function guardarOrgNombre(e){
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($("#formularioNuevoOrgNombre")[0]); //guardo todos los datos del form en formData
	$("#cargandoModal").modal('show');
	$.ajax({
		url:'../ajax/organismo.php?op=guardarOrgNombre',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ //si la accion se hace de forma correcta se hace esto
			$("#cargandoModal").modal('hide');
			if(datos=="¡Nombre de Organismo ya existe!"){
				Swal.fire({
					icon: 'error',
					title: datos,
				});
			}else{
				if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: datos,
					});
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
					});
				}
				cargarSelect();
				limpiarOrgNombre();
				$("#modalNuevoOrgNombre").modal('hide');
			}
			

		}
	});

}



//funcion mostrar
function mostrar(idOrganismo){
	$("#cargandoModal").modal('show');
	$.post("../ajax/organismo.php?op=mostrar", {idOrganismo: idOrganismo}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idOrganismo").val(data.id_organismo);
		$("#idOrgNombre").val(data.id_org_nombre);
		$('#idOrgNombre').selectpicker('refresh');
		$("#autoridad").val(data.autoridad);
		$("#telefono").val(data.telefono);
		$("#email").val(data.email);

      } catch (error){
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}

function cargarSelect(){
	//cargamos los items al select
	$.post("../ajax/organismo.php?op=selectOrgNombre", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idOrgNombre").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idOrgNombre').selectpicker('refresh'); //refresco el select

	});	
}



//funcion para eliminar
	function eliminar(idOrganismo){
		Swal.fire({
		  title: '¿Esta seguro que desea eliminar el registro?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');
				$.post("../ajax/organismo.php?op=eliminar", {idOrganismo : idOrganismo}, function(e){
						if(e=="¡Registro eliminado con exito!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});
						}else{
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#cargandoModal").modal('hide');

				});
		  }
		});
}	


init();