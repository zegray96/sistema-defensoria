var tabla;
var imagenDniAdelanteActual;

//Funcion que se ejecuta al inicio
function init(){
	$("#contenedor").css("text-align",""); //cambiamos el css del contenedor
	$("#contenido").show(); //mostrmos el contenido
	$("#cargandoGif").hide(); //ocultamos el gif cargando
	mostrarForm(false);
	listar();

	$("#formulario").on("submit", function(e){ //si en el form se activa el submit se hara esto
		guardarOeditar(e);
	});

	
}


//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	console.log("num", num);
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

 //evitar que el enter haga el submit en los input text
document.addEventListener('DOMContentLoaded', () => {
	document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
	  if(e.keyCode == 13) {
	    e.preventDefault();
	  }
	}))
});

//validar imagen dni adelante
function validarDniAdelante(){
        var archivo = $("#imagenDniAdelante").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".jpg" && extensiones != ".jpeg" && extensiones != ".png"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#imagenDniAdelante").val(null);
		}
}

//validar imagen dni atras
function validarDniAtras(){
        var archivo = $("#imagenDniAtras").val();
		var extensiones = archivo.substring(archivo.lastIndexOf("."));
		if(extensiones != ".jpg" && extensiones != ".jpeg" && extensiones != ".png"){
		    Swal.fire({
				icon: 'error',
				title: "El archivo de tipo " + extensiones + " no es válido",
			});
		    $("#imagenDniAtras").val(null);
		}
}







//funcion limpiar
function limpiar(){
	$("#idPersonal").val("");
	$("#apellidoNombre").val("");
	$("#dni").val("");
	$("#cuil").val("");
	$("#nroLegajo").val("");
	$("#situacionRevista").val("");
	$("#situacionRevista").selectpicker('refresh');
	$("#categoria").val("");
	$("#categoria").selectpicker('refresh');
	$("#email").val("");
	$("#telefono").val("");
	$("#domicilio").val("");
	$("#imagenDniAdelanteActual").val("");
	$("#imagenDniAdelante").val("");
	$("#imagenDniAtrasActual").val("");
	$("#imagenDniAtras").val("");


	
}

//funcion para mostrar formulario
function mostrarForm(mostrar){
	
	limpiar();
	if(mostrar){
		$("#listadoRegistros").hide();
		$("#formularioRegistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnNuevo").hide();
		$("#dniAdelanteMuestra").hide();
		$("#dniAtrasMuestra").hide();
	}else{
		$("#listadoRegistros").show();
		$("#formularioRegistros").hide();
		$("#btnNuevo").show();
	}
}

//funcion ocultarForm
function ocultarForm(){
	limpiar();
	mostrarForm(false);
}


//funcion para listar
function listar(){
	tabla=$("#tblListado").dataTable({
		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../public/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x


		"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla

		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8,9]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				title: '',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8,9]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'boton-csv',
				titleAttr: 'Exportar a CSV',
				title: '',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8,9],
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				title: '',
				
			},

			

		], 
		"ajax":{
			url:'../ajax/recursoHumano.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"order":[[1,"asc"]] //para ordenar los registros
	}).DataTable();
}





//funcion para guardar o editar
function guardarOeditar(e){
		e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
		$("#btnGuardar").prop("disabled",true);
		var formData = new FormData($("#formulario")[0]); //guardo todos los datos del form en formData
		$("#cargandoModal").modal('show');
		$.ajax({
			url:'../ajax/recursoHumano.php?op=guardarOeditar',//lugar a donde se envia los datos obtenidos del formulario
			type: "POST",
			data: formData, //estos son los datos que envio
			contentType:false,
			processData:false,

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
				$("#cargandoModal").modal('hide');
					if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
						Swal.fire({
							icon: 'success',
							title: datos,
						});
					}else{
						Swal.fire({
							icon: 'error',
							title: datos,
						});
					}
					mostrarForm(false);
					tabla.ajax.reload(); 
					limpiar();
				
				
			}
		});
	

	
	
	

}

//funcion mostrar
function mostrar(idPersonal){
	$("#cargandoModal").modal('show');
	$.post("../ajax/recursoHumano.php?op=mostrar", {idPersonal: idPersonal}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
		mostrarForm(true);
		$("#cargandoModal").modal('hide');
		$("#idPersonal").val(data.id_personal);
		$("#apellidoNombre").val(data.apellido_nombre);
		$("#dni").val(data.dni);
		$("#cuil").val(data.cuil);
		$("#situacionRevista").val(data.situacion_revista);
		$("#situacionRevista").selectpicker('refresh');
		$("#categoria").val(data.categoria);
		$("#categoria").selectpicker('refresh');
		$("#nroLegajo").val(data.nro_legajo);
		$("#email").val(data.email);
		$("#telefono").val(data.telefono);
		$("#domicilio").val(data.domicilio);

		if(!data.imagen_dni_adelante==""){
			// utiliza math random para que no me detecte la imagen en cache
			$("#enlaceDniAdelante").attr("href","../files/imagenesDni/"+data.imagen_dni_adelante+"?"+Math.random());
			$("#dniAdelanteMuestra").show();
			$("#dniAdelanteMuestra").attr("src","../files/imagenesDni/"+data.imagen_dni_adelante+"?"+Math.random());
		}
		$("#imagenDniAdelanteActual").val(data.imagen_dni_adelante);

		if(!data.imagen_dni_atras==""){
			$("#enlaceDniAtras").attr("href","../files/imagenesDni/"+data.imagen_dni_atras+"?"+Math.random());
			$("#dniAtrasMuestra").show();
			$("#dniAtrasMuestra").attr("src","../files/imagenesDni/"+data.imagen_dni_atras+"?"+Math.random());
		}
		$("#imagenDniAtrasActual").val(data.imagen_dni_atras);

		
      } catch (error){
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}


//funcion para eliminar
function eliminar(idPersonal){
		Swal.fire({
		  title: '¿Esta seguro que desea eliminar el registro?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  showConfirmButton:true,
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
		    	$("#cargandoModal").modal('show');

				$.post("../ajax/recursoHumano.php?op=eliminar", {idPersonal : idPersonal}, function(e){
						//bootbox.alert(e); //tipo de alerta , e es el mensaje qu retorno de usuario.php de ajax
						if(e=="¡Registro Eliminado con Exito!"){
							Swal.fire({
							  icon: 'success',
							  title: e,
							});

						}else{
							
							Swal.fire({
							  icon: 'error',
							  title: e,
							});
						}
						
						tabla.ajax.reload();
						$("#cargandoModal").modal('hide');

				});
		  }
		});

}




init();