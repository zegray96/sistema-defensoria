<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">   
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fas fa-user-friends"></i> Referentes/Presidentes</h1>
                    </div>
                    <?php
                      if($_SESSION['v_ref_pre_org']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado
                    ?>

                    <div class="box-header with-border">
                          <?php
                            if($_SESSION['new_ref_pre_org']==1){
                            echo '
                                    <button id="btnNuevo" class="btn btn-success" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                                  ';
                            } 
                          ?>
                           
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">
                        <table id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%">
                          <thead>
                            <th>&nbsp;Opciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Cargo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Telefono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Email&nbsp;</th>
                            
                          </thead>

                        </table>
                    </div>


                    <div class="panel-body" id="formularioRegistros"> 
                         
                          
                          <form name="formulario" id=formulario method="POST">

                            <div class="formMedio">  
                              <div class="formSelect">
                                <label style="color: red; font-size: 15px">(*) Campos Obligatorios</label>
                              </div>
                              <input type="hidden" name="idRefPre" id="idRefPre">
                              <div class="formSelect">
                                <label><label style="color: red;">(*)</label> Cargo: </label>
                                <select id="cargo" name="cargo" class="form-control selectpicker" required>
                                  <option selected="true" disabled="disabled">[SELECCIONAR]</option>  
                                  <option value='REFERENTE'>REFERENTE</option>
                                  <option value='PRESIDENTE DE BARRIO'>PRESIDENTE DE BARRIO</option> 
                                  <option value='PRESIDENTE PRO TIERRA'>PRESIDENTE PRO TIERRA</option>
                                </select>
                              </div>

                              <label><label style="color: red;">(*)</label> Barrio: </label>
                              <input type="text" class="texto form-control" name="barrio" id="barrio" maxlength="100" placeholder="Barrio" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Apellido y Nombre: </label>
                              <input type="text" class="texto form-control" name="apellidoNombre" id="apellidoNombre" maxlength="100" placeholder="Apellido y Nombre" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Dni: </label>
                              <input type="text" class="texto form-control" name="dni" id="dni" maxlength="50" placeholder="DNI" required onkeyup="format(this)" onchange="format(this)">

                              <label><label style="color: red;">(*)</label> Domicilio: </label>
                              <input type="text" class="texto form-control" name="domicilio" id="domicilio" maxlength="100" placeholder="Domicilio" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Telefono: </label>
                              <input type="text" class="texto form-control" name="telefono" id="telefono" maxlength="100" placeholder="TELEFONO" required>

                              <label> Email: </label>
                              <input type="text" class="texto form-control" name="email" id="email" maxlength="100" placeholder="EMAIL">


 
                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                              
                              

                            </div>  
                            
                          </form>


                          
                      
                    </div>
                    <!--Fin centro -->

                    <?php
                              } //Fin contenido autorizado
                    ?>  
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->

  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/referentePresidente.js?ver=<?php echo $version; ?>"></script>
<?php

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>



