<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fa fa-user"></i> Usuarios</h1>
                    </div>
                    <?php
                      if($_SESSION['v_configuracion']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado
                    ?>

                    <div class="box-header with-border">
                          <?php
                            if($_SESSION['new_configuracion']==1){
                            echo '
                                   <button id="btnNuevo" class="btn btn-success" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                                  ';
                            } 
                          ?> 
                          
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">
                        <table id="tblListado" class="table table-striped table-bordered table-hover"  style="width:100%;">
                          <thead>
                            <th style="width: 70px;">&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Tipo&nbsp;Usuario&nbsp;</th>
                            <th>&nbsp;Usuario&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;</th>
                          </thead>

                        </table>
                    </div>
                    <div class="panel-body" id="formularioRegistros">
                         
                          
                          <form name="formulario" id=formulario method="POST">

                            <div class="formMedio">  
                              <div class="formSelect">
                                <label style="color: red;  font-size:15px">(*) Campos Obligatorios </label>
                              </div>
                              <input type="hidden" name="idUsuario" id="idUsuario">
                              <label><label style="color: red;">(*)</label> Nombre: </label>
                              <input type="text" class="texto form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
                            

                              <div class="formSelect">
                                <label><label style="color: red;">(*)</label> Tipo Usuario: </label>
                                <select id="idTipoUsuario" name="idTipoUsuario" class="form-control selectpicker" data-live-search="true" required> </select>
                              </div>

                              <label><label style="color: red;">(*)</label> Usuario: </label>
                              <input type="text" class="texto form-control" name="usuario" id="usuario" maxlength="50" placeholder="Usuario" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Clave: </label>
                              <input type="text" class="texto form-control" name="clave" id="clave" maxlength="100" placeholder="CLAVE" required>

                              <div class="formSelect">
                                <label><label style="color: red;">(*)</label> Estado: </label>
                                <select id="estado" name="estado" class="form-control selectpicker" required>
                                  <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>  
                                  <option value='ACTIVO'>ACTIVO</option>
                                  <option value='INACTIVO'>INACTIVO</option> 
                                </select>
                              </div>

                              <div id="btnModificarClave" style="margin-bottom: 20px;"><a data-toggle="modal" href="#modalModificarClave" class="btn btn-info"><i class="fas fa-lock"></i> Modificar clave</a></div>

                             
                                
                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button> 
                              
                              

                            </div>  
                            
                          </form>

                    

                      
                    </div>
                    <!--Fin centro -->

                    <?php
                              } //Fin contenido autorizado
                    ?>  
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->


  <!--Modal modificar clave -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalModificarClave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div style="text-align: center; margin-top: 10px">
             <h3 style="text-align: center" class="modal-title">Ingrese nueva clave</h3>
          </div>
        </div>
        <div class="modal-body">
          <form name="formularioModificarClave" id="formularioModificarClave" method="POST">
            <input type="hidden" name="idModificarClave" id="idModificarClave">
            <input type="text" class="texto form-control" name="nuevaClave" id="nuevaClave" maxlength="100" placeholder="CLAVE" required>          
        </div> 
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarNuevaClave()"><i class="fas fa-times"></i> Cerrar</button>
            <button class="btn btn-primary" type="submit" id="btnGuardarClave"><i class="fa fa-save"></i> Guardar</button>  
          </form> 

        </div>
      </div>
    </div> 
  </div>

  <!--Fin Modal modificar clave-->

  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/usuario.js?ver=<?php echo $version; ?>"></script>
<?php

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>



