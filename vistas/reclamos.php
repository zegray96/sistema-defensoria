<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
  
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="far fa-clipboard"></i> Reclamos</h1>
                    </div>
                    <?php
                      if($_SESSION['v_reclamos']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado

                    ?>

                    <div id="cabecera" class="box-header with-border">
                      <p class="label bg-orange" id="mostrando" name="mostrando"></p> 
                      


                      <div id="divSelectListar" class="box-header">
                        <label>FILTRO:</label>
                        <select id="listar" class="form-control selectpicker">
                          <option value="todos">TODOS</option>
                          <option value="porFechas">POR FECHAS</option>
                          <option value="hoy" selected="true">HOY</option>
                          <option value="enEsperaDeRespuesta">EN ESPERA DE RESPUESTA</option>
                        </select>
                      </div>

                      <?php
                        if($_SESSION['new_reclamos']==1){
                        echo '
                                 <button id="btnNuevo" class="btn btn-success" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                              ';
                        } 
                      ?>
                        
                        <a id="btnBuscar" data-toggle="modal" href="#modalBuscarPorNro" ><button class="btn btn-primary"><i class="fas fa-search"></i> Buscar por N°</button></a>
                     

                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">
                        <table id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%;">
                          <thead>
                            <th>Id</th>
                            <th>&nbsp;Opciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th> 
                            <th>&nbsp;N°&nbsp;Reclamo&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;</th>
                            <th>&nbsp;Vía&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Dni&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Reclamo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Respuesta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Tramite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Telefono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Ultima&nbsp;Modificación&nbsp;</th> 
                          </thead>

                        </table>

                    </div>


                    <div class="panel-body" id="formularioRegistros"> 
                         
                          
                          <form name="formulario" id=formulario method="POST">
                          <div class="formMedio"> 
                            
                              <div class="formSelect">
                                <label style="color: red; font-size: 15px;">(*) Campos Obligatorios </label>
                              </div>
                              <input type="hidden" name="idReclamo" id="idReclamo">
                              

                              <div class="formSelect">
                              <label><label style="color: red;">(*)</label> Fecha: </label>
                              <br><input class="form-control" required type="date" name="fecha" id="fecha" style="width:200px;">
                              </div>

                              <div class="formSelect">
                                <label><label style="color: red;">(*)</label> Vía:</label>
                                <select id="via" name="via" required class="form-control selectpicker">
                                  <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option> 
                                  <option value='WHATSAPP'>WHATSAPP</option>
                                  <option value='EMAIL'>EMAIL</option>
                                  <option value='TELEFONO'>TELEFONO</option>
                                  <option value='VISITA'>VISITA</option>
                                  <option value='FORMULARIO WEB' disabled="disabled">FORMULARIO WEB</option>
                                </select>
                              </div>
    

                              <label><label style="color: red;">(*)</label> Apellido y Nombre: </label>
                              <input type="text" class="texto form-control" name="apellidoNombre" id="apellidoNombre" maxlength="100" placeholder="Apellido y Nombre" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Dni: </label>
                              <input type="text" class="texto form-control" name="dni" id="dni" maxlength="10" placeholder="DNI" required onkeyup="format(this)" onchange="format(this)">

                              <label><label style="color: red;">(*)</label> Domicilio: </label>
                              <input type="text" class="texto form-control" name="domicilio" id="domicilio" maxlength="100" placeholder="DOMICILIO" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">

                              <label><label style="color: red;">(*)</label> Telefono: </label>
                              <input type="text" class="texto form-control" name="telefono" id="telefono" maxlength="35" placeholder="TELEFONO" required>

                              <label>Email: </label>
                              <input type="text" class="texto form-control" name="email" id="email" maxlength="35" placeholder="EMAIL">


                              <label><label style="color: red;">(*)</label> Reclamo: </label> <label style="font-weight: normal" id="contReclamo">0</label> <label style="font-weight: normal">/ 500 Caracteres</label>
                              <div>
                                  <textarea maxlength="500" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="reclamo" id="reclamo" rows="5" cols="65" onkeyup="return contadorCaracteres('#reclamo','#contReclamo')" placeholder="RECLAMO" required style="resize: none"></textarea>
                              </div>

                              <label> Tramite: </label> <label style="font-weight: normal" id="contTramite">0</label> <label style="font-weight: normal">/ 200 Caracteres</label>
                              <div>
                                  <textarea maxlength="200" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="tramite" id="tramite" rows="5" cols="65" onkeyup="return contadorCaracteres('#tramite','#contTramite')" placeholder="TRAMITE" style="resize: none"></textarea>
                              </div>
                              
                              <label> Respuesta: </label> <label style="font-weight: normal" id="contRespuesta">0</label> <label style="font-weight: normal">/ 500 Caracteres</label>
                              <div>
                                  <textarea maxlength="500" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="respuesta" id="respuesta" rows="5" cols="65" onkeyup="return contadorCaracteres('#respuesta','#contRespuesta')" placeholder="RESPUESTA" style="resize: none"></textarea>
                              </div>
                              
                              <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                              <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button> 
                           </div>
                          </form>

                      
                    </div>

                    
                    <!--Fin centro -->
                    <?php
                                } //Fin contenido autorizado
                    ?>  

                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->
  

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->

  <!--Modal seleccionar fecha -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalSeleccionarFecha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 style="text-align: center" class="modal-title">Seleccione Fechas</h3>
        </div>
        <div class="modal-body">
           <div class="formSelect"><label>Fecha Inicio: </label><input class="form-control" type="date" name="fechaIniListar" id="fechaIniListar" style="width:200px;"></div>

           <div class="formSelect"><label>Fecha Fin:</label><input class="form-control" type="date" name="fechaFinListar" id="fechaFinListar" style="width:200px;"></div>

                        
        </div> 
        <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarFecha()"><i class="fas fa-times"></i> Cerrar</button>
         <button class="btn btn-primary" onclick="listarPorFecha()"><i class="fas fa-search"></i> Listar Reclamos</button>             
        </div>
      </div>
    </div> 
  </div>

  <!--Fin Modal seleccionar fecha-->

  <!--Modal buscar por nro -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalBuscarPorNro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 style="text-align: center" class="modal-title">Ingrese N° de reclamo:</h3>
        </div>
        <div style="text-align: center" class="modal-body">
           <input class="texto form-control" type="text" name="nroReclamoBuscar" id="nroReclamoBuscar" onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
           
           <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarNro()"><i class="fas fa-times"></i> Cerrar</button>
           <button class="btn btn-primary" onclick="buscarPorNro()"><i class="fas fa-search"></i> Buscar</button>                 
        </div> 
        
      </div>
    </div> 
  </div>

  <!--Fin Modal buscar por nro-->
  
  <!--Modal cargando -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal cargando -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/reclamo.js?ver=<?php echo $version; ?>"></script>

<?php
}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>


