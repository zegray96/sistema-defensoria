<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">   
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fas fa-file-alt"></i> Resoluciones</h1>
                    </div>
                    <?php
                      if($_SESSION['v_administracion']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado
                    ?>
                    

                    <div class="box-header with-border">
                          <div id="divSelectListar" class="box-header">
                            <label>FILTRO:</label>
                            <select id="listarPorAnio" class="form-control selectpicker" data-live-search="true">
                              <?php 
                              date_default_timezone_set('America/Argentina/Buenos_Aires');
                              $anio=date('Y');
                              $anioFin=2016;
                              echo '<option value='.$anio.' selected>'.$anio.'</option>';
                              $anio--;
                              while ($anio>=2020) {
                                echo '<option value='.$anio.'>'.$anio.'</option>';
                                $anio--;
                              }

                              ?>

                            </select>
                          </div>
                          <?php
                            if($_SESSION['new_administracion']==1){
                            echo '
                                    <button id="btnNuevo" class="btn btn-success" onclick="mostrarFormNuevo(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                                  ';
                            } 
                          ?>

                
                           
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">
                        <table id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%">
                          <thead>
                            <th>&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;N°&nbsp;Resolución&nbsp;</th>
                            <th>&nbsp;Año&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Área&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Detalle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Reservado&nbsp;por&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Ultima&nbsp;Modificación&nbsp;</th>
                            
                          </thead>

                        </table>
                    </div>


                    <div class="panel-body" id="formularioRegistros"> 
                         
                          
                          <form name="formulario" id=formulario method="POST">

                            <div class="formMedio">  
                              <div class="col-xs-12">
                                <div class="formSelect">
                                  <label style="color: red; font-size: 15px">(*) Campos Obligatorios</label>
                                </div>
                                <input type="hidden" name="idResolucion" id="idResolucion">

                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Fecha:</label>
                                  <br><input class="form-control" type="date" name="fecha" id="fecha" required style="width:200px;">
                                </div>



                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Estado: </label>
                                  <select id="estado" name="estado" class="form-control selectpicker" required>
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>  
                                    <option value='CONCLUIDO'>CONCLUIDO</option>
                                    <option value='RESERVADO'>RESERVADO</option> 
                                  </select>
                                </div>
                              </div>
                              

                              <div id="divNro" class="col-lg-6 col-xs-12" style="display: none; margin-bottom: 20px;">
                                <label><label style="color: red;">(*)</label> N° Resolución:</label>
                                <input type="text" class="texto form-control" name="nroResolucion" id="nroResolucion" placeholder="N° RESOLUCION"  readonly="true" required onkeypress='return validaNumericos(event)' min="1">
                              </div>

                              <div id="divAnio" class="col-lg-6 col-xs-12" style="margin-bottom: 20px;">
                                <label><label style="color: red;">(*)</label> Año: </label>
                                <select data-size="6" id="anio" name="anio" data-live-search="true" class="form-control selectpicker" required>
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>
                                    <?php 
                                    date_default_timezone_set('America/Argentina/Buenos_Aires');
                                    $anio=date("Y");
                                    $anioFin=2016;
                                    while ($anio>=2020) {
                                      echo '<option value='.$anio.'>'.$anio.'</option>';
                                      $anio--;
                                    }

                                    ?>                                     
                                </select>
                              </div>

                              

                              <div class="col-xs-12">
                                
                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Área: </label>
                                  <select id="area" name="area" class="form-control selectpicker" required>
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>  
                                    <option value='ADMINISTRACION'>ADMINISTRACION</option>
                                    <option value='JURIDICO'>JURIDICO</option> 
                                    <option value="RECURSOS HUMANOS">RECURSOS HUMANOS</option>
                                  </select>
                                </div>

                                <label><label style="color: red;">(*)</label> Detalle: </label> <label style="font-weight: normal" id="contDetalle">0</label> <label style="font-weight: normal">/ 150 Caracteres</label>
                                <div>
                                  <textarea maxlength="150" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="detalle" id="detalle" rows="3" cols="65" onkeyup="return contadorCaracteres('#detalle','#contDetalle')"  required placeholder="Detalle" onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase; resize: none"></textarea>
                                </div>


                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Reservado por: </label>
                                  <select id="idAgente" name="idAgente" class="form-control selectpicker" required data-live-search="true">
                                    <!-- datos del select -->
                                  </select>
                                </div>

                              </div>

                                
                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                                <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button> 
                              

                            </div>  
                            
                          </form>


                          
                      
                    </div>
                    <!--Fin centro -->

                    <?php
                              } //Fin contenido autorizado
                    ?>  
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->


  <!--Modal buscar por nro -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalBuscarPorNro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 style="text-align: center" class="modal-title">Ingrese N° resolución:</h3>
        </div>
        <div style="text-align: center" class="modal-body">
           <input class="texto form-control" type="text" name="nroResolucionBuscar" id="nroResolucionBuscar" onkeypress='return validaNumericos(event)' min="1">
           
           <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="limpiarNro()"><i class="fas fa-times"></i> Cerrar</button>
           <button class="btn btn-primary" onclick="buscarPorNro()"><i class="fas fa-search"></i> Buscar</button>                 
        </div> 
        
      </div>
    </div> 
  </div>

  <!--Fin Modal buscar por nro-->


  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/resolucion.js?ver=<?php echo $version; ?>"></script>
<?php

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>



