<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
  //Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600; float: left;" ><i class="fa fa-folder-open"></i> Expedientes&nbsp;&nbsp;</h1>

                        <div id="divTituloRegistrosPorRevisar">
                            <h1 id="cantRegistrosPorRevisar" class="box-title parpadea" style="float:left; font-size:20px; font-weight: 600; color:red;"></h1>
                        </div>
                    </div>
                    <?php
                      if($_SESSION['v_expedientes']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{
                      //Contenido autorizado  

                    ?>




                    <div class="box-header with-border">

                      <div id="divSelectListar" class="box-header">
                        <label>FILTRO:</label>
                        <select id="listarPorAnio" class="form-control selectpicker" data-live-search="true">
                          <option value="porRevisar">POR REVISAR</option>
                          <?php 
                            date_default_timezone_set('America/Argentina/Buenos_Aires');
                            $anio=date('Y');
                            $anioFin=2016;
                            echo '<option value='.$anio.' selected>'.$anio.'</option>';
                            $anio--;
                            while ($anio>=2016) {
                              echo '<option value='.$anio.'>'.$anio.'</option>';
                              $anio--;
                            }

                          ?>
                          
                        </select>
                      </div>

                      <?php
                        if($_SESSION['new_expedientes']==1){
                        echo '
                                <button id="btnNuevo" class="btn btn-success" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                              ';
                        } 
                      ?>
                       
                       

                    </div>
                    
                    <!-- /.box-header -->

                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">

                        <table  id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%;">
                          <thead>
                            <th>Id</th>
                            <th>&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;Fecha&nbsp;Exp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Estado&nbsp;</th>
                            <th>&nbsp;N°&nbsp;Expediente&nbsp;</th>
                            <th>&nbsp;Requirente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Trámite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Agente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Informe&nbsp;Inicial&nbsp;</th>
                            <th>&nbsp;Informe&nbsp;15&nbsp;días&nbsp;</th>
                            <th>&nbsp;Informe&nbsp;30&nbsp;días&nbsp;</th>
                            <th>&nbsp;Informe&nbsp;45&nbsp;días&nbsp;</th>
                            <th>&nbsp;Informe&nbsp;Mensual&nbsp;</th>
                            <th>&nbsp;Resolución&nbsp;</th>
                            <th>&nbsp;Observación&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Ultima&nbsp;Modificación&nbsp;</th>
                            
                          </thead>

                        </table>

                    </div>
                    <div class="panel-body" id="formularioRegistros">
                         
                          
                          <form name="formulario" id=formulario method="POST">
                            <div class="formMedio"> 
                              
                              <div class="col-lg-12 col-xs-12">
                                <div class="formSelect">
                                  <label style="color: red; font-size: 15px;">(*) Campos Obligatorios </label>
                                </div>
                                
                                <input type="hidden" name="idExpediente" id="idExpediente">
                                


                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Fecha:</label>
                                  <br><input class="form-control" type="date" name="fecha" id="fecha" required style="width:200px;">
                                </div>


                                <div class="formSelect">
                                  <label><label style="color: red;">(*)</label> Estado:</label>
                                  <select id="estado" name="estado" class="form-control selectpicker" required>
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>  
                                    <option value='EN TRAMITE'>EN TRAMITE</option>
                                    <option value='CERRADO'>CERRADO</option>
                                    <option value='JURIDICO'>JURIDICO</option>
                                    <option value='PARA CIERRE'>PARA CIERRE</option>
                                  </select>
                                </div>
                              </div>


                              <div id="divNro" class="col-lg-4 col-xs-12" style="margin-bottom: 20px;">
                                <label><label style="color: red;">(*)</label> N°: </label>
                                <input type="text" class="form-control" name="nro" id="nro" maxlength="100" placeholder="N°" onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">
                              </div>

                              <div id="divLetra" class="col-lg-4 col-xs-12" style="margin-bottom: 20px;">
                                <label><label style="color: red;">(*)</label> Letra: </label>
                                <select data-size="6" id="letra" name="letra" data-live-search="true" class="form-control selectpicker">
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>  
                                    <option value='A'>A</option>
                                    <option value='B'>B</option>
                                    <option value='C'>C</option>
                                    <option value='D'>D</option>
                                    <option value='E'>E</option>
                                    <option value='F'>F</option>
                                    <option value='G'>G</option>
                                    <option value='H'>H</option>
                                    <option value='I'>I</option>
                                    <option value='J'>J</option>
                                    <option value='K'>K</option>
                                    <option value='L'>L</option>
                                    <option value='M'>M</option>
                                    <option value='N'>N</option>
                                    <option value='O'>O</option>
                                    <option value='P'>P</option>
                                    <option value='Q'>Q</option>
                                    <option value='R'>R</option>
                                    <option value='S'>S</option>
                                    <option value='T'>T</option>
                                    <option value='U'>U</option>
                                    <option value='V'>V</option>
                                    <option value='W'>W</option>
                                    <option value='X'>X</option>
                                    <option value='Y'>Y</option>
                                    <option value='Z'>Z</option>
                                </select>
                              </div>


                              <div id="divAnio" class="col-lg-4 col-xs-12" style="margin-bottom: 20px;">
                                <label><label style="color: red;">(*)</label> Año: </label>
                                <select data-size="6" id="anio" name="anio" data-live-search="true" class="form-control selectpicker" required>
                                    <option selected="true" disabled="disabled" value="">[SELECCIONAR]</option>
                                    <?php 
                                    date_default_timezone_set('America/Argentina/Buenos_Aires');
                                    $anio=date("Y");
                                    $anioFin=2016;
                                    while ($anio>=2016) {
                                      echo '<option value='.$anio.'>'.$anio.'</option>';
                                      $anio--;
                                    }

                                    ?>                                     
                                </select>
                              </div>
                                


                                

                                
                                
                                <div class="col-lg-12 col-xs-12">
                                  <label><label style="color: red;">(*)</label> Requirente: </label> 

                                  <input type="text" class="texto form-control" name="requirente" id="requirente" maxlength="100" placeholder="Requirente" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;"><div>
                                  

                                  <label><label style="color: red;">(*)</label> Trámite: </label> <label style="font-weight: normal" id="contTramite">0</label> <label style="font-weight: normal">/ 150 Caracteres</label>
                                  <div>
                                  <textarea maxlength="150" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="tramite" id="tramite" rows="3" cols="65" onkeyup="return contadorCaracteres('#tramite','#contTramite')" placeholder=" Trámite" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase; resize: none"></textarea>
                                  </div>

                                  <div class="formSelect">
                                    <label><label style="color: red;">(*)</label> Agente: </label>
                                    <select id="idAgente" name="idAgente" class="form-control selectpicker" data-live-search="true" required> </select>
                                  </div>



                                  <label>Informe Inicial: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="informeInicialActual" name="informeInicialActual">
                                  <a href="" id="enlaceInformeInicial" name="enlaceInformeInicial" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('inicial');" id="borrarInformeInicial" name="borrarInformeInicial" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>

                                  <input type="file" class="texto form-control" name="informeInicial" id="informeInicial" accept="application/pdf" onchange="validarInfInicial()">


                                  <label>Informe 15 días: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="informeQuinceActual" name="informeQuinceActual">
                                  <a href="" id="enlaceInformeQuince" name="enlaceInformeQuince" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('quince');" id="borrarInformeQuince" name="borrarInformeQuince" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>
                                  <input type="file" class="texto form-control" name="informeQuince" id="informeQuince" accept="application/pdf" onchange="validarInfQuince()">

                                  <label>Informe 30 días: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="informeTreintaActual" name="informeTreintaActual">
                                  <a href="" id="enlaceInformeTreinta" name="enlaceInformeTreinta" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('treinta');" id="borrarInformeTreinta" name="borrarInformeTreinta" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>
                                  <input type="file" class="texto form-control" name="informeTreinta" id="informeTreinta" accept="application/pdf" onchange="validarInfTreinta()">

                                  <label>Informe 45 días: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="informeCuarentaCincoActual" name="informeCuarentaCincoActual">
                                  <a href="" id="enlaceInformeCuarentaCinco" name="enlaceInformeCuarentaCinco" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('cuarentaCinco');" id="borrarInformeCuarentaCinco" name="borrarInformeCuarentaCinco" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>
                                  <input type="file" class="texto form-control" name="informeCuarentaCinco" id="informeCuarentaCinco" accept="application/pdf" onchange="validarInfCuarentaCinco()">

                                  <label>Informe Mensual: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="informeMensualActual" name="informeMensualActual">
                                  <a href="" id="enlaceInformeMensual" name="enlaceInformeMensual" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('mensual');" id="borrarInformeMensual" name="borrarInformeMensual" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>
                                  <input type="file" class="texto form-control" name="informeMensual" id="informeMensual" accept="application/pdf" onchange="validarInfMensual()">

                                  <label>Resolución: <label style="color: red;">Solo admite .pdf</label></label>
                                  <input type="hidden" id="resolucionActual" name="resolucionActual">
                                  <a href="" id="enlaceResolucion" name="enlaceResolucion" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                                  <a onclick="borrarArchivo('resolucion');" id="borrarResolucion" name="borrarResolucion" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a> 
                                  <input type="file" class="texto form-control" name="resolucion" id="resolucion" accept="application/pdf" onchange="validarRes()">

                                  <label>Observación: </label> <label style="font-weight: normal" id="contObservacion">0</label> <label style="font-weight: normal">/ 150 Caracteres</label>
                                  <div>
                                  <textarea maxlength="150" class="formSelect col-lg-12 col-md-12 col-sm-12 col-xs-12" name="observacion" id="observacion" rows="3" cols="65" onkeyup="return contadorCaracteres('#observacion','#contObservacion')" placeholder="Observación" onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase; resize: none"></textarea>
                                  </div>
                                  
                                  
                                  <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                                  <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                                </div>  
                            </div>
                          </form>

                      
                    </div>
                    <!--Fin centro -->
                    <?php
                                } //Fin contenido autorizado
                    ?>  

                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->
  

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
  
  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/expediente.js?ver=<?php echo $version; ?>"></script>


<?php
}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>


