<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioDefPos'])){
  header('Location: login');
}else{
  require 'header.php';
//Llave
?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section id="contenedor" class="content" style="text-align: center; width: 100%;">
              <!--imagen cargando-->
              <div id="cargandoGif" style="margin: 0 auto; width: 50%;">
                <img src="../public/img/cargando.gif" style="width: 60px;">
              </div>
              <!--fin imagen cargando-->
            <div id="contenido" style="display: none;" class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border" style="border-bottom: 5px black solid;">
                        <h1 class="box-title" style="font-size:20px; font-weight: 600;" ><i class="fa fa-map"></i> Planchetas Catastrales</h1>
                    </div>
                    <?php
                      if($_SESSION['v_planchetas']==0){
                        echo '<div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px; font-weight: 200;" > No posee acceso a esta vista.</h1>
                              </div>';
                      }else{

                     //Contenido autorizado
                    ?>


                    <div class="box-header with-border">
                      <?php
                        if($_SESSION['new_planchetas']==1){
                        echo '
                                <button id="btnNuevo" class="btn btn-success" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Nuevo</button>
                              ';
                        } 
                      ?>
                       

                    </div>
                    
                    <!-- /.box-header -->

                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoRegistros">

                        <table id="tblListado" class="table table-striped table-bordered table-hover" style="width:100%;">
                          <thead>
                            <th>Id</th>
                            <th style="width: 70px;">&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;Referencia&nbsp;</th>
                            <th>&nbsp;Plancheta&nbsp;</th>
                            <th>&nbsp;Ultima&nbsp;Modificación&nbsp;</th>
                            
                          </thead>

                        </table>

                    </div>
                    <div class="panel-body" id="formularioRegistros"> 
                         
                          
                          <form name="formulario" id=formulario method="POST">
                          <div class="formMedio"> 
                            
                              <div class="formSelect">
                                <label style="color: red;  font-size:15px">(*) Campos Obligatorios </label>
                              </div>
                              <input type="hidden" name="idPlancheta" id="idPlancheta">
                            


                              <label><label style="color: red;">(*)</label> Referencia: <strong style="color: red;">No puede contener los signos \ / : * ? " < > |</strong> </label>
                              <input type="hidden" name="referenciaOriginal" id="referenciaOriginal">
                              <input type="text" class="texto form-control" name="referencia" id="referencia" maxlength="100" placeholder="Referencia" required onblur="this.value=this.value.toUpperCase();" style="text-transform:uppercase;">



                              <label> Plancheta: <label style="color: red;">Solo admite .pdf</label></label>
                              <input type="hidden" id="planchetaActual" name="planchetaActual">
                              <a href="" id="enlacePlancheta" name="enlacePlancheta" target="blank" style="font-size: 25px;"><i class="fa fa-eye"></i></a>

                              <a onclick="borrarArchivo();" id="borrarPlancheta" name="borrarPlancheta" style="font-size: 25px;"><i class="fas fa-trash-alt"></i></a>

                              <input type="file" class="texto form-control" name="plancheta" id="plancheta" accept="application/pdf" onchange="validarPlancheta()">

                            
                              	
                              
                              
                              <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button> 

                              <button class="col-lg-3 col-md-6 col-sm-8 col-xs-12 boton btn btn-danger" type="button" onclick="ocultarForm()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button> 
                           </div>
                          </form>

                      
                    </div>
                    <!--Fin centro -->
                    <?php
                                } //Fin contenido autorizado
                    ?>  

                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->
  

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
  
  <!--Modal -->
  <div class="modal fade" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <img name="cargando" id="cargando" src="../public/img/cargando.gif" style="height: 50px; width: 50px; margin-left: 40%;">
        </div>
      </div>
    </div> 
  </div>
  <!--Fin Modal -->
<?php
  require 'footer.php';

?>
<script type="text/javascript" src="scripts/planchetaCatastral.js?ver=<?php echo $version;?>"></script>

<?php
}
//Fin llave
ob_end_flush(); //libera el espacio del buffer
?>


